﻿#ifndef QQCELL_H
#define QQCELL_H

#include <QString>
#include <QList>
#include <QSharedPointer>
#include <QDebug>
#include <QMap>
#include <QDebug>
#include <QObject>

#include "unit.h"

typedef enum tagQQCellType_e
{
    QQCellType_Group = 0,               //好友组
    QQCellType_Child = 1,               //好友
    QQCellType_GroupEx = 2,             //群组
}QQCellType_e;

class QQCell;
class QQCellChild;
class QQCellGroup;
class ChatWin;

typedef QSharedPointer<QQCell> PtrQQCell;
typedef QSharedPointer<QQCellChild> PtrQQCellChild;
typedef QSharedPointer<QQCellGroup> PtrQQCellGroup;

class QQCell:public QObject
{
    Q_OBJECT
public:
    QQCell(QObject *parent = nullptr):QObject(parent){}
    virtual ~QQCell(){}
public:
    QQCellType_e    type;
};

class QQCellGroup : public QQCell
{
public:
    QQCellGroup();
    ~QQCellGroup(){ qDebug() << "~QQCellGroup()"; }
    void SetSubTitle(const QString &text);
    void SetIconPath(const QString &path);
    void insertChildItem(const PtrQQCellChild& cell);
    bool removeChildItem(const PtrQQCellChild& cell);
    PtrQQCellChild removeChildItem(int QQid);
    void removeAllChild();
    int getOnlineCount();
    int getFriendCount(){ return m_mapChilds.size(); }
    PtrQQCellChild getPtrChildById(int QQid) { return m_mapChilds[QQid]; }
    QList<PtrQQCellChild> getlistChilds()
    {
        return m_mapChilds.values();
    }
public:

    //Group Information
    QString         m_strGroupName;     //组名
    QString         m_strIconPath;
    QString         m_strSubTitle;

    bool            m_bIsOpen;          //是否展开组员（子项）信息
private:
    //子项
    QMap<int,PtrQQCellChild> m_mapChilds;
    QList<PtrQQCellChild> m_listChilds;
};

class QQCellChild : public QQCell
{
    Q_OBJECT
public:
    QQCellChild() : m_strGroupName("我的好友"),m_pChatWin(NULL),m_UnReadMsg(0),m_bShake(false) {  type = QQCellType_Child; }
    ~QQCellChild(){ qDebug() << "~QQCellChild()"; }
    void SetSubTitle(const QString &text);
    void SetIconPath(const QString &path);
    void SetStatus(const quint8 &val);
    void SetUnReadMsg()
    {
        if( 0 == m_UnReadMsg )
        {
            Q_EMIT sigUnReadMsg(m_id);
        }
        m_UnReadMsg++;
    }
    int GetUnReadMsg(){ return m_UnReadMsg; }
    void ClearUnReadMsg(){ m_UnReadMsg = 0; Q_EMIT sigClearUnReadMsg(m_id); }
public:
    User                m_childInfo;            //好友信息
    //QString             m_strIpAddr;
    QString             m_strGroupName;     //所属分组
    //QString           m_strIconPath;
    //QString           m_strName;          //子项昵称
    //QString           m_strSign;          //子项签名
    int                 m_id;               //qq号，作为标识
    quint8              m_status;           //在线，离线
    ChatWin             *m_pChatWin;        //对应的聊天窗口
    bool                m_bShake;           //是否有窗口抖动的消息
    bool                m_bTransFlag;       //是否有待接收的文件
    QList<QStringList>  m_listFileInfo;     //待接收的文件队列信息
private:
    int                 m_UnReadMsg;        //未读消息条数
signals:
    void sigUnReadMsg(int userid);              //第一条未读消息到来时，提示头像闪动
    void sigClearUnReadMsg(int userid);         //清除所有未读消息，关闭头像闪动
};

#endif // QQCELL_H
