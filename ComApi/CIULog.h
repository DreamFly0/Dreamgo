﻿/******************************************************************
 Copyright (C) 2016 - All Rights Reserved by
 熊猫电子有限公司
 文 件 名 : rotatingstackedwidget.h --- RotatingStackedWidget
 作 者    : Zrj
 编写日期 : 2019/1/3
 说 明    :同步写日志,适合低频写日志场合，一定要控制写日志的长度和次数，否则就会因频繁写文件和一次写入数据过大而对界面造成卡顿，
           使用前使用CIULog::Init初始化，在程序退出的地方，调用**CIULog::Uninit**回收日志模块相关的资源
 历史纪录 :
 <作者>    <日期>        <版本>        <内容>
 赵仁杰    2018/12/1    1.0.0.0 1     文件创建
*******************************************************************/
#ifndef CIULOG_H
#define CIULOG_H
#include <QString>
#include <QFile>

enum LOG_LEVEL
{
    LOG_LEVEL_INFO,
    LOG_LEVEL_WARNING,
    LOG_LEVEL_ERROR
};

class CIULog
{
public:
    static bool Init(bool bToFile, QString strLogFileName);
    static void Unit();

    //不输出线程ID号和所在函数签名
    static bool Log(long nLevel, const char *pszFmt, ...);
    //输出线程ID号和所在函数签名
    static bool Log(long nLevel, const char *FunctionSig, int nLineNo, const char *pszFmt, ...);
    //设置日志输出级别
    void SetLevel(LOG_LEVEL nLevel);
private:
    CIULog() = delete;
    ~CIULog() = delete;

    CIULog(const CIULog& rhs) = delete;
    CIULog& operator=(const CIULog& rhs) = delete;
private:
    static bool         m_bToFile;				//日志写入文件还是写到控制台
    static QString      m_strLogFileName;
    static QFile        m_logFile;
    static LOG_LEVEL    m_sLogLevel;            //日志级别,默认LOG_LEVEL_INFO
};

//是否打印日志
#define LOG_DEBUG_ON 1

//注意：如果打印的日志信息中有中文，则格式化字符串要用_T()宏包裹起来？
#ifdef LOG_DEBUG_ON
#define LOG_INFO(...)     CIULog::Log(LOG_LEVEL_INFO, __FUNCSIG__,__LINE__, __VA_ARGS__)
#define LOG_WARNING(...)  CIULog::Log(LOG_LEVEL_WARNING, __FUNCSIG__, __LINE__,__VA_ARGS__)
#define LOG_ERROR(...)    CIULog::Log(LOG_LEVEL_ERROR, __FUNCSIG__,__LINE__, __VA_ARGS__)
#else
#define LOG_INFO(...)
#define LOG_WARNING(...)
#define LOG_ERROR(...)
#endif

#endif // CIULOG_H
