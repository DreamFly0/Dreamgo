﻿#ifndef UNIT_H
#define UNIT_H
#include <QString>

/**
 *  用于客户端组件之间发送状态
 **/
typedef enum {
    ConnectedHost = 0x01,
    DisConnectedHost,

    LoginSuccess,       // 登录成功
    LoginPasswdError,   // 密码错误
    LoginRepeat,        // 重复登录
    LoginNotRegister,   // 账号未注册

    RegisterOk,         //注册成功
    RegisterFailed,     //注册失败
    RegisterAlready,    //账号已存在

    AddFriendOk,
    AddFriendFailed,
} E_STATUS;

//客户端类型
enum CLIENT_TYPE
{
    CLIENT_TYPE_UNKNOWN = 0,
    CLIENT_TYPE_PC      = 1,
    CLIENT_TYPE_IOS		= 2,
    CLIENT_TYPE_ANDROID = 3
};

//在线状态
enum ONLINE_STATUS
{
    STATUS_OFFLINE		  = 0,			//离线
    STATUS_ONLINE		  = 1,			//在线
    STATUS_INVISIBLE	  = 2,			//隐身
    STATUS_BUSY			  = 3,			//忙碌
    STATUS_AWAY			  = 4,			//离开
    STATUS_MOBILE_ONLINE  = 5,			//移动版在线
    STATUS_MOBILE_OFFLINE = 6,			//移动版下线
    STATUS_BOTH			  = 7			//手机和电脑同时在线
};


//用户或者群
struct User
{
    qint32          userid;      //0x0FFFFFFF以上是群号，以下是普通用户//类似QQ号，系统按注册先后顺序分配
    QString         username;    //群账户的username也是群号userid的字符串形式，这个是注册的手机号
    QString         password;
    QString         nickname;    //群账号为群名称
    qint32          facetype;
    QString         customface;
    QString         customfacefmt;//自定义头像格式
    qint32          gender;
    QString         birthday;
    QString         signature;
    QString         address;
    QString         phonenumber;
    QString         mail;
    qint32          ownerid;        //对于群账号，为群主userid
    //set<qint32>     friends;        //为了避免重复
};

//文件上传返回结果码
enum FILE_TRANS_RETCODE
{
    FILE_UPLOAD_FAILED,
    FILE_UPLOAD_SUCCESS,
    FILE_UPLOAD_CANCEL,

    FILE_DOWNLOAD_FAILED,
    FILE_DOWNLOAD_SUCCESS,
    FILE_DOWNLOAD_CANCEL,
};

//文件上传下载类型
enum FILE_ITEM_TYPE
{
    FILE_ITEM_UNKNOWN,
    FILE_ITEM_UPLOAD_CHAT_IMAGE,
    FILE_ITEM_UPLOAD_CHAT_OFFLINE_FILE,
    FILE_ITEM_UPLOAD_USER_THUMB,

    FILE_ITEM_DOWNLOAD_CHAT_IMAGE,
    FILE_ITEM_DOWNLOAD_CHAT_OFFLINE_FILE,
    FILE_ITEM_DOWNLOAD_USER_THUMB,
};
#endif // UNIT_H

