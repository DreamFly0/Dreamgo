﻿#include "QQCell.h"

QQCellGroup::QQCellGroup()
{
    type = QQCellType_Group;
    m_bIsOpen = false;
}

void QQCellGroup::SetSubTitle(const QString &text)
{
    if(text.isEmpty()) return;
    m_strSubTitle = text;
}

void QQCellGroup::SetIconPath(const QString &path)
{
    if(path.isEmpty()) return;
    m_strIconPath = path;
}

void QQCellGroup::insertChildItem(const PtrQQCellChild& cell)
{
    m_mapChilds[cell->m_id] = cell;
}

bool QQCellGroup::removeChildItem(const PtrQQCellChild& cell)
{
    return m_mapChilds.remove(cell->m_id);
}

PtrQQCellChild QQCellGroup::removeChildItem(int QQid)
{
    PtrQQCellChild child = m_mapChilds[QQid];
    m_mapChilds.remove(QQid);
    return child;
}

int QQCellGroup::getOnlineCount()
{
    int nOnlineCnt = 0;
    for(PtrQQCellChild child : m_mapChilds.values())
    {
        if(child->m_status == STATUS_ONLINE)
            nOnlineCnt++;
    }
    return nOnlineCnt;
}

void QQCellGroup::removeAllChild()
{
    m_mapChilds.clear();
}
///////////////////////////////////////////////////////////////////////////////
/// \brief QQCellChild::SetSubTitle
/// \param text
///
void QQCellChild::SetSubTitle(const QString &text)
{
    Q_UNUSED(text);
}

void QQCellChild::SetIconPath(const QString &path)
{
    Q_UNUSED(path);
}

void QQCellChild::SetStatus(const quint8 &val)
{
    m_status = val;
}
