﻿#ifndef MYAPP_H
#define MYAPP_H
#include <QString>
#include <QVariant>
#include "Windows.h"
#include <DbgHelp.h>
#include <QMessageBox>
#include <QDateTime>
class MyApp
{
public:
    MyApp();
    //=======================系统配置部分=========================//
    static QString m_strAppPath;         // 应用程序路径
    static QString m_strDataPath;        // 数据保存路径
    static QString m_strConfPath;        // 配置目录
    static QString s_strDatabasePath;    //数据库保存路径
    static QString m_strIniFile;         // 配置文件
    static QString m_strSoundPath;       // 声音文件
    static QString m_strRecvPath;       //接收文件路径

    static QString m_strUsersFile;      //登录用户列表文件
    static QString m_strUserCfgFile;    //用户配置文件
    static QString m_strUsersPath;      //配置文件:用户信息
    static QString m_strHeadPath;       //配置文件:头像
    static QString m_strHostAddr;       // 服务器地址配置
    static QString m_strFileServerAddr; //文件服务器地址配置
    static int     m_nMsgPort;          // 聊天消息服务器端口配置
    static int     m_nFileSeverPort;    //文件服务器端口配置
    static bool    m_bPlaySound;        //播放声音
    static bool    m_bShowExitDlg;      //退出提示对话框
    static bool    m_bMainWindowHide;   //点击退出按钮时最小化还是关闭

    static QString m_strUserName;       //当前登录用户名
    static QString m_strUserPwd;        //当前登录用户密码
    //=======================函数功能部分=========================//
    // 初始化
    static void InitApp(const QString &appPath);
    static LONG ApplicationCrashHandler(EXCEPTION_POINTERS *pException);
    // 创建配置文件
    static void CreatorSettingFile();
    // 读配置文件，加载系统配置
    static void ReadSettingFile();

    // 读取/修改配置文件
    static void SetSettingFile(const QString& filename, const QString &group, const QString &key, const QVariant &value);
    static QVariant GetSettingKeyValue(const QString& filename, const QString &group, const QString &key, const QVariant &value);
    // 检查目录
    static void CheckDirs();
    static void UnPackHeasicons();
    static void SaveConfig();
    static void CheckSound();
};

#endif // MYAPP_H
