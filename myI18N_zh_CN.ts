<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AmplifierWidget</name>
    <message>
        <location filename="ScreenShot/ui_amplifierwidget.h" line="45"/>
        <source>AmplifierWidget</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AppLauncher</name>
    <message>
        <location filename="ScreenShot/capture/tools/applauncher.cpp" line="25"/>
        <source>App Launcher</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BottomWidget</name>
    <message>
        <location filename="BottomWidget/BottomWidget.cpp" line="29"/>
        <source>System Settings</source>
        <translation>系统设置</translation>
    </message>
    <message>
        <location filename="BottomWidget/BottomWidget.cpp" line="30"/>
        <source>Message manager</source>
        <translation>消息管理器</translation>
    </message>
    <message>
        <location filename="BottomWidget/BottomWidget.cpp" line="31"/>
        <source>File assistant</source>
        <translation>文件助手</translation>
    </message>
    <message>
        <location filename="BottomWidget/BottomWidget.cpp" line="32"/>
        <source>Change the password</source>
        <translation>修改密码</translation>
    </message>
    <message>
        <location filename="BottomWidget/BottomWidget.cpp" line="33"/>
        <source>help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="BottomWidget/BottomWidget.cpp" line="34"/>
        <source>Reconnect server</source>
        <translation>重连服务器</translation>
    </message>
    <message>
        <location filename="BottomWidget/BottomWidget.cpp" line="35"/>
        <source>Update</source>
        <translation>升级</translation>
    </message>
    <message>
        <location filename="BottomWidget/BottomWidget.cpp" line="59"/>
        <source>The system menu</source>
        <translation>系统菜单</translation>
    </message>
    <message>
        <location filename="BottomWidget/BottomWidget.cpp" line="60"/>
        <source>Add buddy</source>
        <translation>添加好友</translation>
    </message>
    <message>
        <location filename="BottomWidget/BottomWidget.cpp" line="61"/>
        <source>Weather</source>
        <translation>天气查询</translation>
    </message>
    <message>
        <location filename="BottomWidget/BottomWidget.cpp" line="62"/>
        <source>Calendar</source>
        <translation>日历</translation>
    </message>
    <message>
        <location filename="BottomWidget/BottomWidget.cpp" line="143"/>
        <source>Looking for it, please wait...</source>
        <translation>正在查找，请稍后...</translation>
    </message>
    <message>
        <location filename="BottomWidget/BottomWidget.cpp" line="178"/>
        <source>The request of adding friend has been sent, please wait for confirmation</source>
        <translation>好友添加请求已发送，请等待对方确认</translation>
    </message>
</context>
<context>
    <name>BuddyListChild</name>
    <message>
        <source>查看好友资料</source>
        <translation type="vanished">查看好友资料</translation>
    </message>
    <message>
        <location filename="MiddleWidget/QQList/BuddyListChild.cpp" line="34"/>
        <source>View friends&apos; information</source>
        <translation>查看好友信息</translation>
    </message>
</context>
<context>
    <name>BuddyListWidget</name>
    <message>
        <location filename="MiddleWidget/QQList/BuddyListWidget.cpp" line="53"/>
        <source>发送即时消息</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MiddleWidget/QQList/BuddyListWidget.cpp" line="54"/>
        <source>发送电子邮件</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MiddleWidget/QQList/BuddyListWidget.cpp" line="55"/>
        <source>查看资料</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MiddleWidget/QQList/BuddyListWidget.cpp" line="56"/>
        <source>修改备注名</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MiddleWidget/QQList/BuddyListWidget.cpp" line="57"/>
        <source>删除好友</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MiddleWidget/QQList/BuddyListWidget.cpp" line="58"/>
        <source>移动联系人至</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CFileTask</name>
    <message>
        <location filename="Network/CFileTask.cpp" line="239"/>
        <source>File Send Failed!</source>
        <translation>文件发送失败</translation>
    </message>
    <message>
        <location filename="Network/CFileTask.cpp" line="242"/>
        <source>File Send Successed!</source>
        <translation>文件发送成功</translation>
    </message>
    <message>
        <location filename="Network/CFileTask.cpp" line="245"/>
        <source>File Recv Failed!</source>
        <translation type="unfinished">接收失败</translation>
    </message>
    <message>
        <location filename="Network/CFileTask.cpp" line="248"/>
        <source>File Recv Successed!</source>
        <translation>接受成功</translation>
    </message>
</context>
<context>
    <name>CMessageBox</name>
    <message>
        <source>恭喜你，升职加薪！</source>
        <translation type="obsolete">恭喜你，升职加薪！</translation>
    </message>
    <message>
        <location filename="BaseWidget/CustomWidget.cpp" line="172"/>
        <source>Test text！</source>
        <translation>测试文本!</translation>
    </message>
    <message>
        <location filename="BaseWidget/CustomWidget.cpp" line="181"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="BaseWidget/CustomWidget.cpp" line="184"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="BaseWidget/CustomWidget.cpp" line="274"/>
        <source>OK(%1)</source>
        <translation>确定(%1)</translation>
    </message>
    <message>
        <location filename="BaseWidget/CustomWidget.h" line="102"/>
        <source>Infomation</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="BaseWidget/CustomWidget.h" line="103"/>
        <source>Question</source>
        <translation>问题</translation>
    </message>
    <message>
        <location filename="BaseWidget/CustomWidget.h" line="104"/>
        <source>Warning</source>
        <translation>告警</translation>
    </message>
</context>
<context>
    <name>ChatWin</name>
    <message>
        <location filename="MiddleWidget/ChatWin.cpp" line="671"/>
        <source> is being transferred. Closing the window will terminate the transfer. Are you sure you want to close it?</source>
        <translation>文件正在传输，关闭窗口将终止传输，你确实要关闭它?</translation>
    </message>
    <message>
        <location filename="MiddleWidget/ChatWin.cpp" line="452"/>
        <source>Press Enter to send the message</source>
        <translation>按Enter键发送消息</translation>
    </message>
    <message>
        <location filename="MiddleWidget/ChatWin.cpp" line="453"/>
        <source>Press Ctrl+Enter to send the message</source>
        <translation>按Ctrl+Enter发送消息</translation>
    </message>
    <message>
        <location filename="MiddleWidget/ChatWin.cpp" line="769"/>
        <location filename="MiddleWidget/ChatWin.cpp" line="1479"/>
        <location filename="MiddleWidget/ChatWin.cpp" line="1547"/>
        <location filename="MiddleWidget/ChatWin.cpp" line="1585"/>
        <location filename="MiddleWidget/ChatWin.cpp" line="1621"/>
        <source>The network connection has been disconnected. Please login again</source>
        <translation>网络故障，请重新登录</translation>
    </message>
    <message>
        <location filename="MiddleWidget/ChatWin.cpp" line="774"/>
        <source>Send content cannot be empty!</source>
        <translation>发送内容不能为空！</translation>
    </message>
    <message>
        <location filename="MiddleWidget/ChatWin.cpp" line="1303"/>
        <source>Choose file</source>
        <translation>选择文件</translation>
    </message>
    <message>
        <source>文件发送成功</source>
        <translation type="vanished">文件发送成功</translation>
    </message>
    <message>
        <location filename="MiddleWidget/ChatWin.cpp" line="1679"/>
        <location filename="MiddleWidget/ChatWin.cpp" line="1701"/>
        <source>File already exists, whether overwrite or not?</source>
        <translation>文件已经存在，是否覆盖？</translation>
    </message>
    <message>
        <location filename="MiddleWidget/ChatWin.cpp" line="1695"/>
        <source>Choose Path</source>
        <translation>选择存放路径</translation>
    </message>
    <message>
        <source>File sent successfully</source>
        <translation type="vanished">文件发送成功</translation>
    </message>
    <message>
        <location filename="MiddleWidget/ChatWin.cpp" line="1199"/>
        <source>(Checking file)</source>
        <translation>正在校验文件</translation>
    </message>
    <message>
        <location filename="MiddleWidget/ChatWin.cpp" line="1209"/>
        <location filename="MiddleWidget/ChatWin.cpp" line="1278"/>
        <source>file transfer</source>
        <translation>文件传输</translation>
    </message>
    <message>
        <source>File Send Failed!</source>
        <translation type="vanished">文件发送失败</translation>
    </message>
    <message>
        <source>File Send Successed!</source>
        <translation type="vanished">文件发送成功</translation>
    </message>
</context>
<context>
    <name>ConfigWin</name>
    <message>
        <location filename="LoginAnimation/ConfigWin.ui" line="26"/>
        <source>Form</source>
        <translation>网络设置窗口</translation>
    </message>
    <message>
        <location filename="LoginAnimation/ConfigWin.ui" line="251"/>
        <source>Network Settings</source>
        <comment>网络设置</comment>
        <translation>网络设置</translation>
    </message>
    <message>
        <location filename="LoginAnimation/ConfigWin.ui" line="277"/>
        <source>Chat service address</source>
        <comment>聊天服务地址</comment>
        <translation>聊天服务地址</translation>
    </message>
    <message>
        <location filename="LoginAnimation/ConfigWin.ui" line="294"/>
        <source>Chat service port</source>
        <comment>聊天服务端口</comment>
        <translation>聊天服务端口</translation>
    </message>
    <message>
        <location filename="LoginAnimation/ConfigWin.ui" line="311"/>
        <source>Image service address</source>
        <comment>图片服务地址</comment>
        <translation>图片服务地址</translation>
    </message>
    <message>
        <location filename="LoginAnimation/ConfigWin.ui" line="328"/>
        <source>Image service port</source>
        <comment>图片服务端口</comment>
        <translation>图片服务端口</translation>
    </message>
    <message>
        <location filename="LoginAnimation/ConfigWin.ui" line="406"/>
        <source>OK</source>
        <comment>确定</comment>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="LoginAnimation/ConfigWin.ui" line="425"/>
        <source>Cancel</source>
        <comment>取消</comment>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="LoginAnimation/ConfigWin.cpp" line="53"/>
        <source>The address cannot be empty</source>
        <translation>地址不能为空</translation>
    </message>
    <message>
        <location filename="LoginAnimation/ConfigWin.cpp" line="58"/>
        <source>The port number cannot be empty</source>
        <translation>端口号不能为空</translation>
    </message>
    <message>
        <location filename="LoginAnimation/ConfigWin.cpp" line="63"/>
        <source>The FileServer address cannot be empty</source>
        <translation>文件服务器地址不能为可控</translation>
    </message>
    <message>
        <location filename="LoginAnimation/ConfigWin.cpp" line="68"/>
        <source>The FileServer port number cannot be empty</source>
        <translation>文件服务器端口不能为空</translation>
    </message>
</context>
<context>
    <name>ContactsWidget</name>
    <message>
        <location filename="MiddleWidget/ContactsWidget.cpp" line="95"/>
        <source>我的好友</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MiddleWidget/ContactsWidget.cpp" line="100"/>
        <source>黑名单</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExitDlg</name>
    <message>
        <location filename="BaseWidget/ExitDlg.cpp" line="11"/>
        <source>最小化</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="BaseWidget/ExitDlg.cpp" line="12"/>
        <source>退出</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="BaseWidget/ExitDlg.cpp" line="33"/>
        <source>退出提示</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="BaseWidget/ExitDlg.cpp" line="45"/>
        <source>退出程序还是最小化到托盘？</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="BaseWidget/ExitDlg.cpp" line="47"/>
        <source>下次不再提示</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FindFriendDlg</name>
    <message>
        <location filename="BottomWidget/FindFriendDlg.cpp" line="11"/>
        <source>Contact</source>
        <translation>好友</translation>
    </message>
    <message>
        <location filename="BottomWidget/FindFriendDlg.cpp" line="12"/>
        <source>Group</source>
        <translation>群组</translation>
    </message>
    <message>
        <location filename="BottomWidget/FindFriendDlg.cpp" line="34"/>
        <source>Find a contact or group</source>
        <translation>查找好友或群组</translation>
    </message>
    <message>
        <location filename="BottomWidget/FindFriendDlg.cpp" line="39"/>
        <source>Mobilephone/Group number</source>
        <translation>手机号/群号</translation>
    </message>
    <message>
        <location filename="BottomWidget/FindFriendDlg.cpp" line="46"/>
        <source>Find</source>
        <translation>查找</translation>
    </message>
</context>
<context>
    <name>FindUserResultDlg</name>
    <message>
        <location filename="BottomWidget/FindUserResultDlg.cpp" line="10"/>
        <source>Add buddy</source>
        <translation>添加好友</translation>
    </message>
    <message>
        <location filename="BottomWidget/FindUserResultDlg.cpp" line="11"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="BottomWidget/FindUserResultDlg.cpp" line="31"/>
        <source>User information</source>
        <translation>好友信息</translation>
    </message>
    <message>
        <location filename="BottomWidget/FindUserResultDlg.cpp" line="62"/>
        <source>Account: </source>
        <translation>账号: </translation>
    </message>
    <message>
        <location filename="BottomWidget/FindUserResultDlg.cpp" line="63"/>
        <source>Nickname: </source>
        <translation>密码: </translation>
    </message>
    <message>
        <location filename="BottomWidget/FindUserResultDlg.cpp" line="64"/>
        <source>You cannot add yourself as a friend</source>
        <translation>你不能添加自己为好友</translation>
    </message>
</context>
<context>
    <name>FriendInfoDlg</name>
    <message>
        <location filename="MiddleWidget/QQList/FriendInfoDlg.cpp" line="8"/>
        <source>账    号：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MiddleWidget/QQList/FriendInfoDlg.cpp" line="9"/>
        <source>昵    称：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MiddleWidget/QQList/FriendInfoDlg.cpp" line="10"/>
        <source>个性签名：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MiddleWidget/QQList/FriendInfoDlg.cpp" line="11"/>
        <source>性    别：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MiddleWidget/QQList/FriendInfoDlg.cpp" line="12"/>
        <source>生    日：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MiddleWidget/QQList/FriendInfoDlg.cpp" line="13"/>
        <source>地    址：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MiddleWidget/QQList/FriendInfoDlg.cpp" line="14"/>
        <source>电    话：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MiddleWidget/QQList/FriendInfoDlg.cpp" line="15"/>
        <source>邮    箱：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MiddleWidget/QQList/FriendInfoDlg.cpp" line="24"/>
        <source>关闭</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginAnimationWin</name>
    <message>
        <location filename="LoginAnimation/LoginAnimationWin.ui" line="14"/>
        <source>QQLoginAnimation</source>
        <translation>登录窗口</translation>
    </message>
    <message>
        <location filename="LoginAnimation/LoginAnimationWin.ui" line="123"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="LoginAnimation/LoginAnimationWin.cpp" line="164"/>
        <source>Server not connected, please wait!</source>
        <translation>服务器未连接，请稍候！</translation>
    </message>
    <message>
        <location filename="LoginAnimation/LoginAnimationWin.cpp" line="173"/>
        <source>User name cannot be empty.</source>
        <translation>用户名不能为空</translation>
    </message>
    <message>
        <location filename="LoginAnimation/LoginAnimationWin.cpp" line="177"/>
        <source>Password cannot be empty.</source>
        <translation>密码不能为空</translation>
    </message>
    <message>
        <location filename="LoginAnimation/LoginAnimationWin.cpp" line="181"/>
        <source>Cancel Login</source>
        <translation>取消登陆</translation>
    </message>
    <message>
        <location filename="LoginAnimation/LoginAnimationWin.cpp" line="289"/>
        <source>Dreamgo</source>
        <translation>Dreamgo</translation>
    </message>
    <message>
        <location filename="LoginAnimation/LoginAnimationWin.cpp" line="294"/>
        <source>Server disconnected</source>
        <translation>服务器未连接</translation>
    </message>
    <message>
        <location filename="LoginAnimation/LoginAnimationWin.cpp" line="276"/>
        <location filename="LoginAnimation/LoginAnimationWin.cpp" line="338"/>
        <location filename="LoginAnimation/LoginAnimationWin.cpp" line="345"/>
        <location filename="LoginAnimation/LoginAnimationWin.cpp" line="352"/>
        <source>Login</source>
        <translation>登录</translation>
    </message>
    <message>
        <location filename="LoginAnimation/LoginAnimationWin.cpp" line="339"/>
        <source>Login failed. Please check user name and password</source>
        <translation>登录失败，请检查用户名和密码</translation>
    </message>
    <message>
        <location filename="LoginAnimation/LoginAnimationWin.cpp" line="346"/>
        <source>Account not registered</source>
        <translation>账户未注册</translation>
    </message>
    <message>
        <location filename="LoginAnimation/LoginAnimationWin.cpp" line="353"/>
        <source>Login failed. The account is logged in</source>
        <translation>登陆失败，该账号已登录</translation>
    </message>
    <message>
        <location filename="LoginAnimation/LoginAnimationWin.cpp" line="358"/>
        <source>The account has been successfully registered.Please click login!</source>
        <translation>该账号成功注册，请点击登陆</translation>
    </message>
    <message>
        <location filename="LoginAnimation/LoginAnimationWin.cpp" line="365"/>
        <source>Registration failed.Please register again!</source>
        <translation>注册失败，请重新注册</translation>
    </message>
    <message>
        <location filename="LoginAnimation/LoginAnimationWin.cpp" line="409"/>
        <location filename="LoginAnimation/LoginAnimationWin.cpp" line="421"/>
        <location filename="LoginAnimation/LoginAnimationWin.cpp" line="450"/>
        <location filename="LoginAnimation/LoginAnimationWin.cpp" line="464"/>
        <source>Server not connected, please wait</source>
        <translation>未连接服务器，请等待</translation>
    </message>
</context>
<context>
    <name>LoginRegDlg</name>
    <message>
        <location filename="LoginDlg/LoginRegDlg.cpp" line="53"/>
        <source>注册新手机号:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="LoginDlg/LoginRegDlg.cpp" line="54"/>
        <source>注册昵称:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="LoginDlg/LoginRegDlg.cpp" line="55"/>
        <source>账号密码:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="LoginDlg/LoginRegDlg.cpp" line="56"/>
        <source>确认密码:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="LoginDlg/LoginRegDlg.cpp" line="71"/>
        <source>注册</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="LoginDlg/LoginRegDlg.cpp" line="72"/>
        <source>取消</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginSettingDlg</name>
    <message>
        <location filename="LoginDlg/LoginSettingDlg.cpp" line="105"/>
        <source>地址不能为空</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="LoginDlg/LoginSettingDlg.cpp" line="110"/>
        <source>端口号不能为空</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginWindow</name>
    <message>
        <location filename="LoginAnimation/LoginWindow.ui" line="32"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="LoginAnimation/LoginWindow.ui" line="266"/>
        <source>Dreamgo</source>
        <translation type="unfinished">Dreamgo</translation>
    </message>
    <message>
        <location filename="LoginAnimation/LoginWindow.ui" line="499"/>
        <source>QQ号码/手机/邮箱</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="LoginAnimation/LoginWindow.ui" line="521"/>
        <source>密码</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="LoginAnimation/LoginWindow.ui" line="539"/>
        <source>Automatic login</source>
        <comment>自动登录</comment>
        <translation>自动登录</translation>
    </message>
    <message>
        <location filename="LoginAnimation/LoginWindow.ui" line="568"/>
        <source>Remember password</source>
        <comment>记住密码</comment>
        <translation>记住密码</translation>
    </message>
    <message>
        <location filename="LoginAnimation/LoginWindow.ui" line="602"/>
        <source>Login</source>
        <comment>登录</comment>
        <translation>登录</translation>
    </message>
    <message>
        <location filename="LoginAnimation/LoginWindow.ui" line="631"/>
        <source>Registered account</source>
        <comment>注册账号</comment>
        <translation>注册账号</translation>
    </message>
    <message>
        <location filename="LoginAnimation/LoginWindow.ui" line="656"/>
        <source>Retrieve password</source>
        <comment>找回密码</comment>
        <translation>修改密码</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="MainWindow.ui" line="25"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="49"/>
        <source>昵称</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="62"/>
        <source>签名</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="111"/>
        <source>会话</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="157"/>
        <location filename="MainWindow.ui" line="174"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="217"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="237"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="257"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="277"/>
        <source>4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="105"/>
        <source>User information update failed</source>
        <translation>用户信息更新失败</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="133"/>
        <source>The server is down</source>
        <translation>服务器已掉线</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="169"/>
        <source>View file assistant</source>
        <translation>查看文件助手</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="179"/>
        <source>A new version has been detected. Do you want to upgrade?</source>
        <translation>检测到新版本，是否更新？</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="181"/>
        <source>System upgrade successful</source>
        <translation>软件升级成功</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="198"/>
        <source>Online</source>
        <translation>在线</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="199"/>
        <source>Offline</source>
        <translation>离线</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="201"/>
        <source>Lock Dreamgo</source>
        <translation>锁定 Dreamgo</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="203"/>
        <source>Turn off all sounds</source>
        <translation>关闭所有提示音</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="205"/>
        <source>Display main panel</source>
        <translation>显示主面板</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="207"/>
        <source>Exit</source>
        <translation>离开</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="227"/>
        <source>File is being transferred. Closing the window will terminate the transfer. Are you sure you want to close it?</source>
        <translation>当前有文件正在传输，关闭窗口将终止传输，你确实要关闭它?</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="253"/>
        <source>Network failure, update failed!</source>
        <translation>网络故障，更新失败！</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="482"/>
        <source>[%1] is already your friend, please do not add again</source>
        <translation>[%1]已经是您的好友，请不要重复添加</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="482"/>
        <source>Add Buddy</source>
        <translation>添加好友</translation>
    </message>
</context>
<context>
    <name>MarkerTool</name>
    <message>
        <location filename="ScreenShot/capture/tools/markertool.cpp" line="23"/>
        <source>Marker</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageItem</name>
    <message>
        <location filename="BaseWidget/messageitem.ui" line="20"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="BaseWidget/messageitem.ui" line="83"/>
        <source>张三</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="BaseWidget/messageitem.ui" line="105"/>
        <source>12</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="BaseWidget/messageitem.ui" line="139"/>
        <source>聊天内容</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MiddleWidget</name>
    <message>
        <location filename="MiddleWidget/MiddleWidget.cpp" line="273"/>
        <source>Do you want to delete the friend?</source>
        <translation>你想删除该好友吗？</translation>
    </message>
    <message>
        <location filename="MiddleWidget/MiddleWidget.cpp" line="273"/>
        <source>Delete friend</source>
        <translation>删除好友</translation>
    </message>
</context>
<context>
    <name>ModifyPwdWin</name>
    <message>
        <location filename="LoginAnimation/ModifyPwdWin.ui" line="26"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="LoginAnimation/ModifyPwdWin.ui" line="218"/>
        <source>Register phone number</source>
        <comment>注册手机</comment>
        <translation>注册手机</translation>
    </message>
    <message>
        <location filename="LoginAnimation/ModifyPwdWin.ui" line="235"/>
        <source>ID number</source>
        <comment>注册昵称</comment>
        <translation>ID号       </translation>
    </message>
    <message>
        <location filename="LoginAnimation/ModifyPwdWin.ui" line="252"/>
        <source>Old password</source>
        <comment>注册密码</comment>
        <translation>原始密码</translation>
    </message>
    <message>
        <location filename="LoginAnimation/ModifyPwdWin.ui" line="269"/>
        <source>New password</source>
        <comment>确认密码</comment>
        <translation>新密码    </translation>
    </message>
    <message>
        <location filename="LoginAnimation/ModifyPwdWin.ui" line="326"/>
        <source>Modify</source>
        <comment>注册</comment>
        <translation>修改</translation>
    </message>
    <message>
        <location filename="LoginAnimation/ModifyPwdWin.ui" line="345"/>
        <source>Cancel</source>
        <comment>取消</comment>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="LoginAnimation/ModifyPwdWin.cpp" line="31"/>
        <source>The phone number cannot be empty</source>
        <translation>手机号不能为空</translation>
    </message>
    <message>
        <location filename="LoginAnimation/ModifyPwdWin.cpp" line="36"/>
        <source>The ID cannot be empty</source>
        <translation>ID号不能为空</translation>
    </message>
    <message>
        <location filename="LoginAnimation/ModifyPwdWin.cpp" line="41"/>
        <source>The old password cannot be empty</source>
        <translation>原始密码不能为空</translation>
    </message>
    <message>
        <location filename="LoginAnimation/ModifyPwdWin.cpp" line="46"/>
        <source>The new password cannot be empty</source>
        <translation>新密码不能为空</translation>
    </message>
</context>
<context>
    <name>MyTextEdit</name>
    <message>
        <location filename="BaseWidget/MyTextEdit.cpp" line="37"/>
        <source>Cut(&amp;T)</source>
        <oldsource>剪切(&amp;T)</oldsource>
        <translation>剪切(&amp;T)</translation>
    </message>
    <message>
        <location filename="BaseWidget/MyTextEdit.cpp" line="38"/>
        <source>Copy(&amp;C)</source>
        <oldsource>复制(&amp;C)</oldsource>
        <translation>复制(&amp;C)</translation>
    </message>
    <message>
        <location filename="BaseWidget/MyTextEdit.cpp" line="39"/>
        <source>Paste(&amp;P)</source>
        <translation>粘贴(&amp;P)</translation>
    </message>
    <message>
        <location filename="BaseWidget/MyTextEdit.cpp" line="40"/>
        <source>Select All(&amp;A)</source>
        <translation>全选(&amp;A)</translation>
    </message>
    <message>
        <location filename="BaseWidget/MyTextEdit.cpp" line="41"/>
        <source>Font(&amp;F)</source>
        <translation>字体(&amp;F)</translation>
    </message>
</context>
<context>
    <name>QColorPicker</name>
    <message>
        <location filename="ScreenShot/ui_qcolorpicker.h" line="512"/>
        <source>QColorPicker</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="ComApi/MyApp.cpp" line="282"/>
        <source>&lt;FONT size=4&gt;&lt;div&gt;&lt;b&gt;We sincerely apologize for the mistake&lt;/b&gt;&lt;br/&gt;&lt;/div&gt;</source>
        <translation type="unfinished">&lt;FONT size=4&gt;&lt;div&gt;&lt;b&gt;很抱歉，程序出现异常，即将退出&lt;/b&gt;&lt;br/&gt;&lt;/div&gt;</translation>
    </message>
    <message>
        <location filename="ComApi/MyApp.cpp" line="283"/>
        <source>&lt;div&gt;Error code:%1&lt;/div&gt;&lt;div&gt;Wrong address:%2&lt;/div&gt;&lt;/FONT&gt;</source>
        <translation>&lt;div&gt;错误代码:%1&lt;/div&gt;&lt;div&gt;错误地址:%2&lt;/div&gt;&lt;/FONT&gt;</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">错误</translation>
    </message>
</context>
<context>
    <name>RegisterWin</name>
    <message>
        <location filename="LoginAnimation/RegisterWin.ui" line="32"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="LoginAnimation/RegisterWin.ui" line="224"/>
        <source>Register phone number</source>
        <comment>注册手机</comment>
        <translation>注册手机</translation>
    </message>
    <message>
        <location filename="LoginAnimation/RegisterWin.ui" line="241"/>
        <source>Your nickname</source>
        <comment>注册昵称</comment>
        <translation>您的昵称</translation>
    </message>
    <message>
        <location filename="LoginAnimation/RegisterWin.ui" line="258"/>
        <source>Your password</source>
        <comment>注册密码</comment>
        <translation>您的密码</translation>
    </message>
    <message>
        <location filename="LoginAnimation/RegisterWin.ui" line="275"/>
        <source>Confirm password</source>
        <comment>确认密码</comment>
        <translation>确认密码</translation>
    </message>
    <message>
        <location filename="LoginAnimation/RegisterWin.ui" line="332"/>
        <source>Register</source>
        <comment>注册</comment>
        <translation>注册</translation>
    </message>
    <message>
        <location filename="LoginAnimation/RegisterWin.ui" line="351"/>
        <source>Cancel</source>
        <comment>取消</comment>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="LoginAnimation/RegisterWin.cpp" line="30"/>
        <source>The phone number cannot be empty</source>
        <translation>手机号不能为空</translation>
    </message>
    <message>
        <location filename="LoginAnimation/RegisterWin.cpp" line="35"/>
        <source>The account cannot be empty</source>
        <translation>账号不能为空</translation>
    </message>
    <message>
        <location filename="LoginAnimation/RegisterWin.cpp" line="40"/>
        <source>The password cannot be empty</source>
        <translation>密码不能为空</translation>
    </message>
    <message>
        <location filename="LoginAnimation/RegisterWin.cpp" line="45"/>
        <source>The password input is inconsistent</source>
        <translation>密码输入不一致</translation>
    </message>
</context>
<context>
    <name>RequestAdduserDlg</name>
    <message>
        <location filename="BottomWidget/RequestAdduserDlg.cpp" line="7"/>
        <source>Accept</source>
        <translation>接受</translation>
    </message>
    <message>
        <location filename="BottomWidget/RequestAdduserDlg.cpp" line="8"/>
        <source>Reject</source>
        <translation>拒绝</translation>
    </message>
    <message>
        <location filename="BottomWidget/RequestAdduserDlg.cpp" line="29"/>
        <source>Add buddy request</source>
        <translation>添加好友请求</translation>
    </message>
</context>
<context>
    <name>ShowGroupBtn</name>
    <message>
        <location filename="MiddleWidget/QQList/ShowGroupBtn.cpp" line="54"/>
        <source>添加分组</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MiddleWidget/QQList/ShowGroupBtn.cpp" line="55"/>
        <source>删除分组</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MiddleWidget/QQList/ShowGroupBtn.cpp" line="56"/>
        <source>重命名</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SizeIndicatorTool</name>
    <message>
        <location filename="ScreenShot/capture/tools/sizeindicatortool.cpp" line="21"/>
        <source>Selection Size Indicator</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TopLogin</name>
    <message>
        <location filename="LoginDlg/TopLogin.cpp" line="140"/>
        <source>逐梦通讯1.0 正在连接服务器...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="LoginDlg/TopLogin.cpp" line="216"/>
        <source>登陆</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="LoginDlg/TopLogin.cpp" line="305"/>
        <location filename="LoginDlg/TopLogin.cpp" line="503"/>
        <location filename="LoginDlg/TopLogin.cpp" line="581"/>
        <source>未连接服务器，请等待！</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="LoginDlg/TopLogin.cpp" line="385"/>
        <source>逐梦通讯1.0 服务器已连接</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="LoginDlg/TopLogin.cpp" line="390"/>
        <source>逐梦通讯1.0 服务器已断开</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="LoginDlg/TopLogin.cpp" line="504"/>
        <location filename="LoginDlg/TopLogin.cpp" line="569"/>
        <source>登录</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="LoginDlg/TopLogin.cpp" line="514"/>
        <source>用户名不能为空！</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="LoginDlg/TopLogin.cpp" line="521"/>
        <source>密码不能为空！</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="LoginDlg/TopLogin.cpp" line="546"/>
        <source>取消登录</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TopWidget</name>
    <message>
        <location filename="TopWidget/TopWidget.cpp" line="48"/>
        <source>NickName</source>
        <translation>昵称</translation>
    </message>
    <message>
        <location filename="TopWidget/TopWidget.cpp" line="63"/>
        <source>Signature</source>
        <translation>签名</translation>
    </message>
    <message>
        <location filename="TopWidget/TopWidget.cpp" line="102"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
</context>
<context>
    <name>TransFileWgt</name>
    <message>
        <location filename="MiddleWidget/TransFileWgt.ui" line="26"/>
        <source>Form</source>
        <translation>From</translation>
    </message>
    <message>
        <location filename="MiddleWidget/TransFileWgt.ui" line="88"/>
        <source>Icon</source>
        <translation>Icon</translation>
    </message>
    <message>
        <location filename="MiddleWidget/TransFileWgt.ui" line="131"/>
        <source>Title</source>
        <translation>Title</translation>
    </message>
    <message>
        <location filename="MiddleWidget/TransFileWgt.ui" line="175"/>
        <source>FILE</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="MiddleWidget/TransFileWgt.ui" line="207"/>
        <source>Save as</source>
        <translation>另存为</translation>
    </message>
    <message>
        <location filename="MiddleWidget/TransFileWgt.ui" line="226"/>
        <source>Offline send</source>
        <translation>转离线发送</translation>
    </message>
    <message>
        <location filename="MiddleWidget/TransFileWgt.ui" line="245"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>(File verification completed)</source>
        <translation type="vanished">文件校验完成</translation>
    </message>
    <message>
        <location filename="MiddleWidget/TransFileWgt.cpp" line="72"/>
        <source>Recv</source>
        <translation>接收</translation>
    </message>
    <message>
        <location filename="MiddleWidget/TransFileWgt.cpp" line="80"/>
        <source>Waiting to receive</source>
        <translation>准备接收</translation>
    </message>
    <message>
        <location filename="MiddleWidget/TransFileWgt.cpp" line="123"/>
        <source>File verification completed</source>
        <translation>文件校验完成</translation>
    </message>
    <message>
        <location filename="MiddleWidget/TransFileWgt.cpp" line="150"/>
        <source>File Recv failed</source>
        <translation>接收失败</translation>
    </message>
    <message>
        <location filename="MiddleWidget/TransFileWgt.cpp" line="155"/>
        <source>File sending failed</source>
        <translation>发送失败</translation>
    </message>
</context>
</TS>
