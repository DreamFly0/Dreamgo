﻿#ifndef BASEDWIDGET_H
#define BASEDWIDGET_H

#include <QWidget>

class BasedWidget:public QWidget
{
public:
    explicit BasedWidget(QWidget *parent = 0);
    ~BasedWidget(){}
protected:
    void mousePressEvent(QMouseEvent*);
    void mouseReleaseEvent(QMouseEvent*);
    void mouseMoveEvent(QMouseEvent*);
    void paintEvent(QPaintEvent*);
};

#endif // BASEDWIDGET_H
