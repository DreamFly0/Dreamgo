﻿#include "ExitDlg.h"
#include "LoginTitleBar.h"
#include <QDebug>
#pragma execution_character_set("utf-8")

ExitDlg::ExitDlg(QWidget *parent)
    : CBaseDialog(parent),
      m_labelIcon(this),
      m_labelMsgContent(this),
      m_box(this),
      m_btnMin(tr("最小化"),this),
      m_btnExit(tr("退出"),this)
{
    this->setFixedSize(380,200);
    this->setAttribute(Qt::WA_DeleteOnClose,false); //由于该对话框不需要parent，需要设置这个属性自动销毁
    //设置窗口背景
    this->setAutoFillBackground(true); // 这句要加上, 否则可能显示不出背景图.
    QPalette palette = this->palette();
    palette.setBrush(QPalette::Window,QBrush(QColor("#f2f1f1")));             // 使用平滑的缩放方式
    setPalette(palette);

    //设置标题背景
    m_pTitleBar->getMinimizeBtn()->hide();
    m_pTitleBar->setAutoFillBackground(true); // 这句要加上, 否则可能显示不出背景图.
    QPalette palette1 = m_pTitleBar->palette();
    palette1.setBrush(QPalette::Window,
                      QBrush(QPixmap(":/Images/resource/titleBg.png").scaled(// 缩放背景图.
                      this->size(),
                      Qt::IgnoreAspectRatio,
                      Qt::SmoothTransformation)));             // 使用平滑的缩放方式

    m_pTitleBar->setPalette(palette1);
    m_pTitleBar->setWinTitle(tr("退出提示"));

    //界面
    m_labelIcon.setFixedSize(QSize(48,48));
    m_labelIcon.setAlignment(Qt::AlignCenter);
    m_labelIcon.setPixmap(QPixmap(":/Images/resource/ic_question.png"));
    m_labelIcon.move(50,45);

    //m_labelMsgContent.setWordWrap(true);              //自动换行
    m_labelMsgContent.setFixedSize(250,30);
    m_labelMsgContent.setObjectName("labelMsgContent");
    m_labelMsgContent.setStyleSheet(QString("QLabel {font: 18px;}"));
    m_labelMsgContent.setText(tr("退出程序还是最小化到托盘？"));
    m_labelMsgContent.move(110,55);
    m_box.setText(tr("下次不再提示"));
    m_box.setFixedSize(200,30);
    m_box.move(60,100);

    m_btnMin.setFixedSize(80,30);
    m_btnExit.setFixedSize(80,30);
    m_btnMin.move(60,150);
    m_btnExit.move(240,150);
    connect(&m_btnMin,&QPushButton::clicked,this,&ExitDlg::SltClicked);
    connect(&m_btnExit,&QPushButton::clicked,this,&ExitDlg::SltClicked);
}

ExitDlg::~ExitDlg()
{
    qDebug() << "ExitDlg::~ExitDlg()";
}

void ExitDlg::SltClicked()
{
    this->close();
    if(sender() == &m_btnMin)
    {
        //qDebug() << "&m_btnMin";
        Q_EMIT sigMainWinHide(true);
    }else
    {
        //qDebug() << "&m_btnHide";
        Q_EMIT sigMainWinHide(false);
    }
    //qDebug() << "ExitDlg::SltClicked end";
}
