﻿#ifndef EXITDLG_H
#define EXITDLG_H

#include <QWidget>
#include <QLabel>
#include <QCheckBox>
#include <QPushButton>

#include "CustomWidget.h"
class MainWindow;

class ExitDlg : public CBaseDialog
{
    Q_OBJECT
public:
    friend class MainWindow;
    explicit ExitDlg(QWidget *parent = 0);
    ~ExitDlg();
private:
    QLabel      m_labelIcon;
    QLabel      m_labelMsgContent;
    QCheckBox   m_box;
    QPushButton m_btnMin;
    QPushButton m_btnExit;
signals:
    void sigMainWinHide(bool bMainWindowHide);
public slots:
    void SltClicked();
};

#endif // EXITDLG_H
