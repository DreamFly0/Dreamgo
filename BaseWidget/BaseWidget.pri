HEADERS += \
    $$PWD/AnimationStackedWidget.h \
    $$PWD/HeadButton.h \
    $$PWD/AbsFrameLessAutoSize.h \
    $$PWD/BaseWindow.h \
    $$PWD/BasedWidget.h \
    $$PWD/ExitDlg.h \
    $$PWD/MyTextEdit.h \
    $$PWD/MessageBox.h \
    $$PWD/messageitem.h \
    $$PWD/CustomWidget.h

SOURCES += \
    $$PWD/AnimationStackedWidget.cpp \
    $$PWD/HeadButton.cpp \
    $$PWD/AbsFrameLessAutoSize.cpp \
    $$PWD/BaseWindow.cpp \
    $$PWD/BasedWidget.cpp \
    $$PWD/ExitDlg.cpp \
    $$PWD/MyTextEdit.cpp \
    $$PWD/MessageBox.cpp \
    $$PWD/messageitem.cpp \
    $$PWD/CustomWidget.cpp

FORMS += \
    $$PWD/messageitem.ui
