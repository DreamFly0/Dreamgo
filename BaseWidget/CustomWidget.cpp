﻿#include "CustomWidget.h"
#include "LoginTitleBar.h"

#include <QMouseEvent>
#include <QPoint>
#include <QDebug>
#include <QPainter>
#include <QScrollBar>
/////////////////////////////////////////////////////////////////////////////////////////
/// \brief stackButton::stackButton
/// \param pixnormal
/// \param pixhover
/// \param pixsel
/// \param parent
///
stackButton::stackButton(const QString& pixnormal,const QString& pixhover,const QString& pixsel,QWidget*parent)
    :QPushButton(parent)
    ,m_pixnormal(pixnormal)
    , m_pixhover(pixhover)
    ,m_pixselected(pixsel)
    ,m_index(-1)
    ,m_enter(false)
    ,m_pressed(false)
{
    setCursor(Qt::PointingHandCursor);
    this->setMouseTracking(true);
    setFlat(true);
    setStyleSheet("QPushButton{background:transparent;}");
}
void stackButton::paintEvent(QPaintEvent *e)
{
    QPushButton::paintEvent(e);
    QPainter p(this);
    if(!m_enter&&!m_pressed)//初始化
    p.drawPixmap((width()-m_pixnormal.width())/2,(height()-m_pixnormal.height())/2,m_pixnormal);

    if(m_enter&&!m_pressed)//未选中进入
     p.drawPixmap((width()-m_pixhover.width())/2,(height()-m_pixhover.height())/2,m_pixhover);

    if(m_pressed)//选中
     p.drawPixmap((width()-m_pixselected.width())/2,(height()-m_pixselected.height())/2,m_pixselected);
}
void stackButton::setselected(bool sel)//用于控制pix显示
{
    m_pressed=sel;
    update();
}
void stackButton::mousePressEvent(QMouseEvent *e)
{
    QPushButton::mousePressEvent(e);
    if(e->button()==Qt::LeftButton)
    {
        m_pressed=true;
        update();
    }
}
void stackButton::enterEvent(QEvent *e)
{
    QPushButton::enterEvent(e);
    m_enter=true;
    update();
}
void stackButton::leaveEvent(QEvent *e)
{
    QPushButton::leaveEvent(e);
    m_enter=false;
    update();
}

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief myPushButton::myPushButton
/// \param parent
///
myPushButton::myPushButton(QWidget*parent):QPushButton(parent)
{
    setCursor(Qt::PointingHandCursor);
    setFlat(true);
    setStyleSheet("QPushButton{background:transparent;}");
}
myPushButton::myPushButton(const QString& str, QWidget *parent):QPushButton(str,parent)
{
     setCursor(Qt::PointingHandCursor);
     setFlat(true);
     setStyleSheet("QPushButton{background:transparent;}");
}

//////////////////////////////////////////////////////////////////////////////////////////
/// \brief CustomWidget::CustomWidget
/// \param parent
///
CustomWidget::CustomWidget(QWidget *parent) : QWidget(parent)
{
    m_pTitleBar = new LoginTitleBar("标题栏",this);
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief CBaseDialog::CBaseDialog
/// \param parent
///
CBaseDialog::CBaseDialog(QWidget *parent) :
    QDialog(parent),
    m_bMousePress(false)
{
    this->setWindowFlags(Qt::FramelessWindowHint);
    //this->setMinimumSize(300, 150);

    m_nNormalSize = QSize(300, 170);
    this->setAttribute(Qt::WA_DeleteOnClose);
    setWindowIcon(QIcon(":/Images/resource/flamingo.ico"));
    setFixedSize(m_nNormalSize);
    setMinimumSize(0, 0);

    m_pTitleBar = new LoginTitleBar("标题栏",this);
    m_pBodyWidget = new QWidget(this);

    //设置窗口背景
    this->setAutoFillBackground(true); // 这句要加上, 否则可能显示不出背景图.
    QPalette palette = this->palette();
    palette.setColor(QPalette::Background,Qt::white);
    this->setPalette(palette);

    QVBoxLayout *verLayoutWindow = new QVBoxLayout(this);
    verLayoutWindow->setContentsMargins(0,0,0,0);
    verLayoutWindow->setSpacing(0);
    verLayoutWindow->addWidget(m_pTitleBar);
    verLayoutWindow->addWidget(m_pBodyWidget, 1);

    connect(m_pTitleBar->getMinimizeBtn(), &Button::clicked, this, &CBaseDialog::hide);
    connect(m_pTitleBar->getCloseBtn(), &Button::clicked, this, &CBaseDialog::close);
}

void CBaseDialog::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton){
         m_windowPos = this->pos();
         m_mousePos = event->globalPos();
         m_bMousePress = true;
    }
}

void CBaseDialog::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
        m_bMousePress = false;
}

void CBaseDialog::mouseMoveEvent(QMouseEvent *event)
{
    if (m_bMousePress == true){
        move(m_windowPos + (event->globalPos() - m_mousePos));
    }
}

//////////////////////////////////////////////////////////////////////
/// \brief CMessageBox::CMessageBox
/// \param parent
///
CMessageBox::CMessageBox(const QString &title, QWidget *parent):CBaseDialog(parent)
{
    m_pTitleBar->setWinTitle(title);

    m_pLabelIcon = new QLabel(m_pBodyWidget);
    m_pLabelIcon->setFixedSize(QSize(64,64));
    m_pLabelIcon->setAlignment(Qt::AlignCenter);
    m_pLabelIcon->setPixmap(QPixmap(":/Images/resource/ic_info.png"));

    m_pLabelMsgContent = new QLabel(m_pBodyWidget);
    m_pLabelMsgContent->setWordWrap(true);              //自动换行
    m_pLabelMsgContent->setMinimumHeight(64);
    m_pLabelMsgContent->setObjectName("labelMsgContent");
    m_pLabelMsgContent->setStyleSheet(QString("QLabel {font: 18px;}"));
    m_pLabelMsgContent->setText(tr("Test text！"));

    QHBoxLayout *horLayoutContent = new QHBoxLayout();
    horLayoutContent->setContentsMargins(2, 2, 2, 2);
    horLayoutContent->setSpacing(10);
    horLayoutContent->addWidget(m_pLabelIcon);
    horLayoutContent->addWidget(m_pLabelMsgContent, 1);

    m_pBtnCancel = new QPushButton(m_pBodyWidget);
    m_pBtnCancel->setText(tr("Cancel"));

    m_pBtnOk = new QPushButton(m_pBodyWidget);
    m_pBtnOk->setText(tr("OK"));

    QHBoxLayout *horLayoutBtn = new QHBoxLayout();
    horLayoutBtn->setContentsMargins(2, 2, 2, 2);
    horLayoutBtn->setSpacing(10);
    horLayoutBtn->addStretch();
    horLayoutBtn->addWidget(m_pBtnCancel);
    horLayoutBtn->addWidget(m_pBtnOk);

    QVBoxLayout *verLayout = new QVBoxLayout(m_pBodyWidget);
    verLayout->setContentsMargins(6, 6, 6, 6);
    verLayout->addLayout(horLayoutContent, 1);
    verLayout->addLayout(horLayoutBtn);

    connect(m_pBtnCancel, &QPushButton::clicked, this, &QDialog::reject);
    connect(m_pBtnOk,  &QPushButton::clicked, this, &QDialog::accept);

    //倒计时定时器
    m_nTimerCnt = 10;

    m_pTimer = new QTimer(this);
    connect(m_pTimer,&QTimer::timeout,this,&CMessageBox::SltTimerOut);
}

CMessageBox::~CMessageBox()
{
    qDebug() << "CMessageBox::~CMessageBox()";
}

void CMessageBox::ShowMessage(const QString &content, const quint8 &msgType)
{
    if (content.isEmpty()) return;
    m_pLabelMsgContent->setText(content);

    m_pBtnCancel->setVisible(CMessageBox::E_Question == msgType);

    if (CMessageBox::E_Information == msgType)
        m_pLabelIcon->setPixmap(QPixmap(":/Images/resource/ic_info.png"));
    else if (CMessageBox::E_Warning == msgType)
        m_pLabelIcon->setPixmap(QPixmap(":/Images/resource/ic_warning.png"));
    else if (CMessageBox::E_Question == msgType)
        m_pLabelIcon->setPixmap(QPixmap(":/Images/resource/ic_question.png"));
    else
        m_pLabelIcon->setPixmap(QPixmap(":/Images/resource/ic_info.png"));
}

/**
 * @brief CMessageBox::StartTimer   启动倒计时
 */
void CMessageBox::StartTimer()
{
    if (m_pTimer->isActive()) m_pTimer->stop();
    m_pTimer->start(1000);
}

/**
 * @brief CMessageBox::Infomation
 * @param parent
 * @param content
 * @param title
 * @return
 */
int CMessageBox::Infomation(const QString &content, const QString &title)
{
    // 创建对话框
    CMessageBox *messageBox = new CMessageBox(title);
    messageBox->ShowMessage(content, CMessageBox::E_Information);
    messageBox->StartTimer();
    messageBox->SltTimerOut();
    return messageBox->exec();
}

int CMessageBox::Question(const QString &content, const QString &title)
{
    // 创建对话框
    CMessageBox *messageBox = new CMessageBox(title);
    messageBox->ShowMessage(content, CMessageBox::E_Question);
    return messageBox->exec();
}

int CMessageBox::Warning(const QString &content, const QString &title)
{
    // 创建对话框
    CMessageBox *messageBox = new CMessageBox(title);
    messageBox->ShowMessage(content, CMessageBox::E_Warning);
    return messageBox->exec();
}

void CMessageBox::SltTimerOut()
{
    m_pBtnOk->setText(tr("OK(%1)").arg(m_nTimerCnt--));
    if(m_nTimerCnt < 0)
    {
        m_pTimer->stop();
        this->accept();
    }
}

//////////////////////////////////////////////////////////////////////////////////
/// \brief MyScrollArea::MyScrollArea
/// \param parent
///
MyScrollArea::MyScrollArea(QWidget* parent)
    : QScrollArea(parent),
    mMoveStart(false)
{
    installEventFilter(this);
}

bool MyScrollArea::eventFilter(QObject *obj, QEvent *evt)
{
    qDebug() << "MyScrollArea::eventFilter1";
    if (evt->type() == QEvent::MouseMove) {
        QMouseEvent* event = (QMouseEvent*) evt;
        if ((event->buttons() & Qt::LeftButton)) {
            qDebug() << "MyScrollArea::eventFilter";
            if (!mMoveStart) {

                mMoveStart = true;

                mContinousMove =false;

                mMousePoint = event->globalPos();
            }
            else {
                QScrollBar* scrollBar = verticalScrollBar();

                QPoint p = event->globalPos();

                int offset = p.y() - mMousePoint.y();
                qDebug() << "OFFSET = " << offset;
                qDebug() << "scrollBar->value()" << scrollBar->value();
                if( !mContinousMove && ( offset > -10 && offset < 10 ) )
                    return false;

                mContinousMove = true;

                scrollBar->setValue(scrollBar->value() - offset);

                mMousePoint = p;
            }
            return true;
        }
    }
    else if (evt->type() == QEvent::MouseButtonRelease) {
        mMoveStart = false;
    }

    return QObject::eventFilter(obj, evt);
}
