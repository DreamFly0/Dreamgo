﻿#include "BasedWidget.h"
#include <QStyleOption>
#include <QPainter>
#include <QDebug>
#include <QMouseEvent>

#include "AbsFrameLessAutoSize.h"
#include "MainWindow.h"

BasedWidget::BasedWidget(QWidget *parent) : QWidget(parent)
{
    //setStyleSheet("QWidget{background:transparent;}");
    setMouseTracking(true);
}

void BasedWidget::paintEvent(QPaintEvent *e)
{
    Q_UNUSED(e);
    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}

void BasedWidget::mousePressEvent(QMouseEvent *e)
{
    QWidget::mousePressEvent(e);
}

void BasedWidget::mouseMoveEvent(QMouseEvent *e)
{
    //qDebug() << "BasedWidget::mouseMoveEvent";
    QWidget::mouseMoveEvent(e);
    if(MainWindow::GetInstance())
    {
        MainWindow::GetInstance()->mouseMoveEvent(e);
    }
}

void BasedWidget::mouseReleaseEvent(QMouseEvent *e)
{
    QWidget::mouseReleaseEvent(e);
}
