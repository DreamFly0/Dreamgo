﻿#include "HeadButton.h"
#include <QPainter>
#include <QRect>
#include <QPaintEvent>
#include <math.h>
#include <QDebug>

HeadButton::HeadButton(QString FileName, int num, QWidget *parent):
    Button(FileName, num, parent),
    m_Background(":/Images/resource/head/0.png")
{
}

HeadButton::HeadButton(QVector<QString> &list, QWidget *parent):
    Button(list,parent),
    m_Background(":/Images/resource/head/0.png")
{

}

HeadButton::~HeadButton()
{
}

void HeadButton::paintEvent ( QPaintEvent * event)
{
    QPainter painter(this);
    QRect rect = event->rect();
    //qDebug() << rect;
    painter.drawPixmap(rect.x()+4,rect.y()+4,rect.width()-8,rect.height()-8, m_Background);

    Button::paintEvent(event);
}
