﻿#ifndef MESSAGEITEM_H
#define MESSAGEITEM_H

#include <QWidget>
#include "qqheadwidget.h"
#include "QQCell.h"

/*
#define HEAD_WIDGET_WIDTH 50				// 头像宽度;

// 自定义头像控件;
class QQHeadWidget : public QWidget
{
public:
    QQHeadWidget(QWidget* parent = nullptr);
protected:
    void QQHeadWidget::paintEvent(QPaintEvent *event);
private:
};
*/
#define ITEM_HEIGTH 75
namespace Ui {
class MessageItem;
}

class MessageItem : public QWidget
{
    Q_OBJECT

public:
    explicit MessageItem(QWidget *parent = nullptr);
    ~MessageItem();

private:
    void initUi();
    Ui::MessageItem *ui;
};

#endif // MESSAGEITEM_H
