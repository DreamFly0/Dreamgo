﻿#include "MessageBox.h"
#include <QListWidget>
#include <QIcon>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QGraphicsDropShadowEffect>
#include <QPainter>
#include <QColor>
#include <QPushButton>
#include <QHBoxLayout>
#include <QToolButton>
#include <QCursor>

#include "messageitem.h"
#include "QQCell.h"

#define Margin       4
#define SHADOW_WIDTH    12
QQMessageBox::QQMessageBox(QWidget *parent) : QWidget(parent)
{
    this->initUi();
}

QQMessageBox::~QQMessageBox()
{

}

void QQMessageBox::paintEvent(QPaintEvent *evt)
{
    Q_UNUSED(evt);
    QPainter painter(this);
    painter.setPen(Qt::blue);
    painter.setFont(QFont("Arial", 30));
    painter.drawText(rect(), Qt::AlignCenter, "Qt");
}

void QQMessageBox::initUi()
{
    //窗体基本属性设置
    this->setAttribute(Qt::WA_TranslucentBackground);
    this->setWindowIcon(QIcon(":/Images/resource/flamingo.ico"));
    this->setWindowFlags(windowFlags() | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
    this->setFixedWidth(450);
    m_MainWin.setAutoFillBackground(true);
    m_MainWin.setMouseTracking(true);
    QGridLayout *lyout=new QGridLayout;
    lyout->addWidget(&m_MainWin);
    lyout->setMargin(Margin);
    setLayout(lyout);

    //设置边框阴影，没有用paintevent，感觉这个效果比paintevent好
    QGraphicsDropShadowEffect* shadowEffect = new QGraphicsDropShadowEffect(this);
    shadowEffect->setOffset(0, 0);
    shadowEffect->setColor(QColor(0,0,0,150));
    shadowEffect->setBlurRadius(SHADOW_WIDTH);
    m_MainWin.setGraphicsEffect(shadowEffect);

    //框体元素布局
    m_pTitle = new QWidget;
    m_pBottomWgt = new QWidget;
    m_plistView = new QListWidget;
    m_plistView->setFixedHeight(0);
    m_pTitle->setFixedHeight(50);
    m_pBottomWgt->setFixedHeight(50);
    m_plistView->setFrameShape(QFrame::NoFrame);
    m_plistView->setMinimumHeight(100);
    m_pBottomWgt->setStyleSheet("border-top: 2px solid rgb(200,200,200);");
    QVBoxLayout *vlyout = new QVBoxLayout;
    vlyout->setMargin(0);
    vlyout->setSpacing(0);
    vlyout->addWidget(m_pTitle);
    vlyout->addWidget(m_plistView);
    vlyout->addWidget(m_pBottomWgt);
    m_MainWin.setLayout(vlyout);

    QPalette palette(m_pTitle->palette());
    m_pTitle->setAutoFillBackground(true); // 这句要加上, 否则可能显示不出背景图.
    palette.setColor(QPalette::Background,Qt::white);
    m_pTitle->setPalette(palette);
    palette = m_pBottomWgt->palette();
    m_pBottomWgt->setAutoFillBackground(true); // 这句要加上, 否则可能显示不出背景图.
    palette.setColor(QPalette::Background,Qt::white);
    m_pBottomWgt->setPalette(palette);
    //标题栏
    m_pName = new QPushButton();
    m_pConfig = new QToolButton();
    m_pConfig->setFixedSize(40,40);
    m_pConfig->setIcon(QIcon(":/Images/resource/config.png"));
    m_pConfig->setObjectName("ToolBtn");
    m_pName->setText("张三");
    m_pName->setCursor(Qt::PointingHandCursor);
    m_pName->setStyleSheet("text-align : left;border:none;font: bold 20px;background-color: transparent;");
    QHBoxLayout *hlyouttitle = new QHBoxLayout;
    hlyouttitle->setContentsMargins(5,0,5,0);
    hlyouttitle->setSpacing(0);
    hlyouttitle->addWidget(m_pName);
    hlyouttitle->addStretch();
    hlyouttitle->addWidget(m_pConfig);
    m_pTitle->setLayout(hlyouttitle);
    //底部按钮
    m_pbtnIgnore = new QPushButton;
    m_pbtnLook = new QPushButton;
    m_pbtnIgnore->setStyleSheet("border: 0px;");
    m_pbtnLook->setStyleSheet("border: 0px;");
    m_pbtnIgnore->setCursor(Qt::PointingHandCursor);
    m_pbtnLook->setCursor(Qt::PointingHandCursor);
    m_pbtnIgnore->setObjectName("MsgBoxBtn");
    m_pbtnLook->setObjectName("MsgBoxBtn");
    m_pbtnIgnore->setText("忽略全部");
    m_pbtnLook->setText("查看全部");
    QHBoxLayout *hlyoutBottom = new QHBoxLayout;
    hlyoutBottom->setContentsMargins(5,0,5,0);
    hlyoutBottom->setSpacing(0);
    hlyoutBottom->addWidget(m_pbtnIgnore);
    hlyoutBottom->addStretch();
    hlyoutBottom->addWidget(m_pbtnLook);
    m_pBottomWgt->setLayout(hlyoutBottom);
}

//设置高度
void QQMessageBox::setAutoLyout()
{
    int height = 0;
    for(int i = 0; i<m_plistView->count();i++)
    {
        height += ITEM_HEIGTH;       //每一个item的行高
    }
    m_plistView->setMinimumHeight(height);
}

void QQMessageBox::updateMsg(int userid, QString msg)
{
    QQCellChild* user = nullptr;
    bool flag = false;
    foreach( user , m_msgList )
    {
        if(userid == user->m_id)
        {
            flag = true;
        }
    }

    if(!flag)
    {

    }

    setAutoLyout();
}
