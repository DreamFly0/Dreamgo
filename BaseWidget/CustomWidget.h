﻿#ifndef CUSTOMWIDGET_H
#define CUSTOMWIDGET_H

#include <QWidget>
#include <QDialog>
#include <QTimer>
#include <QPushButton>
#include <QScrollArea>
#include <QPoint>
class QLabel;
class LoginTitleBar;
///
/// \brief The myPushButton class
///
class myPushButton : public QPushButton
{
    Q_OBJECT
public:
    myPushButton(QWidget*parent=0);
    myPushButton(const QString& str,QWidget*parent=0);
};

class CustomWidget : public QWidget
{
    Q_OBJECT
public:
    explicit CustomWidget(QWidget *parent = 0);
    LoginTitleBar       *m_pTitleBar;             //标题栏
signals:

public slots:
};
///
/// \brief The stackButton class
///
class stackButton:public QPushButton
{
    Q_OBJECT
public:
    explicit stackButton(const QString& pixnormal,const QString& pixhover,const QString& pixsel,QWidget*parent);
    void setselected(bool=true);
protected:
    void mousePressEvent(QMouseEvent *e);
    void enterEvent(QEvent *);
    void leaveEvent(QEvent *);
    void paintEvent(QPaintEvent *);
private:
    QPixmap m_pixnormal;
    QPixmap m_pixhover;
    QPixmap m_pixselected;

    int m_index;
    bool m_enter;
    bool m_pressed;
};

/// \brief The CMessageBox class
/// 只有一个标题栏的对话框
class CBaseDialog : public QDialog
{
    Q_OBJECT
public:
    explicit CBaseDialog(QWidget *parent = 0);
    ~CBaseDialog(){}
protected:
    void mousePressEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
private:
    QSize               m_nNormalSize;

    bool                m_bMousePress;
    QPoint              m_windowPos;
    QPoint              m_mousePos;
protected:
    LoginTitleBar       *m_pTitleBar;             //标题栏
    QWidget             *m_pBodyWidget;
};

/// \brief The CMessageBox class
/// 弹出信息框类
class CMessageBox : public CBaseDialog
{
    Q_OBJECT
public:
    typedef enum {
        E_Information =  0x01,
        E_Warning,
        E_Question,
        E_MSGTYPE_Error,
    } E_MSGTYPE;

public:
    explicit CMessageBox(const QString &title,QWidget *parent = 0);
    ~CMessageBox();

    // 显示消息
    void ShowMessage(const QString &content, const quint8 &msgType = CMessageBox::E_Information);

    void StartTimer();

    static int Infomation(const QString &content, const QString &title = tr("Infomation"));
    static int Question(const QString &content, const QString &title = tr("Question"));
    static int Warning(const QString &content, const QString &title = tr("Warning"));
protected:

private:
    QLabel      *m_pLabelIcon;
    QLabel      *m_pLabelMsgContent;

    QPushButton *m_pBtnOk;
    QPushButton *m_pBtnCancel;

    QTimer      *m_pTimer;
    int         m_nTimerCnt;
public slots:
    void SltTimerOut();
};

class MyScrollArea : public QScrollArea
{
//这样就可以使用这个类来做窗口的parent，这样里面的内容就支持拖动了。

Q_OBJECT
public:

    MyScrollArea(QWidget* parent =NULL);
    ~MyScrollArea(){}
protected:
    bool eventFilter(QObject *obj, QEvent *evt);
    private:
    bool mMoveStart;
    bool mContinousMove;
    QPoint mMousePoint;
 };
#endif // CUSTOMWIDGET_H
