﻿#ifndef LOGINBUTTON_H
#define LOGINBUTTON_H

#include "Button.h"
#include <QPixmap>
#include <QVector>

class HeadButton : public Button
{
public:
    HeadButton(QString FileName, int num  = 1, QWidget *parent = 0);
    HeadButton(QVector<QString>& list,QWidget* parent = 0);
    virtual ~HeadButton();

    void setBackGround(const QPixmap& bg){m_Background = bg; update();}
    const QPixmap getBackGround(){return m_Background;}

protected:
    virtual void paintEvent ( QPaintEvent * event);
private:
    QPixmap m_Background;
};

#endif // LOGINBUTTON_H
