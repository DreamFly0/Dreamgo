﻿#ifndef MESSAGEBOX_H
#define MESSAGEBOX_H

#include <QWidget>
#include <QFrame>

#include "BaseWindow.h"
class QListWidget;
class QPushButton;
class QToolButton;
class QQCellChild;

class QQMessageBox : public QWidget
{
    Q_OBJECT
public:
    explicit QQMessageBox(QWidget *parent = nullptr);
    ~QQMessageBox();
protected:
    virtual void paintEvent(QPaintEvent *evt);
private:
    void initUi();
    void setAutoLyout();
    void updateMsg(int userid,QString msg);
private:
    Widget              m_MainWin;
    QWidget             *m_pTitle;
    QWidget             *m_pBottomWgt;
    QListWidget         *m_plistView;
    QPushButton         *m_pName;
    QToolButton         *m_pConfig;
    QPushButton         *m_pbtnIgnore;
    QPushButton         *m_pbtnLook;
    QList<QQCellChild*> m_msgList;
signals:

public slots:
};

#endif // MESSAGEBOX_H
