﻿#include "BaseWindow.h"
#include "global.h"
#include <QPixmap>
#include <QPainter>
#include <QGridLayout>
#include <QPainterPath>
#include <QColor>
#include <qmath.h>
#include <QBitmap>
#include <QGraphicsDropShadowEffect>
#include <QIcon>

#define SHADOW_WIDTH 12
#define Margin       4
BaseWindow::BaseWindow(QWidget *parent)
    : AbsFrameLessAutoSize(parent),
      m_MainWin(this),
      m_bmaxSize(false)
{
    QSize size = myHelper::GetScreenInfo();
    QSize sizeoffset(Margin*2,Margin*2);
    this->setMaximumSize(size+sizeoffset);

    setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    this->setAttribute(Qt::WA_TranslucentBackground);
    this->setWindowIcon(QIcon(":/Images/resource/flamingo.ico"));

    m_MainWin.setAutoFillBackground(true);
    m_MainWin.setMouseTracking(true);
    QGridLayout *lyout=new QGridLayout;
    lyout->addWidget(&m_MainWin);
    lyout->setMargin(Margin);
    setLayout(lyout);
/*
    //设置边框阴影，没有用paintevent，感觉这个效果比paintevent好
    QGraphicsDropShadowEffect* shadowEffect = new QGraphicsDropShadowEffect(this);
    shadowEffect->setOffset(0, 0);
    shadowEffect->setColor(QColor(0,0,0,150));
    shadowEffect->setBlurRadius(SHADOW_WIDTH);
    m_MainWin.setGraphicsEffect(shadowEffect);
*/
}

//画出边框阴影
void BaseWindow::paintEvent(QPaintEvent *evt)
{
    AbsFrameLessAutoSize::paintEvent(evt);

    QPainter painter(this);
 // draw shadow margin

    QBitmap bmp(this->size());
    bmp.fill();
    QPainter p(&bmp);
    p.setPen(Qt::NoPen);
    p.setBrush(Qt::black);
    p.drawRoundedRect(bmp.rect(),8,8);
    setMask(bmp);

    QPainterPath path;
    path.setFillRule(Qt::WindingFill);
    //path.addRect(9, 9, this->width()-18, this->height()-18);
    path.addRect(6, 6, this->width()-12, this->height()-12);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.fillPath(path, QBrush(Qt::white));


    QColor color(0, 0, 0);
    for(int i=0; i<6; i++)
    {
        QPainterPath path;
        path.setFillRule(Qt::WindingFill);
        path.addRect(6-i, 6-i, this->width()-(6-i)*2, this->height()-(6-i)*2);
        color.setAlpha(70- i*5);
        painter.setPen(color);
        painter.drawPath(path);
    }
}

//因为加了边框阴影，实际外边框透明，要避开这个区域吧窗口最大化
void BaseWindow::SltShowMaxSize()
{
    static QRect rect;
    if(m_bmaxSize)
    {
        m_bmaxSize = false;
        this->resize(rect.size());
        this->move(rect.topLeft());
        m_isForbidDrag = false;
    }else
    {
        m_bmaxSize = true;
        rect = this->geometry();
        this->move(-Margin,-Margin);
        this->resize(maximumSize());
        m_isForbidDrag = true;
    }
}

///////////////////////////////////////////////////////////////////////
/// \brief Widget::Widget
/// \param parent
///
Widget::Widget(QWidget *parent)
    :BasedWidget(parent),
      m_skinPic(":/Images/resource/skin/M4A1.jpg")
{
    this->setObjectName("BaseMain");
    m_bIsShowBG=true;
}

void Widget::setCurBGPic(const QString &strPix)
{
    m_skinPic.load(strPix);
    update();
}

void Widget::setSkin(const QString &strPix)
{
    m_skinPic.load(strPix);
    update();
}

void Widget::clearBg()
{
    m_bIsShowBG=false;
    m_skinPic.load("");
    update();
}

void Widget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    //窗口设置圆角
    QBitmap bmp(this->size());
    bmp.fill();
    QPainter p(&bmp);
    p.setPen(Qt::NoPen);
    p.setBrush(Qt::black);
    p.drawRoundedRect(bmp.rect(),2,2);
    setMask(bmp);
/*
    if(m_bIsShowBG)
    {
        float d =(float)m_skinPic.height()/m_skinPic.width();
        int h=d*width();
        int w=height()/d;
        if(h<height())//如果图片高度小于窗口高度
        {
             painter.drawPixmap(0,0,w,height(),m_skinPic);
             return;
        }
        painter.drawPixmap(0,0,width(),h,m_skinPic);
    }
    */
}
