﻿#include "messageitem.h"
#include "ui_messageitem.h"

#include <QPainter>
/*
QQHeadWidget::QQHeadWidget(QWidget* parent)
    : QWidget(parent)
{
    this->setFixedSize(QSize(HEAD_WIDGET_WIDTH, HEAD_WIDGET_WIDTH));
    this->setAttribute(Qt::WA_TranslucentBackground);
}

void QQHeadWidget::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    QRect headRect = QRect(0, 0, HEAD_WIDGET_WIDTH, HEAD_WIDGET_WIDTH);
    painter.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
    // 绘制圆形头像;
    QPainterPath path;
    path.addEllipse(headRect);
    painter.setClipPath(path);
    painter.drawPixmap(headRect, QPixmap(":/Images/resource/head/0.png"));
    // 绘制圆形边框;
    //painter.setPen(QPen(QColor(255, 255, 255, 200), 4));
    //painter.drawRoundedRect(headRect, headRect.width() / 2, headRect.height() / 2);

    return QWidget::paintEvent(event);
}
*/
MessageItem::MessageItem(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MessageItem)
{
    this->setMouseTracking(true);
    ui->setupUi(this);
    //ui->widget->setFixedSize(QSize(48, 48));
    this->setFixedHeight(ITEM_HEIGTH);
    initUi();
}

MessageItem::~MessageItem()
{
    delete ui;
}

void MessageItem::initUi()
{
    ui->labelName->setStyleSheet("font: '黑体';");
    ui->labelsign->setStyleSheet("font:'黑体';color:rgb(100,100,100);");
    ui->labelCnt->setStyleSheet("font:16px;color:white;background:#82d2e6;border-radius: 12px;");
}
