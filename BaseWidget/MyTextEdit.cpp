﻿#include "MyTextEdit.h"
#include <QMenu>
#include <QContextMenuEvent>
#include <QClipboard>
#include <QApplication>
#include <QString>
#include <QMimeData>

#include <QDebug>
#include <QMovie>

MyTextEdit::MyTextEdit(QWidget *parent) : QTextEdit(parent)
{
    initMunu();
    //this->setStyleSheet("font-family: '微软雅黑'; font: 16pt");
}

MyTextEdit::~MyTextEdit()
{
    QHash<QMovie*, QUrl>::const_iterator i;
    for (i = m_hasUrls.constBegin(); i != m_hasUrls.constEnd(); ++i)
    {
        delete i.key();
    }
    qDebug() << "MyTextEdit::~MyTextEdit()";
}

void MyTextEdit::contextMenuEvent(QContextMenuEvent *e)
{
    if(canPaste())
    {
        m_pPaste->setEnabled(true);
    }else
    {
        m_pPaste->setEnabled(false);
    }
     m_pMenu->exec(e->globalPos());
}


void MyTextEdit::initMunu()
{
    m_pMenu = new QMenu(this);
    m_pCut = new QAction(tr("Cut(&T)"),this);
    m_pCopy = new QAction(tr("Copy(&C)"),this);
    m_pPaste = new QAction(tr("Paste(&P)"),this);
    m_pSelectAll = new QAction(tr("Select All(&A)"),this);
    m_pFont = new QAction(tr("Font(&F)"),this);

    m_pMenu->addAction(m_pCut);
    m_pMenu->addAction(m_pCopy);
    m_pMenu->addAction(m_pPaste);
    m_pMenu->addAction(m_pSelectAll);
    m_pMenu->addSeparator();
    m_pMenu->addAction(m_pFont);

    connect(m_pCut,&QAction::triggered,this,&MyTextEdit::SltCut);
    connect(m_pCopy,&QAction::triggered,this,&MyTextEdit::SltCopy);
    connect(m_pPaste,&QAction::triggered,this,&MyTextEdit::SltPaste);
    connect(m_pSelectAll,&QAction::triggered,this,&MyTextEdit::SltSelectAll);
    connect(m_pFont,&QAction::triggered,this,&MyTextEdit::SltFont);

    this->m_pCopy->setEnabled(false);
    this->m_pCut->setEnabled(false);
    this->m_pPaste->setEnabled(false);
    connect(this,&MyTextEdit::copyAvailable,[this](bool b){
        if(b){
            this->m_pCopy->setEnabled(b);
            this->m_pCut->setEnabled(b);
        }else
        {
            this->m_pCopy->setEnabled(b);
            this->m_pCut->setEnabled(b);
        }

    });
}

void MyTextEdit::SltSubAnimate(int)
{
    //使用QMovie中的当前帧替换掉富文本中的图片元素
    QMovie* movie = qobject_cast<QMovie*>(sender());
    //替换图片为当前帧
    document()->addResource(QTextDocument::ImageResource,m_hasUrls.value(movie),movie->currentPixmap());
    //刷新显示
    this->setLineWrapColumnOrWidth(lineWrapColumnOrWidth());
}

void MyTextEdit::SltAddEmotion(int faceid)
{
    QString&& file = QString("<img src=qrc:/face/resource/Face/%1.gif>").arg(faceid);

    //插入html描述的图片
    QTextCursor cursor = this->textCursor();
    cursor.insertHtml(file);
    //判断是否是相同的图片
    if(m_lstUrls.contains(faceid))
    {
        return;
    }else
    {
        m_lstUrls.append(faceid);
    }

    //创建QMovie以显示gif
    QMovie* movie = new QMovie(this);
    movie->setFileName(QString(":/face/resource/Face/%1.gif").arg(faceid));
    movie->setCacheMode(QMovie::CacheNone);

    m_hasUrls.insert(movie,QUrl(QString("qrc:/face/resource/Face/%1.gif").arg(faceid)));
    //绑定帧切换信号槽
    connect(movie,&QMovie::frameChanged,this,&MyTextEdit::SltSubAnimate);
    movie->start();
    this->setFocus();
}

void MyTextEdit::SltCut()
{
    this->cut();
}

void MyTextEdit::SltCopy()
{
    this->copy();
}

void MyTextEdit::SltPaste()
{
    this->paste();
}

void MyTextEdit::SltSelectAll()
{
    this->selectAll();
}

void MyTextEdit::SltFont()
{

}

