﻿#ifndef ABSFRAMELESSAUTOSIZE_H
#define ABSFRAMELESSAUTOSIZE_H

#include <QWidget>
#pragma execution_character_set("utf-8")
/**
 * @brief The AbsFrameLessAutoSize class
 * 无边框、鼠标自由拖动窗口、自由缩放大小的基类
 * @author: zrj
 */

#define PADDING 4   //可拖动部分边界宽度

enum Direction{
    UP = 0,
    DOWN=1,
    LEFT,
    RIGHT,
    LEFTTOP,
    LEFTBOTTOM,
    RIGHTBOTTOM,
    RIGHTTOP,
    NONE
};

class AbsFrameLessAutoSize : public QWidget
{
    Q_OBJECT
public:
    explicit AbsFrameLessAutoSize(QWidget *parent = 0);
    virtual ~AbsFrameLessAutoSize(){}
protected:
    void mousePressEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
protected:
    void region(const QPoint &cursorGlobalPoint);
protected:
    bool        m_bIsLeftPressDown;
    QPoint      m_dragPosition;
    Direction   m_dir;
    bool        m_isForbidDrag;             //用于basewindow最大化时禁止窗口拖动
signals:

public slots:
};
#endif // ABSFRAMELESSAUTOSIZE_H
