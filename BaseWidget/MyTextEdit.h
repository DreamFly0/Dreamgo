﻿#ifndef MYTEXTEDIT_H
#define MYTEXTEDIT_H

#include <QWidget>
#include <QTextEdit>
#include <QList>
#include <QHash>
class MyTextEdit : public QTextEdit
{
    Q_OBJECT
public:
    explicit MyTextEdit(QWidget *parent = 0);
    virtual ~MyTextEdit();

protected:
    void contextMenuEvent(QContextMenuEvent *e);
    void initMunu();
private:
    QMenu*                  m_pMenu;
    QAction*                m_pCut;
    QAction*                m_pCopy;
    QAction*                m_pPaste;
    QAction*                m_pSelectAll;
    QAction*                m_pFont;
    QList<int>              m_lstUrls;
    QHash<QMovie*, QUrl>    m_hasUrls;

signals:
private slots:
    void SltSubAnimate(int);
public slots:
    void SltAddEmotion(int faceid);
    void SltCut();
    void SltCopy();
    void SltPaste();
    void SltSelectAll();
    void SltFont();
};

#endif // MYTEXTEDIT_H
