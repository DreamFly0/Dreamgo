﻿#ifndef BASEWIDGET_H
#define BASEWIDGET_H

#include <QWidget>
#include "AbsFrameLessAutoSize.h"
#include "BasedWidget.h"

//实际没有用这里设置的这个图片单独做背景，最后还是直接设置了topWidget的背景其他都是白色
class Widget:public BasedWidget
{
    Q_OBJECT
public:
    explicit Widget(QWidget*parent=0);

    void setCurBGPic(const QString&);

    void clearBg();

    const QString currentSkinPath(){return m_curPixPath;}

    inline QPixmap getRectPix(const QRect&rect){update();return  m_skinPic.copy(rect);}
protected:
    virtual void paintEvent(QPaintEvent *);
 public slots:
    void setSkin(const QString &strPix);
private:
    QString m_curPixPath;
    QPixmap m_skinPic;
public:
    bool m_bIsShowBG;
};

class BaseWindow : public AbsFrameLessAutoSize
{
    Q_OBJECT
public:
    explicit BaseWindow(QWidget *parent = 0);
    virtual ~BaseWindow(){}
    void paintEvent(QPaintEvent *evt);
protected:
    bool        m_bmaxSize;
public:
    Widget m_MainWin;
    //BasedWidget m_Box;
signals:

protected slots:
    void SltShowMaxSize();
};

#endif // BASEWIDGET_H
