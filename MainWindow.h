﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "BasedWidget.h"
#include "BaseWindow.h"
#include <qmetatype.h>
#include <QJsonObject>
#include <QJsonDocument>
#include <QSystemTrayIcon>
#include <QTimer>
#include <QHash>

#include "unit.h"

class TopWidget;
class MiddleWidget;
class BottomWidget;
class ClientSocket;
class ExitDlg;
class RequestAdduserDlg;
class QQCellChild;
//这个类管理该组下好友的个人信息，最后也负责清理好友信息申请内存
class MainWindow:public BaseWindow
{
    Q_OBJECT
    friend class BasedWidget;
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    static MainWindow *InitInstance();
    static MainWindow *GetInstance(){return s_pMainWnd;}
    void setSocket(ClientSocket* socket);
    void showMainWin();
private:
    void initUi();
    void initConnect();
    void updateUserInfo();
    void InitSysTrayIcon();
    //void closeEvent(QCloseEvent *evt);
protected:
    virtual void closeEvent(QCloseEvent *e);
    static MainWindow *s_pMainWnd;
private:
    TopWidget*              m_pTopWidget;
    MiddleWidget*           m_pMiddleWidget;
    BottomWidget*           m_pBottomWidget;
    ClientSocket*           m_pClientSocket;
    //QJsonObject           m_userInfo;                 //用户信息
    User                    m_userInfo;
    QHash<int,QQCellChild*> m_hashChilds;               //保存所有好友信息
    ExitDlg*                m_pExitDlg;
    RequestAdduserDlg*      m_pRequestAdduserDlg;
    int                     m_requestUserid;            //请求加好友的账号
    QString                 m_requestUsername;          //请求加好友的昵称
    int                     m_msgtype;                  //客户端收到的消息类型，决定托盘图标
    int                     m_msgtype1;                 //消息类型的补充说明
    int                     m_addFriendResult;          //
    QString                 m_operateUsername;          //操作好友的账号
    QTimer                  *m_ptimerSysTray;           //系统托盘的一个定时器，控制托盘图标
    int                     m_SysTrayTwinkle;           //有消息时控制托盘图标，闪烁
    QList<QPair<QJsonObject,int> >  m_listCachedChatMsg;    //离线聊天消息缓存
public:
    QSystemTrayIcon*        m_pSysTrayIcon;
    static QString          s_strUserNickName;          //当前登录用户昵称，用于聊天显示
    static int              s_facetype;                 //当前登录用户头像
    static quint8           s_tcpStatus;
    static bool             s_isTransFile;              //标志当前客户端是否有文件在传输
signals:
    void sigSend(int cmd, const QJsonObject& data);
    void sigConnectToHost();
    void sigDisConnToHost();
public slots:
    void setUserInfo(const QJsonObject &info);                          //保存当前登录用户信息
    void SltTcpStatus(const quint8 &status);                            //网络状况改变
    void SltSysmenuCliecked(int types);                                 //底部菜单
    void SltModifiUserInfo();                                           //修改个人信息
    void SltCancelModifiUserInfo();                                     //取消修改个人信息
    void SltTrayIcoClicked(QSystemTrayIcon::ActivationReason reason);   //单击托盘图标
    void SltTrayIconMenuClicked(QAction* action);                       //选择托盘菜单
    void SltQuitApp();                                                  //程序退出
    void SltClickedCloseBtn();                                          //单击主界面关闭按钮
    void SltMainWinExit(bool hide);
    void SltRequestAddUser(const QJsonObject &info);                    //好友请求
    void SltReplyAddUser(int select);                                   //回复申请好友请求
    void SltSetTrayIcon();                                              //设置托盘图标
    void SltAddUserResult(QString username, int accept);                //添加好友结果
    void SltReplyUserExisted(int userid,QString username);              //好友已经存在，请勿重复添加
    void SltGotFriendList(const QJsonObject &jsonArray);                //获取好友列表
    void SltDelFriendUpdateWidget(int userid);                          //删除好友成功，更新界面
    void SltUpdateUserStatus(int userid, int status);                   //好友在线状态改变，更新界面
    void SltUpdateFriendInfo(const QJsonObject& jsonObj);               //好友个人信息更新
    void SltRecvChatMsg(const QJsonObject &json, int senderid);         //收到好友聊天消息
};

#endif // MAINWINDOW_H
