﻿#include <QApplication>
#include <QSettings>
#include <QDebug>
#include <QDesktopWidget>
#include <QTextCodec>
#include <QTranslator>
#include <QDir>

#include "global.h"
#include "TopLogin.h"
#include "MyApp.h"
#include "MainWindow.h"
#include "MessageBox.h"
#include "messageitem.h"
#include "LoginAnimationWin.h"
#include "CustomWidget.h"
#include "ChatWin.h"
#include "QQCell.h"
#include "TransFileWgt.h"
#include "CIULog.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
    app.setQuitOnLastWindowClosed(false);       //关闭最后一个无父窗口的窗口时程序不会退出

    const QString strFileName = QDir::currentPath() + "/Log/" + QDateTime::currentDateTime().toString("yyyy-MM-dd-hhmmss")+QString(".log");
    qDebug() << CIULog::Init(false,strFileName);

    QTranslator translator;
    translator.load(":/myI18N_zh_CN.qm");
    app.installTranslator(&translator);

    myHelper::setStyle("default");
    MyApp::InitApp(app.applicationDirPath());

    LoginAnimationWin *w = new LoginAnimationWin;
    w->show();

    return app.exec();
}
