﻿#ifndef CONTACTSWIDGET_H
#define CONTACTSWIDGET_H

#include <QWidget>
#include <QVector>
#include <QScrollArea>

#include "BuddyListGroup.h"
#include "BasedWidget.h"
class MiddleWidget;
class myScrollArea:public QScrollArea
{
    Q_OBJECT
public:
    explicit myScrollArea(QWidget *parent = Q_NULLPTR);
protected:
    void mousePressEvent(QMouseEvent *){setFocus();}
private:
};

class ContactsWidget : public myScrollArea
{
    Q_OBJECT
public:
    friend class MiddleWidget;
    explicit ContactsWidget(QWidget *parent = 0);
    virtual ~ContactsWidget();
    QVector<BuddyListGroup*>& getBuddyListGroupVector() { return m_vecBuddyGroup; }
    BasedWidget* getWidget() { return &m_wid; }
    bool addFriendToGroup(const User& userInfo, int Status);
    bool DelFriendUpdateWidget(int userid);
    void addFriendToGroup(QQCellChild* child);
    void updateUserStatus(int userid, const QString &groupName);
protected:
    virtual void mousePressEvent(QMouseEvent *evt);
    virtual void mouseMoveEvent(QMouseEvent *evt);
private:
    BasedWidget m_wid;
    QVector<BuddyListGroup*> m_vecBuddyGroup;
    BuddyListGroup *m_pDefaultList;
    BuddyListGroup *m_pBackList;

private:
    void initUi();
    void initConnect();
    void setAutoLayout();
signals:
    void sigDelFriend(int userid);      //由BuddyListWidget发送
    void sigOpenChatWin(QQCellChild*);  //由BuddyListWidget发送,通知middlewidget窗口
public slots:
};

#endif // CONTACTSWIDGET_H
