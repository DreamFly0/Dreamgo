﻿#include "ChatWinTitle.h"
#include <QHBoxLayout>
#include <QPaintEvent>
#include <QPainter>
ChatWinTitle::ChatWinTitle(QWidget *parent)
    : QWidget(parent),
      m_skinPic(":/Images/resource/skin/M4A1.jpg")
{
    m_btnMin.setMouseTracking(true);
    m_btnMax.setMouseTracking(true);
    m_btnExit.setMouseTracking(true);
    m_btnWrapWgt.setMouseTracking(true);
    this->setMouseTracking(true);
    this->setAutoFillBackground(true);
    QHBoxLayout *hlayout = new QHBoxLayout;
    QHBoxLayout *hlayoutbtn = new QHBoxLayout;

    m_btnMin.setFixedSize(40,40);
    m_btnMax.setFixedSize(40,40);
    m_btnExit.setFixedSize(40,40);
    m_btnMin.setIcon(QPixmap(":/Images/resource/MinSize.png"));
    m_btnMax.setIcon(QPixmap(":/Images/resource/max.png"));
    m_btnExit.setIcon(QPixmap(":/Images/resource/exit.png"));
    m_btnMin.setObjectName("btnWinMin");
    m_btnMax.setObjectName("btnWinMax");
    m_btnExit.setObjectName("btnWinClose");

    m_title.m_labelSign.setText("这个是真的好天气啊啊！-------------------------------------------------------------");
    m_title.m_labelName.setStyleSheet("color:white;font: 20px 黑体;");
    m_title.m_labelSign.setStyleSheet("color:white;font: 18px 黑体;");
    //m_title.m_labelSign.setMaximumWidth(400);
    hlayoutbtn->addStretch();
    hlayoutbtn->addWidget(&m_btnMin);
    hlayoutbtn->addWidget(&m_btnMax);
    hlayoutbtn->addWidget(&m_btnExit);
    hlayoutbtn->setSpacing(0);
    hlayoutbtn->setContentsMargins(0,0,0,40);
    m_btnWrapWgt.setLayout(hlayoutbtn);
    m_btnWrapWgt.setFixedWidth(120);

    hlayout -> addWidget(&m_title);
    //hlayout->addStretch(1);
    hlayout->addWidget(&m_btnWrapWgt);
    hlayout->setContentsMargins(0,0,0,0);
    this->setFixedHeight(80);
    this->setLayout(hlayout);
}

void ChatWinTitle::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    float d =(float)m_skinPic.height()/m_skinPic.width();
    int h=d*width();
    int w=height()/d;
    if(h<height())//如果图片高度小于窗口高度
    {
         painter.drawPixmap(0,0,w,height(),m_skinPic);
         return;
    }
    painter.drawPixmap(0,0,width(),h,m_skinPic);
}

