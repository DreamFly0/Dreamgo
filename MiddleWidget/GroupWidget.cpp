﻿#include "GroupWidget.h"


#include <QVBoxLayout>

GroupWidget::GroupWidget(QWidget *parent) : BasedWidget(parent)
{
    initUi();
}

void GroupWidget::initUi()
{
    //设置背景色
    this->setAutoFillBackground(true); // 这句要加上, 否则可能显示不出背景图.
    QPalette palette = this->palette();
    palette.setColor(QPalette::Background,Qt::white);
    this->setPalette(palette);
}
