﻿#ifndef GROUPWIDGET_H
#define GROUPWIDGET_H

#include <QWidget>

#include "BasedWidget.h"

class GroupWidget : public BasedWidget
{
    Q_OBJECT
public:
    explicit GroupWidget(QWidget *parent = 0);
public:

private:
    void initUi();
signals:

public slots:
};

#endif // GROUPWIDGET_H
