﻿#ifndef TRANSFILEWGT_H
#define TRANSFILEWGT_H

#include <QWidget>
//该类的样式表都在ui文件中
namespace Ui {
class TransFileWgt;
}
class MD5;

class TransFileWgt : public QWidget
{
    Q_OBJECT

public:
    explicit TransFileWgt(QWidget *parent = nullptr);
    ~TransFileWgt();

    //设置进度条样式
    void setProgressBarQss(int barHeight = 8,                           //进度条高度
                           int barRadius = 5,                           //进度条半径
                           int fontSize = 9,                            //文字字号
                           const QString &normalColor = "#E8EDF2",      //正常颜色
                           const QString &chunkColor = "#E74C3C");      //进度颜色
    void setTitle(QString str);
    void setTips(QString str);
    void setIcon(QString str);
    void setProgressVisible(bool v);
    void setTransStatusOfRecv();
    void setTransBtnEnabled(bool enable);
    int GetProgressValue();
    void TransFileStatus(qint8 status,QString str);
public:
    QString     m_filename;
    QString     m_filePath;
    QString     m_fileMd5;

    bool        m_isMd5Finished;        //用来标记MD5获取结束，为了方便取消发送时，取消对应的信号到槽
    MD5*        m_pTask;                //用来保存相应task，如果用户取消发送，则取消对应的信号槽
    bool        m_isPending;            //当该项已经在下载或者上传为FALSE，反之为TRUE
    qint8       m_nFileType;            //发送文件类型，图片，用户头像，文件
    qint64      m_nFileSize;            //保存文件大小
signals:
    void sigPreparedToSend(TransFileWgt*);
    void sigFileTransStatus(qint8 status,TransFileWgt* widget);
    void sigPreparedToRecv(TransFileWgt* widget,bool flag);         //flag=0:接收；flag=1:另存为；
public slots:
    void SltUpdateTips(QString str);
    void SltUpdateTipsMd5Finished();
    void SltGetFileMd5(QString md5);
    void SltUpdateProgressValue(int value);
public slots:
    void SltCancel();
    void SltTransBtnClicked();
    void SltSaveAsBtnClicked();
private:
    Ui::TransFileWgt *ui;
};

#endif // TRANSFILEWGT_H
