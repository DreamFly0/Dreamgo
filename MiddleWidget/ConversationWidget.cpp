﻿#include "ConversationWidget.h"
#include "messageitem.h"
#include "BuddyListWidget.h"

#include <QVBoxLayout>
#include <QListWidget>

ConversationWidget::ConversationWidget(QWidget *parent)
    : BasedWidget(parent)
{
    initUi();
}

void ConversationWidget::initUi()
{
    //设置背景色
    this->setAutoFillBackground(true); // 这句要加上, 否则可能显示不出背景图.
    QPalette palette = this->palette();
    palette.setColor(QPalette::Background,Qt::white);
    this->setPalette(palette);

    m_pListWgt = new MyListWgt(this);
    m_pListWgt->setFrameShape(QListWidget::NoFrame);
    m_pListWgt->setMouseTracking(true);
    QVBoxLayout *lyout = new QVBoxLayout;
    lyout->setMargin(0);
    lyout->setSpacing(0);
    lyout->addWidget(m_pListWgt);
    this->setLayout(lyout);

    MessageItem *widget = new MessageItem;
    widget->setGeometry(0,0,360,80);
    QListWidgetItem * item = new QListWidgetItem(m_pListWgt);
    item->setSizeHint(widget->geometry().size());
    m_pListWgt->setItemWidget(item, widget);

    MessageItem *widget1 = new MessageItem;
    widget1->setGeometry(0,0,360,80);
    QListWidgetItem * item1 = new QListWidgetItem(m_pListWgt);
    item1->setSizeHint(widget1->geometry().size());
    m_pListWgt->setItemWidget(item1, widget1);

}

MyListWgt::MyListWgt(QWidget *parent)
{

}

MyListWgt::~MyListWgt()
{

}

void MyListWgt::mouseMoveEvent(QMouseEvent *evt)
{
    QListWidget::mouseMoveEvent(evt);
    evt->ignore();
}
