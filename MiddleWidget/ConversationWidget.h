﻿#ifndef CONVERSATIONWIDGET_H
#define CONVERSATIONWIDGET_H

#include <QWidget>
#include "BasedWidget.h"
#include <QListWidget>

class MyListWgt:public QListWidget
{
public:
    explicit MyListWgt(QWidget *parent = 0);
    ~MyListWgt();
protected:
    virtual void mouseMoveEvent(QMouseEvent *evt);
};


class ConversationWidget : public BasedWidget
{
    Q_OBJECT
public:
    explicit ConversationWidget(QWidget *parent = 0);
protected:
private:
    void initUi();
private:
    QListWidget*        m_pListWgt;
signals:

public slots:
};

#endif // CONVERSATIONWIDGET_H
