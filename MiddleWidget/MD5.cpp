﻿#include "MD5.h"
#include <QFile>
#include <QCryptographicHash>

MD5::MD5(QString filepath,QObject *parent) : QObject(parent)
{
    m_filePath = filepath;
}

void MD5::SltFileMD5()
{
    QString md5;
    QFile file(m_filePath);
    if (file.open(QFile::ReadOnly))
    {
        QCryptographicHash hash(QCryptographicHash::Md5);
        if (hash.addData(&file))
        {
            md5 = QString(hash.result().toHex());
        }
        Q_EMIT sigFileMD5(md5);
        file.close();
    }
    Q_EMIT sigMD5finished();
}
