﻿#include "ApplyWidget.h"
#include <QVBoxLayout>

ApplyWidget::ApplyWidget(QWidget *parent) : BasedWidget(parent)
{
    //设置背景色
    this->setAutoFillBackground(true); // 这句要加上, 否则可能显示不出背景图.
    QPalette palette = this->palette();
    palette.setColor(QPalette::Background,Qt::white);
    this->setPalette(palette);
}

