﻿#ifndef MIDDLEWIDGET_H
#define MIDDLEWIDGET_H

#include <QObject>
#include <QWidget>
#include <QButtonGroup>
#include <QPropertyAnimation>
#include <QThread>

#include "BasedWidget.h"

class stackButton;
class AnimationStackedWidget;
class ConversationWidget;
class ContactsWidget;
class GroupWidget;
class ApplyWidget;
class ClientSocket;
class ChatWin;
class QQCellChild;
class ClientFileSocket;
class CFileTask;

class MiddleWidget : public BasedWidget
{
    Q_OBJECT
    Q_PROPERTY(int m_x READ getValue WRITE animation)
protected:
    void paintEvent(QPaintEvent *);
    void resizeEvent(QResizeEvent *evt);
    //void mouseMoveEvent(QMouseEvent *evt);
public:
    explicit MiddleWidget(QWidget *parent = 0);
    virtual ~MiddleWidget();
    int getValue(){return m_x;}
    void animation(int i){m_x=i;update();}
    void setSocket(ClientSocket *pSocket);
    void addFriendToGroup(QQCellChild* child);
    void delFriendUpdateWidget(int userid);
    void updateUserStatus(int userid,const QString& groupName);
    void recvChatMsg(const QJsonObject& json, int senderid, QQCellChild *child, const QString &groupName);
    QList<ChatWin*> getChatWinList(){ return m_mapChatWin.values(); }
private:
    ClientSocket            *m_pClientSocket;
    CFileTask               *m_pFileTask;
    ClientFileSocket        *m_pClientFileSocket;
    QThread                 m_thread;

    stackButton             *m_pBtnStack[4];
    QButtonGroup            *m_pBtnGroup;
    AnimationStackedWidget  *m_pStackWidget;
    ConversationWidget      *m_pConversationWidget;
    ContactsWidget          *m_pContactsWidget;
    GroupWidget             *m_pGroupWidget;
    ApplyWidget             *m_pApplyWidget;

    static QColor           color;
    static QColor           bgcolor;
    QPixmap                 m_pix;
    int                     m_index;
    int                     m_preindex;
    int                     m_x;
    bool                    m_isDrawVerticalLine;
    bool                    m_isAnima;
    QPropertyAnimation      m_animation;

    QMap<int,ChatWin*>  m_mapChatWin;
private:
    void initUi();
    void initConnect();
    void initAnimation();
    void initFileSocket();
signals:
    void sigSend(int cmd, const QJsonObject& data);
    void sigSendToTargetid(int cmd,const QJsonObject& data,int targetid);
public slots:
    void SltStackPageChanged(int index);
    //void SltGotFriendList(const QJsonObject& jsonArray);
    void Sltfinished(){ m_isAnima = false; }
    void SltDelFriend(int userid);
    void SltOpenChatWin(QQCellChild* child);
    void SltCloseChatWin(int id);
    void SltSendTextMsg(const QJsonObject& json,int targetid);
};
#endif // MIDDLEWIDGET_H
