﻿#ifndef CHATWINTITLE_H
#define CHATWINTITLE_H

#include <QWidget>
#include <QPushButton>

#include "BuddyListChild.h"
class ChatWin;

class ChatWinTitle : public QWidget
{
    Q_OBJECT
    friend class ChatWin;
public:
    explicit ChatWinTitle(QWidget *parent = 0);
protected:
    void paintEvent(QPaintEvent *);
private:
    BuddyListChild m_title;
    QWidget     m_btnWrapWgt;
    QPushButton m_btnMin;
    QPushButton m_btnMax;
    QPushButton m_btnExit;
    QPixmap         m_skinPic;
signals:

public slots:
};

#endif // CHATWINTITLE_H
