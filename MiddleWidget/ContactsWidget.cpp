﻿#include "ContactsWidget.h"
#include "ShowGroupBtn.h"
#include "BuddyListWidget.h"

#include <QVBoxLayout>
#include <QMenu>
#include <QDebug>
#include <QScrollBar>
#include <QMouseEvent>

#ifdef _MSC_VER
#pragma execution_character_set("utf-8")
#endif

ContactsWidget::ContactsWidget(QWidget *parent)
    : myScrollArea(parent),
      m_pDefaultList(NULL),
      m_pBackList(NULL)
{
    initUi();
    initConnect();
}

ContactsWidget::~ContactsWidget()
{
    qDebug() << "~ContactsWidget()";

}

bool ContactsWidget::addFriendToGroup(const User &userInfo, int Status)
{
    return m_pDefaultList->addFriendToGroup(userInfo,Status);
}

bool ContactsWidget::DelFriendUpdateWidget(int userid)
{
    return m_pDefaultList->delFriendFromGroup(userid);
}

void ContactsWidget::addFriendToGroup(QQCellChild *child)
{
    foreach (BuddyListGroup* pGroup, m_vecBuddyGroup) {
        if(pGroup->GetGroupName().compare(child->m_strGroupName) == 0)
        {
            pGroup->addFriendToGroup(child);
        }
    }
}

void ContactsWidget::updateUserStatus(int userid,const QString &groupName)
{
    foreach (BuddyListGroup* pGroup, m_vecBuddyGroup) {
        if(pGroup->GetGroupName().compare(groupName) == 0)
        {
            pGroup->updateUserStatus(userid);
        }
    }
}

void ContactsWidget::mousePressEvent(QMouseEvent *evt)
{
    Q_UNUSED(evt);
    qDebug() << "ContactsWidget::mousePressEvent";
    //evt->accept();
}

void ContactsWidget::mouseMoveEvent(QMouseEvent *evt)
{
    //qDebug() << "ContactsWidget::mouseMoveEvent";
}

void ContactsWidget::initUi()
{
    //设置背景色
    this->setMouseTracking(true);
    this->verticalScrollBar()->setMouseTracking(true);
    //this->verticalScrollBar()->setCursor();
    this->setCursor(QCursor(Qt::ArrowCursor));
    this->setAutoFillBackground(true); // 这句要加上, 否则可能显示不出背景图.
    QPalette palette = this->palette();
    palette.setColor(QPalette::Background,Qt::white);
    this->setPalette(palette);
    m_wid.setPalette(palette);
    this->setWidget(&m_wid);
    //布局
    QVBoxLayout *vlayout = new QVBoxLayout;
    vlayout->setContentsMargins(0,0,0,0);
    vlayout->addWidget(&m_wid);
    vlayout->setSpacing(0);
    this->setLayout(vlayout);

    //添加两个默认分组
    m_pDefaultList = new BuddyListGroup(this);
    m_pDefaultList->setMidStackWidget(this);
    m_pDefaultList->m_pBtnList->setListName(tr("我的好友"));
    m_pDefaultList->m_pBtnList->setEnabledMenuItem(false);

    m_pBackList = new BuddyListGroup(this);
    m_pBackList->setMidStackWidget(this);
    m_pBackList->m_pBtnList->setListName(tr("黑名单"));
    m_pBackList->m_pBtnList->setEnabledMenuItem(false);
    m_vecBuddyGroup << m_pDefaultList << m_pBackList;

    /*---------------测试添加用户--------------/
    User user1;
    user1.facetype = 0;
    user1.nickname = "张三";
    user1.signature = "签名";
    User user2;
    user2.facetype = 1;
    user2.nickname = "李四";
    user2.signature = "签名";

    m_pDefaultList->addFriendToGroup(user2,STATUS_ONLINE);
    m_pDefaultList->addFriendToGroup(user1,STATUS_OFFLINE);

    m_pBackList->addFriendToGroup(user1,STATUS_OFFLINE);
    /---------------测试添加用户--------------*/
    QVBoxLayout *wid_vlayout = new QVBoxLayout();
    wid_vlayout->addWidget(m_pDefaultList);
    wid_vlayout->addWidget(m_pBackList);

    wid_vlayout->addSpacerItem(new QSpacerItem(0,0,QSizePolicy::Expanding,QSizePolicy::Expanding));
    wid_vlayout->setContentsMargins(0,0,0,0);
    wid_vlayout->setSpacing(0);
    m_wid.setLayout(wid_vlayout);

    setAutoLayout();
}

void ContactsWidget::initConnect()
{
    //connect(this,&ContactsWidget::sigDelFriend,m_pM);
}

void ContactsWidget::setAutoLayout()
{
    int height = 0;
    foreach(BuddyListGroup* group,m_vecBuddyGroup)
    {
        height += group->height();
        //qDebug() << "height: " << height;
    }
    m_wid.setMinimumHeight(height);
}

/////////////////////////////////////////////////////////////////////////////////////
myScrollArea::myScrollArea(QWidget *parent):QScrollArea(parent)
{

    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    setFrameShadow(QFrame::Plain);//设置平的样式
    setFrameShape(QFrame::NoFrame);//设置无边样式

    verticalScrollBar()->setStyleSheet("QScrollBar{background:transparent; width:10px; padding-right: 2px;}"
                                      "QScrollBar::handle{background:rgb(180,180,180,150);border-radius:4px;}"
                                      "QScrollBar::handle:hover{background: rgb(150,150,150,150);}"
                                      "QScrollBar::add-line:vertical{border:1px rgb(230,230,230,150);height: 1px;}"
                                      "QScrollBar::sub-line:vertical{border:1px rgb(230,230,230,150);height: 1px;}"
                                      "QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {background:transparent;}");


//////////////////////////////////////////////用于最外边


}
