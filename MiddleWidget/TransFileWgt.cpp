﻿#include "TransFileWgt.h"
#include "ui_TransFileWgt.h"
#include "unit.h"

TransFileWgt::TransFileWgt(QWidget *parent) :
    QWidget(parent),
    m_isMd5Finished(false),
    m_isPending(true),
  ui(new Ui::TransFileWgt)
{
    this->setMouseTracking(true);
    ui->setupUi(this);
    ui->progressBar->setTextVisible(false);
    ui->progressBar->setRange(0,100);
    connect(ui->btnCancel,&QPushButton::clicked,this,&TransFileWgt::SltCancel);
    ui->btnTrans->setEnabled(false);
    ui->btnSaveAs->setVisible(false);
    connect(ui->btnTrans,&QPushButton::clicked,this,&TransFileWgt::SltTransBtnClicked);
    connect(ui->btnSaveAs,&QPushButton::clicked,this,&TransFileWgt::SltSaveAsBtnClicked);
    //setProgressBarQss(8, 5, 9, "#E8EDF2", "#1ABC9C");
}

TransFileWgt::~TransFileWgt()
{
    delete ui;
}

void TransFileWgt::setProgressBarQss(int barHeight, int barRadius, int fontSize,
                               const QString &normalColor, const QString &chunkColor)
{

    QStringList qss;
    qss.append(QString("QProgressBar{font:%1pt;background:%2;max-height:%3px;border-radius:%4px;text-align:center;border:1px solid %2;}")
               .arg(fontSize).arg(normalColor).arg(barHeight).arg(barRadius));
    qss.append(QString("QProgressBar:chunk{border-radius:%2px;background-color:%1;}")
               .arg(chunkColor).arg(barRadius));
    ui->progressBar->setStyleSheet(qss.join(""));
}

void TransFileWgt::setTitle(QString str)
{
    QFontMetrics fm(ui->title->font());
    QString text2 = fm.elidedText(str, Qt::ElideRight, ui->title->width());
    ui->title->setText(text2);
}

void TransFileWgt::setTips(QString str)
{
    ui->tips->setText(str);
}

void TransFileWgt::setIcon(QString str)
{
    ui->Icon->setPixmap(QPixmap(str));
}

void TransFileWgt::SltUpdateProgressValue(int value)
{
    ui->progressBar->setValue(value);
}

void TransFileWgt::setProgressVisible(bool v)
{
    ui->progressBar->setVisible(v);
}

//传输按钮默认是发送，如果是接受文件，将该按钮文本改为接收，同时另存为按钮显示出来
void TransFileWgt::setTransStatusOfRecv()
{
    ui->btnTrans->setEnabled(true);
    ui->btnSaveAs->setVisible(true);
    ui->btnTrans->setText(tr("Recv"));
    ui->tips->setText(tr(""));
}

void TransFileWgt::setTransBtnEnabled(bool enable)
{
    ui->btnSaveAs->setEnabled(enable);
    ui->btnTrans->setEnabled(enable);
    ui->tips->setText(tr("Waiting to receive"));
}

int TransFileWgt::GetProgressValue()
{
    return ui->progressBar->value();
}

void TransFileWgt::SltUpdateTips(QString str)
{
    ui->tips->setText(str);
}

//文件传输状态返回，更新界面
void TransFileWgt::TransFileStatus(qint8 status, QString str)
{
    ui->btnCancel->setEnabled(false);
    ui->tips->setText(str);
    ui->progressBar->setValue(100);
    if(FILE_UPLOAD_SUCCESS == status)
    {
        Q_EMIT sigFileTransStatus(FILE_UPLOAD_SUCCESS,this);
    }else if(FILE_UPLOAD_FAILED == status)
    {

        Q_EMIT sigFileTransStatus(FILE_UPLOAD_FAILED,this);
        ui->progressBar->setStyleSheet(QString("QProgressBar:chunk{border-radius:%2px;background-color:%1;}")
                                       .arg("red").arg(3));
        ui->tips->setStyleSheet("QLabel{color: red;font:16px;}");
    }else if(FILE_DOWNLOAD_FAILED == status)
    {
        Q_EMIT sigFileTransStatus(FILE_DOWNLOAD_FAILED,this);
        ui->progressBar->setStyleSheet(QString("QProgressBar:chunk{border-radius:%2px;background-color:%1;}")
                                       .arg("red").arg(3));
        ui->tips->setStyleSheet("QLabel{color: red;font:16px;}");
    }else if(FILE_DOWNLOAD_SUCCESS == status)
    {
        Q_EMIT sigFileTransStatus(FILE_DOWNLOAD_SUCCESS,this);
    }
}

//获取到文件md5值，更新界面
void TransFileWgt::SltUpdateTipsMd5Finished()
{
    ui->tips->setText(tr("File verification completed"));
}

//获取到文件md5值，准备发送
void TransFileWgt::SltGetFileMd5(QString md5)
{
    m_isMd5Finished = true;
    m_fileMd5 = md5;
    Q_EMIT sigPreparedToSend(this);
}

//用户点击了取消传输文件按钮
void TransFileWgt::SltCancel()
{
    //100%表示已经发送完成
    if(100 != ui->progressBar->value())
    {
        ui->btnCancel->setEnabled(false);
        ui->btnSaveAs->setEnabled(false);
        ui->btnTrans->setEnabled(false);

        ui->progressBar->setValue(100);
        ui->progressBar->setStyleSheet(QString("QProgressBar:chunk{border-radius:%2px;background-color:%1;}")
                                       .arg("#c8c8c8").arg(3));
        if(FILE_ITEM_DOWNLOAD_CHAT_OFFLINE_FILE == m_nFileType)
        {
            //取消下载文件
            ui->tips->setText(tr("File Recv failed"));
            Q_EMIT sigFileTransStatus(FILE_DOWNLOAD_CANCEL,this);
        }else if(FILE_ITEM_UPLOAD_CHAT_OFFLINE_FILE == m_nFileType)
        {
            //取消上传文件
            ui->tips->setText(tr("File sending failed"));
            Q_EMIT sigFileTransStatus(FILE_UPLOAD_CANCEL,this);
        }
    }
}

//点击转离线发送或者接收按钮
void TransFileWgt::SltTransBtnClicked()
{
    if(FILE_ITEM_DOWNLOAD_CHAT_OFFLINE_FILE == m_nFileType)
    {
        Q_EMIT sigPreparedToRecv(this,0);
    }
}

//点击另存为按钮
void TransFileWgt::SltSaveAsBtnClicked()
{
    Q_EMIT sigPreparedToRecv(this,1);
}
