﻿#ifndef CHATWIN_H
#define CHATWIN_H

#include <QWidget>
#include <QPushButton>
#include <QTextEdit>
#include <QToolButton>
#include <QFont>
#include <QColor>
#include <QComboBox>
#include <QWebEngineView>
#include <QTabWidget>
#include <QSqlQuery>
#include <QQueue>

#include "ChatWinTitle.h"
#include "BaseWindow.h"
#include "BuddyListChild.h"
#include "MyTextEdit.h"

#define UnReadMsg 10
#define HistoryMsg 11
struct Font
{
    QString fontfamily;
    int     pointsize;
    bool    italic;
    bool    underline;
    bool    bold;
    QColor  color;
};

class MiddleWidget;
class EmotionWindow;
class QListWidget;
class TransFileWgt;
class ClientFileSocket;
class CFileTask;

class ChatWin : public BaseWindow
{
    Q_OBJECT
public:
    friend class MiddleWidget;
    explicit ChatWin(QWidget *parent = 0);
    virtual ~ChatWin();
    void setQQCell(QQCellChild *child);         //创建一个ChatWin以后需要马上调用setQQCell来设置窗口的基本信息，然后再载入历史消息，这个设计不好
    void setFileSocket(ClientFileSocket* pSocket,CFileTask* pTask);
    QQCellChild *getQQCell(){ return m_pChild; }
    void UpdateWinInfo();
    void InsertChatMsg(uint utcTime,QFont font,const QString& msgText,bool recv = true,QColor color = QColor(Qt::black));
    void InsertTipMsg(const QString& msg);
    void InsertHistoryMsg(uint utcTime, QFont font, const QString &msgText, bool recv, QColor color  = QColor(Qt::black));
    void ShakeWin(const QString& text);
    int getTransFileCnt(){ return m_nTransFileCnt; }
protected:
    virtual void paintEvent(QPaintEvent *e);
    virtual void closeEvent(QCloseEvent *e);
    virtual bool eventFilter(QObject *obj, QEvent *evt);
    virtual void resizeEvent(QResizeEvent *event);
private:
    void initUi();
    void initConnect();
    void mergeFormatOnWordOrSelection(const QTextCharFormat &format);
    void LoadHistoryMsg(quint8 Type, int offset, int count);
    void LoadAllHistoryMsg();
    void GetMsgFromQuery();
    void InsertToTransFileWgt();
    void SendFile(QString strFilePath);         //发送文件
    void RecvFile(QStringList fileInfo);
    void testInsert(const QString& msg);        //仅测试用
private:
    ChatWinTitle        m_titleBar;
    QWidget             m_mainWgt;
    QWidget             m_leftWgt;
    QTabWidget          m_tabWgt;

    QWidget             m_midWgt;
    QWebEngineView*     m_pwebView;
    QWidget             m_FontWidget;
    QComboBox           m_fontfamilyCombox;
    QComboBox           m_fontsizeCombox;
    QToolButton         m_btnBold;
    QToolButton         m_btnitalic;
    QToolButton         m_btnUnderline;
    QToolButton         m_btnColor;

    QWidget             m_btnToolBar;
    QWidget             m_bottomWgt;
    MyTextEdit          m_editMsg;
    QWidget             m_sendWgt;
    QPushButton         m_btnSend;
    QPushButton         m_btnClose;
    QPushButton         m_btnAction;

    QToolButton         m_btnFont;
    QToolButton         m_btnFace;
    QToolButton         m_btnFile;
    QToolButton         m_btnPicture;
    QToolButton         m_btnScreenshot;
    QToolButton         m_btnShake;
    QToolButton         m_btnHistory;

    QWidget             m_HistoryMsgWrapWgt;
    QWidget             m_msgMgrWgt;
    MyTextEdit*         m_historyTextedit;
    QListWidget*        m_pTransfileWgt;
    QWidget*            m_pTestWgt;
    QToolButton         m_btnFirstPage;
    QToolButton         m_btnPrePage;
    QLabel              m_labelTips;
    QToolButton         m_btnNextPage;
    QToolButton         m_btnLastPage;

    EmotionWindow*      m_emotionWgt;
    Font                m_font;
    int                 m_id;
    bool                m_bEnterChecked;           //Enter键发送还是Ctrl+Enter键发送消息,默认Enter发送

    QTimer*             m_timer;                    //以下三个变量用于窗口抖动
    int                 m_nPosition;
    QPoint              m_curPos;

    int                 m_currentPage;              //以下三个变量用于标记查询的历史消息
    int                 m_currentMsgIndex;          //起始index = 1;
    int                 m_totalPage;
    int                 m_lastPageMsgCnt;
    QSqlQuery           m_historyQuery;

    int                 m_nTransFileCnt;            //文件传输数量+1，传输结束（成功，失败，取消）-1
    TransFileWgt*       m_pCurrentTransWgt;         //当前正在传输的widget
    ClientFileSocket*   m_pClientFileSocket;
    CFileTask*          m_pFileTask;
private:
    QQCellChild*        m_pChild;
signals:
    void sigCloseChatWin(int id);
    void sigSendMsg(const QJsonObject& msg,int targetid);
private slots:
    void SltTransFileBtnClicked();
    void SltHistoryMsgWgtShow();

    void SltFontWgtShow();                                      //一组字体修改槽函数
    void SltBoldBtnCLicked(bool checked);
    void SltItalicClicked(bool checked);
    void SltUnderlineClicked(bool checked);
    void SltFontFamilyChanged(const QString &fontfamily);
    void SltFontSizeChanged(const QString& fontsize);
    void SltColorBtnCLicked(bool checked);
    void SltCharFormatChanged(const QTextCharFormat &format);

    void SltPrePageHistoryMsg();                                //一组历史消息翻页槽函数
    void SltNextPageHistoryMsg();
    void SltLastPageHistoryMsg();
    void SltFirstPageHistoryMsg();
public slots:
    void SltSeleteFont();
    void SltSreenShot();
    void SltClickedEmotion(int faceid);
    void SltShowNormalEmotionWgt(bool checked);
    void SltClose();
    void SltSend();
    void SltSendWinShake();
    void SltActionEnter(bool checked);
    void SltShakeWin();
    void SlttabCloseRequested(int index);
    void SltLoadHistoryMsg(bool ok);
    void SltPreparedToSend(TransFileWgt* task);
    void SltTransFileStatus(qint8 status,TransFileWgt* task);
    void SltPreparedToRecv(TransFileWgt* widget,bool flag);
};

#endif // CHATWIN_H
