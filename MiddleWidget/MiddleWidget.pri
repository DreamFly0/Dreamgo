INCLUDEPATH     += $$PWD
DEPENDPATH      += $$PWD

INCLUDEPATH     += $$PWD/QQList
DEPENDPATH      += $$PWD/QQList

HEADERS += \
    $$PWD/MiddleWidget.h \
    $$PWD/ConversationWidget.h \
    $$PWD/ContactsWidget.h \
    $$PWD/GroupWidget.h \
    $$PWD/ApplyWidget.h \
    $$PWD/QQList/BuddyListChild.h \
    $$PWD/QQList/BuddyListGroup.h \
    $$PWD/QQList/BuddyListWidget.h \
    $$PWD/QQList/ShowGroupBtn.h \
    $$PWD/QQList/FriendInfoDlg.h \
    $$PWD/ChatWin.h \
    $$PWD/ChatWinTitle.h \
    $$PWD/ChatWindow.h \
    $$PWD/MD5.h \
    $$PWD/TransFileWgt.h

SOURCES += \
    $$PWD/MiddleWidget.cpp \
    $$PWD/ConversationWidget.cpp \
    $$PWD/ContactsWidget.cpp \
    $$PWD/GroupWidget.cpp \
    $$PWD/ApplyWidget.cpp \
    $$PWD/QQList/BuddyListChild.cpp \
    $$PWD/QQList/BuddyListGroup.cpp \
    $$PWD/QQList/BuddyListWidget.cpp \
    $$PWD/QQList/ShowGroupBtn.cpp \
    $$PWD/QQList/FriendInfoDlg.cpp \
    $$PWD/ChatWin.cpp \
    $$PWD/ChatWinTitle.cpp \
    $$PWD/ChatWindow.cpp \
    $$PWD/MD5.cpp \
    $$PWD/TransFileWgt.cpp

FORMS += \
    $$PWD/TransFileWgt.ui
