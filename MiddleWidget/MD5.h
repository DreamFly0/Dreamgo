﻿#ifndef MD5_H
#define MD5_H

#include <QObject>

class MD5 : public QObject
{
    Q_OBJECT
public:
    explicit MD5(QString filepath,QObject *parent = nullptr);

signals:
    void sigFileMD5(const QString& str);
    void sigMD5finished();
public slots:
    void SltFileMD5();
private:
    QString m_filePath;
};

#endif // MD5_H
