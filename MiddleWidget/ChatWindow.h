﻿#ifndef CHATWINDOW_H
#define CHATWINDOW_H

#include "BaseWindow.h"
#include "ChatWinTitle.h"
#include <QWebEngineView>

class ChatWindow : public BaseWindow
{
public:
    ChatWindow(QWidget *parent = 0);
    ~ChatWindow();
protected:
    void paintEvent(QPaintEvent *evt);
private:
    void initUi();
private:

    ChatWinTitle    *m_ptitleBar;
    QWidget         *m_pmainWgt;
    QWidget         *m_pleftWgt;
    QWidget         *m_prightWgt;
    QWidget         *m_pShowMsgWgt;
    QWidget         *m_pFontWgt;
    QWidget         *m_pToolWgt;
    QWidget         *m_pEditMsgWgt;
    QWebEngineView  *m_pWebview;

};

#endif // CHATWINDOW_H
