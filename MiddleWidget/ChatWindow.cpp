﻿#include "ChatWindow.h"
#include <QDebug>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPainter>
#include <QPaintEvent>
#include <QGraphicsDropShadowEffect>

ChatWindow::ChatWindow(QWidget *parent)
    : BaseWindow(parent)
{
    this->resize(708,800);
    this->initUi();
}

ChatWindow::~ChatWindow()
{
    qDebug() << "ChatWin::~ChatWindow()";
}

void ChatWindow::paintEvent(QPaintEvent *evt)
{
    /*
    QPainter paint(m_pmainWgt);
    paint.setPen(QColor("#3bb1ea"));
    qDebug() << m_pleftWgt->mapToParent(m_pleftWgt->rect().topRight());
    qDebug() << m_pleftWgt->mapToParent(m_pleftWgt->rect().bottomRight());
    paint.drawLine( m_pleftWgt->mapToParent(m_pleftWgt->rect().topRight())+QPoint(2,0),m_pleftWgt->mapToParent(m_pleftWgt->rect().bottomRight()));
*/
    QPainter painter(this);
    painter.setPen(Qt::black);
    painter.setFont(QFont("Arial", 30));
    painter.drawText(rect(), Qt::AlignCenter, "Qt");

}

void ChatWindow::initUi()
{

    m_ptitleBar = new ChatWinTitle;
    m_pmainWgt = new QWidget;
    m_pleftWgt = new QWidget;
    m_prightWgt = new QWidget;
    m_pShowMsgWgt = new QWidget;
    m_pFontWgt = new QWidget;
    m_pToolWgt = new QWidget;
    m_pEditMsgWgt = new QWidget;
    m_pWebview = new QWebEngineView;
    m_pWebview->load(QUrl("http://qt-project.org/"));
    m_pleftWgt->setMinimumWidth(700);
    m_prightWgt->setFixedWidth(350);
    m_prightWgt->setMinimumHeight(700);
    m_pWebview->setMinimumHeight(400);
    m_pFontWgt->setFixedHeight(40);
    m_pToolWgt->setFixedHeight(40);
    m_pEditMsgWgt->setFixedHeight(160);
    QPalette palette = m_prightWgt->palette();
    palette.setColor(QPalette::Background,Qt::red);
    m_prightWgt->setPalette(palette);
    palette = m_pShowMsgWgt->palette();
    palette.setColor(QPalette::Background,Qt::blue);
    m_pShowMsgWgt->setPalette(palette);
    palette = m_pFontWgt->palette();
    palette.setColor(QPalette::Background,Qt::black);
    m_pFontWgt->setPalette(palette);
    palette = m_pToolWgt->palette();
    palette.setColor(QPalette::Background,Qt::green);
    m_pToolWgt->setPalette(palette);

    palette = m_pEditMsgWgt->palette();
    palette.setColor(QPalette::Background,Qt::red);
    m_pEditMsgWgt->setPalette(palette);

    m_pmainWgt->setAutoFillBackground(true);
    m_pleftWgt->setAutoFillBackground(true);
    m_prightWgt->setAutoFillBackground(true);
    m_pFontWgt->setAutoFillBackground(true);
    m_pToolWgt->setAutoFillBackground(true);

    //1.整个窗口布局
    QVBoxLayout *Lyout = new QVBoxLayout;
    Lyout->setMargin(0);
    Lyout->setSpacing(0);
    Lyout->addWidget(m_ptitleBar);
    Lyout->addWidget(m_pmainWgt);
    m_MainWin.setLayout(Lyout);
    //2.中间主窗口布局
    QHBoxLayout *mainLyout = new QHBoxLayout;
    mainLyout->setMargin(0);
    mainLyout->setSpacing(2);
    mainLyout->addWidget(m_pleftWgt);
    mainLyout->addWidget(m_prightWgt);
    m_pmainWgt->setLayout(mainLyout);
    //this->resize(708,800);
    //3.左侧窗口布局
    QVBoxLayout *leftlyout = new QVBoxLayout;
    leftlyout->addWidget(m_pShowMsgWgt);
    leftlyout->addWidget(m_pToolWgt);
    leftlyout->addWidget(m_pEditMsgWgt);
    m_pleftWgt->setLayout(leftlyout);
    //3.1显示消息窗体布局
    QVBoxLayout *showMsglyout = new QVBoxLayout;
    showMsglyout->setMargin(0);
    showMsglyout->setSpacing(0);
    showMsglyout->addWidget(m_pWebview);
    showMsglyout->addWidget(m_pFontWgt);
    m_pShowMsgWgt->setLayout(showMsglyout);

    //右侧窗口布局
}
