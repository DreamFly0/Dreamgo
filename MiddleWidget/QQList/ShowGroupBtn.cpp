﻿#include "ShowGroupBtn.h"
#include <QMouseEvent>
#include <QAction>
#include <QHBoxLayout>
#include "BuddyListGroup.h"
#pragma execution_character_set("utf-8")
ShowGroupBtn::ShowGroupBtn(QWidget *parent)
    : QPushButton(parent),
      m_indicator(this),
      m_btnMenu(this),
      m_lineEdit(this),
      m_pBuddyGroup(NULL),
      m_BuddylistName("我的好友",this),
      m_labelFriendCnt("[0/0]",this),
      m_isShowChild(false)
{
    m_isdrawMove = false;
    init();
    initMenu();
    setFixedHeight(40);
    connect(this,&ShowGroupBtn::clicked,this,&ShowGroupBtn::SltClicked);
}

void ShowGroupBtn::init()
{
    this->setCursor(Qt::PointingHandCursor);
    this->setAcceptDrops(true);
    this->setObjectName("BuddyGroupBtn");
    m_lineEdit.setGeometry(16,5,250,30);
    m_lineEdit.setContextMenuPolicy(Qt::NoContextMenu);
    m_lineEdit.setStyleSheet("QLineEdit{border:4px solid rgb(40,80,150);}");
    m_lineEdit.installEventFilter(this);
    m_lineEdit.hide();

    m_BuddylistName.setStyleSheet("color:rgb(38,38,38);font: 20px 黑体;");
    m_labelFriendCnt.setStyleSheet("color:rgb(38,38,38);font: 20px 黑体;");
    m_indicator.setFixedSize(30,30);
    m_indicator.setPixmap(QPixmap(":/Images/resource/images/indicator_right.png"));

    QHBoxLayout *layout=new QHBoxLayout;
    layout->addWidget(&m_indicator,0,Qt::AlignVCenter);
    layout->addWidget(&m_BuddylistName,0,Qt::AlignVCenter);
    layout->addWidget(&m_labelFriendCnt,0,Qt::AlignVCenter);
    layout->addStretch(0);
    layout->setContentsMargins(0,0,0,0);
    layout->setSpacing(2);
    this->setLayout(layout);
    connect(&m_lineEdit,&QLineEdit::returnPressed,this,&ShowGroupBtn::SltRetrunPressed);
    //connect(&m_lineEdit,&QLineEdit::returnPressed,this,&ShowGroupBtn::setFocus);
}

void ShowGroupBtn::initMenu()
{
    m_pAddGroup=new QAction(tr("添加分组"),this);
    m_pDelGroup=new QAction(tr("删除分组"),this);
    m_pRenameGroup=new QAction(tr("重命名"),this);

    m_btnMenu.addAction(m_pAddGroup);
    m_btnMenu.addAction(m_pDelGroup);
    m_btnMenu.addSeparator();
    m_btnMenu.addAction(m_pRenameGroup);

    m_btnMenu.setContentsMargins(4,10,4,10);

    connect(m_pAddGroup,&QAction::triggered,this,&ShowGroupBtn::sigAddGroup);
    connect(m_pDelGroup,&QAction::triggered,this,&ShowGroupBtn::sigDelGroup);
    connect(m_pRenameGroup,&QAction::triggered,this,&ShowGroupBtn::SltRename);
}

void ShowGroupBtn::setBuddyGroup(BuddyListGroup *p)
{
    m_pBuddyGroup = p;
}

//设置部分菜单功能不可用
void ShowGroupBtn::setEnabledMenuItem(bool isSetting)
{
    m_pRenameGroup->setEnabled(isSetting);
    m_pDelGroup->setEnabled(isSetting);
}

void ShowGroupBtn::updateFriendCnt(int onlineCnt, int Cnt)
{
    m_labelFriendCnt.setText(QString("[%1/%2]").arg(onlineCnt).arg(Cnt));
}

void ShowGroupBtn::showLabel(bool show)
{
    if(show)
    {
        m_BuddylistName.show();
        m_labelFriendCnt.show();
    }else
    {
        m_BuddylistName.hide();
        m_labelFriendCnt.hide();
    }
}

void ShowGroupBtn::SltUpdateFriendCount()
{

}

void ShowGroupBtn::SltRetrunPressed()
{
    Q_EMIT sigRename(m_lineEdit.text());

    m_BuddylistName.setText(m_lineEdit.text());
    m_BuddylistName.show();
    //m_labelFriendCnt.show();
    m_lineEdit.hide();
    SltUpdateFriendCount();
}

void ShowGroupBtn::SltRename()
{
    m_lineEdit.show();
    m_lineEdit.setFocus();
    m_lineEdit.raise();

    m_BuddylistName.hide();
    //m_labelFriendCnt.hide();
    m_lineEdit.setText(m_BuddylistName.text());
    m_lineEdit.selectAll();
}

void ShowGroupBtn::SltClicked()
{
    m_isShowChild = !m_isShowChild;
    if (m_isShowChild)
    {
        m_indicator.setPixmap(QPixmap(":/Images/resource/images/indicator_down.png"));
    }
    else
    {
        m_indicator.setPixmap(QPixmap(":/Images/resource/images/indicator_right.png"));
    }
}

void ShowGroupBtn::mousePressEvent(QMouseEvent *evt)
{
    if(evt->button()==Qt::LeftButton)
    {
        QPushButton::mousePressEvent(evt);
    }else
    {
        m_presspos=evt->pos();
        SltMenuRequest();
    }

    evt->ignore();
}

void ShowGroupBtn::mouseMoveEvent(QMouseEvent *evt)
{
    Q_UNUSED(evt);
}

bool ShowGroupBtn::eventFilter(QObject *obj, QEvent *evt)
{
    if(obj == &m_lineEdit)
    {
        if(evt->type() == QEvent::FocusOut)
        {
            SltRetrunPressed();
        }
    }

    return QObject::eventFilter(obj,evt);
}

