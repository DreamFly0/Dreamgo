﻿#ifndef BUDDYLISTCHILD_H
#define BUDDYLISTCHILD_H

#include <QWidget>
#include <QLabel>
#include "BasedWidget.h"
#include "HeadButton.h"

class QQCellChild;

class BuddyListChild : public BasedWidget
{
    Q_OBJECT
public:
    explicit BuddyListChild(QWidget *parent = 0);
    virtual ~BuddyListChild();
    void setQQCell(QQCellChild *child);
    QQCellChild* getQQCell(){ return m_pCchild; }
    void updateWinInfo();
protected:
    void resizeEvent(QResizeEvent *evt);
public:
    QPushButton m_headBtn;
    QLabel m_labelName;
    QLabel m_labelSign;
    QQCellChild* m_pCchild;

signals:
    void sigShowFriendInfo(QQCellChild*);
public slots:
    void SltClickedHeadBtn();
};

#endif // BUDDYLISTCHILD_H
