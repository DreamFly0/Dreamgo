﻿#ifndef BUDDYLISTGROUP_H
#define BUDDYLISTGROUP_H

#include <QWidget>
#include <QMap>
#include <QTimer>
#include "BasedWidget.h"
#include "QQCell.h"
#include "unit.h"

class ContactsWidget;
class ShowGroupBtn;
class BuddyListWidget;
class QListWidgetItem;

class BuddyListGroup : public BasedWidget
{
    Q_OBJECT
public:
    friend class BuddyListWidget;
    friend class ContactsWidget;
    explicit BuddyListGroup(QWidget *parent = 0);
    virtual ~BuddyListGroup();
    //void SetBuddyGroupName(QString)
    /*static fuction*/
    static BuddyListGroup* getCurList(){ return s_pCurList; }
    static void setCurList(BuddyListGroup* plist) { s_pCurList = plist; }

    void setMidStackWidget(ContactsWidget* pwidget);
    void setAutoLayout();
    void updateOnlineCnt();
    bool addFriendToGroup(const User& userInfo, int Status);
    void addFriendToGroup(QQCellChild *child);
    bool delFriendFromGroup(int userid);
    void updateUserStatus(int userid);
    void recvMsg(int senderid);
    QString GetGroupName();
    void groupTwinkle();
private:
    ContactsWidget*         m_pContactsWidget;
    ShowGroupBtn*           m_pBtnList;
    BuddyListWidget*        m_pListwidget;
    QQCellGroup             m_Cgroup;
    QHash<int,QQCellChild*> m_hashChilds;           //保存当前组的好友
    int                     m_OnlineCnt;
    QTimer                  m_timer;
    QList<int>              m_RecvMsgIds;                 //发来消息的好友id
private:

protected:
    static BuddyListGroup *s_pCurList;
    virtual void wheelEvent(QWheelEvent *e);
    virtual void mousePressEvent(QMouseEvent *evt);
signals:

public slots:
    void SltShowChildList();
    void SltTwinkle();
    void SltStopTwinkle();
};

#endif // BUDDYLISTGROUP_H
