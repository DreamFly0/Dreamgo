﻿#ifndef FRIENDINFODLG_H
#define FRIENDINFODLG_H
#include "CustomWidget.h"

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QTextEdit>
class FriendInfoDlg : public CBaseDialog
{
    Q_OBJECT
public:
    explicit FriendInfoDlg(QWidget *parent = 0);
    virtual void closeEvent(QCloseEvent* evt);
    virtual  ~FriendInfoDlg();
    QLabel          m_labelAccount;
    QLabel          m_labelName;
    QLabel          m_labelSign;
    QLabel          m_labelSex;
    QLabel          m_labelBirthday;
    QLabel          m_labelSite;
    QLabel          m_labelTel;
    QLabel          m_labelEmail;

    QLineEdit       m_editAccount;
    QLineEdit       m_editName;
    QTextEdit       m_editSign;
    QLineEdit       m_editSex;
    QLineEdit       m_editBirthday;
    QLineEdit       m_editSite;
    QLineEdit       m_editTel;
    QLineEdit       m_editEmail;

    QPushButton     m_btnClose;
    int             m_id;
signals:
    void sigFriendInfoDlgClosed(int id);
public slots:
    void SltBtnClicked();
};

#endif // FRIENDINFODLG_H
