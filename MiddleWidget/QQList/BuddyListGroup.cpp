﻿#include "BuddyListGroup.h"
#include "ContactsWidget.h"
#include "ShowGroupBtn.h"
#include "BuddyListWidget.h"
#include "BuddyListChild.h"
#include "ChatWin.h"

#include <QVBoxLayout>
#include <QWheelEvent>
#include <QScrollBar>
#include <QDebug>

BuddyListGroup *BuddyListGroup::s_pCurList = NULL;

BuddyListGroup::BuddyListGroup(QWidget *parent)
    : BasedWidget(parent),
      m_pContactsWidget(NULL),
      m_OnlineCnt(0),
      m_timer(this)
{
    this->setMinimumSize(360,40);
    m_pBtnList = new ShowGroupBtn(this);
    m_pListwidget = new BuddyListWidget(this);

    m_pListwidget->setBuddyListGroup(this);
    m_pBtnList->setBuddyGroup(this);
    m_pListwidget->hide();

    this->setAcceptDrops(true);
    //qDebug() << "this->height1()" << this->height();
    QVBoxLayout *vlayout = new QVBoxLayout();
    vlayout->addWidget(m_pBtnList);
    vlayout->addWidget(m_pListwidget);
    vlayout->setContentsMargins(0,0,0,0);
    vlayout->setSpacing(0);
    this->setLayout(vlayout);

    connect(m_pBtnList,&QPushButton::clicked,this,&BuddyListGroup::SltShowChildList);
    connect(&m_timer,&QTimer::timeout,this,&BuddyListGroup::SltTwinkle);
    //m_ptimerSysTray->start(500);
}

BuddyListGroup::~BuddyListGroup()
{
    qDebug() << "BuddyListGroup::~BuddyListGroup()";
}

void BuddyListGroup::setMidStackWidget(ContactsWidget *pwidget)
{
    m_pContactsWidget = pwidget;
    m_pListwidget->setContactsWidget(pwidget);
}

void BuddyListGroup::setAutoLayout()
{
    m_pListwidget->setAutoLayoutSize();
}

void BuddyListGroup::updateOnlineCnt()
{
    int onlineCnt = 0;
    for(auto child : m_hashChilds)
    {
        if(child->m_status == STATUS_ONLINE)
        {
            onlineCnt++;
        }
    }
    m_OnlineCnt = onlineCnt;
    m_pBtnList->updateFriendCnt(m_OnlineCnt,m_pListwidget->count());
}

bool BuddyListGroup::addFriendToGroup(const User &userInfo, int Status)
{
    if(m_pListwidget->count() == 0)
    {
        if(m_pBtnList->ShowChild())
            m_pBtnList->click();
    }
    /*
    if(m_mapChilds.find(userInfo.userid) == m_mapChilds.end())
    {   //如果好友不存在，添加好友到分组
        QQCellChild *child = new QQCellChild();
        child->m_childInfo = userInfo;
        child->m_id = userInfo.userid;
        child->m_status = Status;

        m_mapChilds[child->m_id] = child;
        m_pListwidget->addChildItem(child);
        updateOnlineCnt();
        //m_pListwidget->sortItems(Qt::AscendingOrder);
        setAutoLayout();
    }else
    {   //如果好友存在，刷新好友信息即可
        QQCellChild *child = m_mapChilds[userInfo.userid];

        child->m_childInfo = userInfo;
        child->m_id = userInfo.userid;
        child->m_status = Status;

        //如果存在聊天窗口更新其对应好友个人资料
        if(child->m_pChatWin != NULL)
        {
            ChatWin* chatwin = child->m_pChatWin;
            chatwin->setQQCell(child);
        }

        //更新好友信息
        for(int i = 0;i<m_pListwidget->count();i++)
        {
            QListWidgetItem* item = m_pListwidget->item(i);

            if(child->m_id == item->data(Qt::UserRole))
            {

                QString str = (Status == STATUS_ONLINE ? "A":"Z");
                item->setText(str);
                //item->setData(Qt::InitialSortOrderRole,str);

                BuddyListChild* w = qobject_cast<BuddyListChild*>(m_pListwidget->itemWidget(item));
                w->setQQCell(child);
            }
        }
        updateOnlineCnt();
    }
    */
    return true;
    //SltShowChildList();
}

void BuddyListGroup::addFriendToGroup(QQCellChild *child)
{
    if(m_pListwidget->count() == 0)
    {
        if(m_pBtnList->ShowChild())
            m_pBtnList->click();
    }
    if(m_hashChilds.contains(child->m_id))
    {
        //如果好友存在，刷新好友信息即可
        for(int i = 0;i<m_pListwidget->count();i++)
        {
            QListWidgetItem* item = m_pListwidget->item(i);

            if(child->m_id == item->data(Qt::UserRole))
            {

                QString str = (STATUS_ONLINE == child->m_status? "A":"Z");
                item->setText(str);
                //item->setData(Qt::InitialSortOrderRole,str);

                BuddyListChild* w = qobject_cast<BuddyListChild*>(m_pListwidget->itemWidget(item));
                //w->setQQCell(child);
                w->updateWinInfo();
            }
        }
        updateOnlineCnt();
    }
    else
    {
        m_hashChilds.insert(child->m_id,child);
        m_pListwidget->addChildItem(child);
        updateOnlineCnt();
        setAutoLayout();
    }
}

bool BuddyListGroup::delFriendFromGroup(int userid)
{
    m_pListwidget->removeChildItem(userid);
    m_hashChilds.remove(userid);

    //如果listWidget处于展开状态且删除的是最后一个item，则收起listwidget
    if(m_pListwidget->count() == 0 && (!m_pListwidget->isHidden()))
    {
        m_pBtnList->click();
    }
    setAutoLayout();
    updateOnlineCnt();
    return true;
}

//更新好友状态
void BuddyListGroup::updateUserStatus(int userid)
{
    if(m_hashChilds.contains(userid))
    {
        QQCellChild *child = m_hashChilds[userid];

        for(int i = 0;i<m_pListwidget->count();i++)
        {
            QListWidgetItem* item = m_pListwidget->item(i);

            if(userid == item->data(Qt::UserRole))
            {

                QString str = (STATUS_ONLINE == child->m_status ? "A":"Z");
                item->setText(str);
                //item->setData(Qt::InitialSortOrderRole,str);

                BuddyListChild* w = qobject_cast<BuddyListChild*>(m_pListwidget->itemWidget(item));
                w->updateWinInfo();
            }
        }
    }
    updateOnlineCnt();
}

void BuddyListGroup::recvMsg(int senderid)
{
    m_timer.start(500);
    m_pListwidget->startAnimation(senderid);
    /*直接弹出好友信息框，这个写在另一个函数
    if(m_mapChilds.find(senderid) != m_mapChilds.end())
    {
        QQCellChild* child= m_mapChilds[senderid];
        Q_EMIT m_pContactsWidget->sigOpenChatWin(child);
    }
    */
}

QString BuddyListGroup::GetGroupName()
{
    return m_pBtnList->getListName();
}

//开启定时器，分组标题闪烁
void BuddyListGroup::groupTwinkle()
{
    m_timer.start(500);
}

void BuddyListGroup::wheelEvent(QWheelEvent *e)
{
    if(m_pContactsWidget != NULL)
    {
        int i=e->delta()/5;
        int value=m_pContactsWidget->verticalScrollBar()->value();
        m_pContactsWidget->verticalScrollBar()->setValue(value-i);
    }

}

void BuddyListGroup::mousePressEvent(QMouseEvent *evt)
{
    evt->accept();
}

void BuddyListGroup::SltShowChildList()
{
    //m_pListwidget->count() > 0不加这个当count == 0时候显示有问题？为什么?这导致编程时，删除最后一个好友
    //和添加第一个好友时为了避免这个显示问题加了一些不必要的if...else
    if(m_pListwidget->isHidden() && m_pListwidget->count() > 0)
    {
        m_pListwidget->show();
    }else
    {
        m_pListwidget->hide();
    }
    setAutoLayout();
    m_pBtnList->showLabel(true);
}

//收到消息，控制分组栏闪烁
void BuddyListGroup::SltTwinkle()
{
    if(m_pListwidget->isHidden())
    {
        static bool show = true;
        if(show)
        {
            m_pBtnList->showLabel(show);
            show = false;
        }else
        {
            m_pBtnList->showLabel(show);
            show = true;
        }
    }
}

void BuddyListGroup::SltStopTwinkle()
{
    m_timer.stop();
}
