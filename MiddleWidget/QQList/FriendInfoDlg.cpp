﻿#include "FriendInfoDlg.h"
#include "LoginTitleBar.h"
#include <QCloseEvent>
#include <QDebug>
#pragma execution_character_set("utf-8")
FriendInfoDlg::FriendInfoDlg(QWidget *parent)
    : CBaseDialog(parent),
      m_labelAccount(tr("账    号："),this),
      m_labelName(tr("昵    称："),this),
      m_labelSign(tr("个性签名："),this),
      m_labelSex(tr("性    别："),this),
      m_labelBirthday(tr("生    日："),this),
      m_labelSite(tr("地    址："),this),
      m_labelTel(tr("电    话："),this),
      m_labelEmail(tr("邮    箱："),this),
      m_editAccount(this),
      m_editName(this),
      m_editSign(this),
      m_editSex(this),
      m_editBirthday(this),
      m_editSite(this),
      m_editTel(this),
      m_editEmail(this),
      m_btnClose(tr("关闭"),this),
      m_id(0)
{
    this->setFixedSize(660,480);
    //设置窗口背景
    this->setAutoFillBackground(true); // 这句要加上, 否则可能显示不出背景图.
    QPalette palette = this->palette();
    palette.setBrush(QPalette::Window,QBrush(QColor("#f2f1f1")));             // 使用平滑的缩放方式
    setPalette(palette);

    //设置标题背景
    m_pTitleBar->setAutoFillBackground(true); // 这句要加上, 否则可能显示不出背景图.
    QPalette palette1 = m_pTitleBar->palette();
    palette1.setBrush(QPalette::Window,
                      QBrush(QPixmap(":/Images/resource/titleBg.png").scaled(// 缩放背景图.
                      this->size(),
                      Qt::IgnoreAspectRatio,
                      Qt::SmoothTransformation)));             // 使用平滑的缩放方式

    m_pTitleBar->setPalette(palette1);
    m_pTitleBar->setWinTitle("好友资料");
    m_pTitleBar->getMinimizeBtn()->hide();

    //布局
    m_labelAccount.move(40,40);
    m_labelName.move(40,80);
    m_labelSign.move(40,120);
    m_labelSex.move(40,210);
    m_labelBirthday.move(40,250);
    m_labelSite.move(40,290);
    m_labelTel.move(40,330);
    m_labelEmail.move(40,370);

    m_editAccount.move(150,40);
    m_editAccount.setFixedSize(480,30);
    m_editName.move(150,80);
    m_editName.setFixedSize(480,30);
    m_editSign.move(150,120);
    m_editSign.setFixedSize(480,80);
    m_editSex.move(150,210);
    m_editSex.setFixedSize(480,30);
    m_editBirthday.move(150,250);
    m_editBirthday.setFixedSize(480,30);
    m_editSite.move(150,290);
    m_editSite.setFixedSize(480,30);
    m_editTel.move(150,330);
    m_editTel.setFixedSize(480,30);
    m_editEmail.move(150,370);
    m_editEmail.setFixedSize(480,30);

    m_editAccount.setReadOnly(true);
    m_editName.setReadOnly(true);
    m_editSign.setReadOnly(true);
    m_editSex.setReadOnly(true);
    m_editBirthday.setReadOnly(true);
    m_editSite.setReadOnly(true);
    m_editTel.setReadOnly(true);
    m_editEmail.setReadOnly(true);

    m_btnClose.move(500,430);
    connect(&m_btnClose,&QPushButton::clicked,this,&FriendInfoDlg::SltBtnClicked);
    disconnect(m_pTitleBar->getCloseBtn(), &Button::clicked, this, &CBaseDialog::close);
    connect(m_pTitleBar->getCloseBtn(), &Button::clicked, this, &FriendInfoDlg::SltBtnClicked);
}

//Fixme:如果
void FriendInfoDlg::closeEvent(QCloseEvent *evt)
{
    Q_EMIT sigFriendInfoDlgClosed(m_id);
}

FriendInfoDlg::~FriendInfoDlg()
{
    qDebug() << "FriendInfoDlg::~FriendInfoDlg()";
}

void FriendInfoDlg::SltBtnClicked()
{
    this->close();
}

