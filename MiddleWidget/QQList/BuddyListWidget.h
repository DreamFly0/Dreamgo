﻿#ifndef BUDDYLISTWIDGET_H
#define BUDDYLISTWIDGET_H

#include <QWidget>
#include <QListWidget>
#include <QMap>

#include "QQCell.h"

class BuddyListGroup;
class ContactsWidget;
class ChatWin;
class FriendInfoDlg;
class QPropertyAnimation;
/**
 * @brief The BuddyListWidget class
 * 这个类负责显示分组下的所有好友，但是这个类不负责保存好友的信息，所有好友信息保存在BuddyListGroup中
 */
class BuddyListWidget : public QListWidget
{
    Q_OBJECT
public:
    explicit BuddyListWidget(QWidget *parent = 0);
    virtual ~BuddyListWidget();
    void initUi();
    void initMenu();

    ////it must be initialized
    void setBuddyListGroup(BuddyListGroup *group) { m_pGroup = group; }
    void setContactsWidget(ContactsWidget* w) { m_pContactsWidget = w; }

    ////other Fuction
    void addChildItem(QQCellChild* child);
    void removeChildItem(QQCellChild* child);
    bool removeChildItem(int userid);
    void setAutoLayoutSize();
    QQCellChild* getCurChild() { return m_pCurrentChild; }
private:
    QListWidgetItem* getListwidgetItemByUserid(int userid);
protected:
    virtual void resizeEvent(QResizeEvent *e);
    virtual void mousePressEvent(QMouseEvent*evt);
    virtual void mouseDoubleClickEvent(QMouseEvent *evt);
    virtual void mouseMoveEvent(QMouseEvent *evt);
    virtual void focusOutEvent(QFocusEvent *event);
    virtual void focusInEvent(QFocusEvent *event);
private:
    QMenu           *m_pMenu;
    BuddyListGroup* m_pGroup;
    ContactsWidget* m_pContactsWidget;
    QQCellChild*    m_pCurrentChild;        //当前好友,注意如果删除该好友，这个指针就没有用了，后期如果增加聊天窗口需要注意这个指针
    QMap<int,FriendInfoDlg*>    m_mapFriendInfoDlg;
    QHash<int,QPropertyAnimation*>     m_RecvMsgIds;       //发来消息的好友id
signals:
    void sigShowFriendInfo(QQCellChild*);
public slots:
    void startAnimation(int userid);
    void stopAnimation(int userid);
    void SltMenuClicked(QAction *action);
    void SltFriendInfoDlgClosed(int id);
    void SltShowFriendInfo(QQCellChild*child);
};

#endif // BUDDYLISTWIDGET_H
