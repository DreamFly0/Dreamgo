﻿#include "BuddyListChild.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPixmap>
#include <QMouseEvent>

#include "QQCell.h"
#include "global.h"

#include <QDebug>

BuddyListChild::BuddyListChild(QWidget *parent)
    : BasedWidget(parent),
      m_headBtn(this),
      m_labelName("张三",this),
      m_labelSign("签名",this),
      m_pCchild(NULL)
{
    this->setMouseTracking(true);
    m_labelName.setStyleSheet("color:rgb(38,38,38);font: 20px 黑体;");
    m_labelSign.setStyleSheet("color:rgb(102, 102, 102);font: 18px 黑体;");
    //m_labelSign.setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
    //m_headBtn.setFixedSize(48,48);
    m_headBtn.setObjectName("QQCellHeadBtn");
    m_headBtn.setFixedSize(64,64);
    m_headBtn.setIconSize(QSize(56,56));
    m_headBtn.setIcon(QPixmap(QString(":/Images/resource/head/%1.png").arg(0)));
    //m_pHeadBtn->setBackGround(QPixmap(":/Images/resource/head/0.png").scaled(m_labelHead.size(),Qt::IgnoreAspectRatio,Qt::SmoothTransformation));
    m_headBtn.setMouseTracking(true);
    m_labelName.setMouseTracking(true);
    m_labelSign.setMouseTracking(true);
    m_headBtn.setCursor(Qt::PointingHandCursor);
    this->setFixedHeight(80);
    m_headBtn.setToolTip(tr("View friends' information"));
    QHBoxLayout *hlayout = new QHBoxLayout();
    QVBoxLayout *vlayout = new QVBoxLayout();
    vlayout->addWidget(&m_labelName,0);
    vlayout->addWidget(&m_labelSign,0);
    vlayout->setSpacing(5);
    vlayout->setContentsMargins(5,10,5,10);
    hlayout->addWidget(&m_headBtn,0,Qt::AlignVCenter);
    hlayout->addLayout(vlayout);
    //hlayout->addStretch();
    hlayout->setContentsMargins(10,0,0,0);

    this->setLayout(hlayout);
    connect(&m_headBtn,&QPushButton::clicked,this,&BuddyListChild::SltClickedHeadBtn);
}

BuddyListChild::~BuddyListChild()
{
    qDebug() << "~BuddyListChild()";
}

void BuddyListChild::setQQCell(QQCellChild *child)
{
    m_pCchild = child;
    updateWinInfo();
}

void BuddyListChild::updateWinInfo()
{
    QPixmap pixmap = QPixmap(QString(":/Images/resource/head/%1.png").arg(m_pCchild->m_childInfo.facetype));

    if(m_pCchild->m_status == STATUS_OFFLINE)
    {
        pixmap = myHelper::ChangeGrayPixmap(pixmap.toImage());
    }

    m_headBtn.setIcon(pixmap);
    /*m_labelHead.setPixmap(pixmap.scaled(m_labelHead.size(),
                                        Qt::IgnoreAspectRatio,
                                        Qt::SmoothTransformation));
*/
    m_labelName.setText(m_pCchild->m_childInfo.nickname);
    QFontMetrics fm(m_labelSign.font());
    QString text2 = fm.elidedText(m_pCchild->m_childInfo.signature, Qt::ElideRight, m_labelSign.width());
    m_labelSign.setText(text2);
    //m_labelSign.setToolTip(m_pCchild->m_childInfo.signature);
}

void BuddyListChild::resizeEvent(QResizeEvent *evt)
{
    if(m_pCchild != NULL)
    {
        QFontMetrics fm(m_labelSign.font());
        QString text2 = fm.elidedText(m_pCchild->m_childInfo.signature, Qt::ElideRight, m_labelSign.width());
        m_labelSign.setText(text2);
    }
    QWidget::resizeEvent(evt);
}

void BuddyListChild::SltClickedHeadBtn()
{
    if(m_pCchild != NULL)
    {
        Q_EMIT sigShowFriendInfo(m_pCchild);
    }
}
