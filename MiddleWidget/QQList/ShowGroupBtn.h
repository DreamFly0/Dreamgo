﻿#ifndef SHOWGROUPBTN_H
#define SHOWGROUPBTN_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QMenu>
#include <QPoint>
#include <QLineEdit>

class BuddyListGroup;

class ShowGroupBtn : public QPushButton
{
    Q_OBJECT
public:
    explicit ShowGroupBtn(QWidget *parent = 0);

    void init();
    void initMenu();
    void setBuddyGroup(BuddyListGroup* p);
    void setEnabledMenuItem(bool isSetting=false); //menu!!
    void setListName(const QString& strText){m_BuddylistName.setText(strText);}
    void updateFriendCnt(int onlineCnt,int Cnt);
    QString getListName(){return m_BuddylistName.text();}
    bool ShowChild() { return m_isShowChild; }
    void showLabel(bool show);
public slots:
    void SltUpdateFriendCount();
    void SltMenuRequest(){ m_btnMenu.exec(QCursor::pos()); }
    void SltRetrunPressed();
    void SltRename();
    void SltClicked();
    //void slot_reNameDB(const QString&);

signals:
    void sigAddGroup();
    void sigDelGroup();
    void sigAddFriend();
    void sigRename(const QString&);
protected:
    virtual void mousePressEvent(QMouseEvent*evt);
    virtual void mouseMoveEvent(QMouseEvent*evt);
    virtual bool eventFilter(QObject *obj, QEvent *evt);
    //virtual void dragLeaveEvent(QDragLeaveEvent*);
    //virtual void dragMoveEvent(QDragMoveEvent*);
    //virtual void dragEnterEvent(QDragEnterEvent*);
    //virtual void dropEvent(QDropEvent*);


    //virtual void mouseReleaseEvent(QMouseEvent *e);
    //virtual bool eventFilter(QObject *, QEvent *);
    //virtual void paintEvent(QPaintEvent *);
private:
    QLabel          m_indicator;
    QMenu           m_btnMenu;
    QLineEdit       m_lineEdit;
    BuddyListGroup  *m_pBuddyGroup;
    QAction         *m_pAddGroup;
    QAction         *m_pDelGroup;
    QAction         *m_pRenameGroup;
    QLabel          m_BuddylistName;
    QLabel          m_labelFriendCnt;
    bool            m_isdrawTop;
    bool            m_isdrawMove;
    bool            m_isShowChild;
    QPoint          m_presspos;
};

#endif // SHOWGROUPBTN_H
