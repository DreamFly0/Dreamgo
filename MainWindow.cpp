﻿#include "MainWindow.h"
#include "TopWidget.h"
#include "MiddleWidget.h"
#include "BottomWidget.h"
#include "ClientSocket.h"
#include "global.h"
#include "Msg.h"
#include "MyApp.h"
#include "ExitDlg.h"
#include "RequestAdduserDlg.h"
#include "QQCell.h"
#include "ChatWin.h"
#include "LoginAnimationWin.h"

#include <QMouseEvent>
#include <QIcon>
#include <QVBoxLayout>
#include <QCloseEvent>
#include <QJsonArray>

MainWindow *MainWindow::s_pMainWnd = nullptr;
QString MainWindow::s_strUserNickName = "";
int MainWindow::s_facetype = 0;
quint8   MainWindow::s_tcpStatus;
bool   MainWindow::s_isTransFile = false;
MainWindow *MainWindow::InitInstance()
{
    if(!s_pMainWnd)
        s_pMainWnd=new MainWindow;
    return s_pMainWnd;
}

MainWindow::MainWindow(QWidget *parent) :
    BaseWindow(parent),
    m_msgtype(0),
    m_msgtype1(0),
    m_SysTrayTwinkle(0)
{
    this->setObjectName("MainWindow");
    initUi();
    InitSysTrayIcon();
    initConnect();
}

MainWindow::~MainWindow()
{
    qDebug() << "~MainWindow()" << m_hashChilds.size();

    delete m_pExitDlg;
    delete m_pRequestAdduserDlg;
    MyApp::SetSettingFile(MyApp::m_strUserCfgFile,"UserInfo","FaceID",m_userInfo.facetype);
    MyApp::SetSettingFile(MyApp::m_strUserCfgFile,"UserConf","PlaySound",MyApp::m_bPlaySound);
    MyApp::SetSettingFile(MyApp::m_strUserCfgFile,"UserConf","ShowExitDlg",MyApp::m_bShowExitDlg);
    MyApp::SetSettingFile(MyApp::m_strUserCfgFile,"UserConf","MainWindowHide",MyApp::m_bMainWindowHide);
    MyApp::SetSettingFile(MyApp::m_strUserCfgFile,"MainDlg","position",pos());
    MyApp::SetSettingFile(MyApp::m_strUserCfgFile,"MainDlg","size",size());
    foreach(QQCellChild* item,m_hashChilds)
    {
        delete item;
    }
    qApp->quit();
    //SltQuitApp();
}

void MainWindow::initConnect()
{
    connect(m_pTopWidget->m_pTitleBar->getCloseBtn(), &QPushButton::clicked, this, &MainWindow::SltClickedCloseBtn);
    connect(m_pTopWidget->m_pTitleBar->getMinimizeBtn(), &QPushButton::clicked, this, &MainWindow::hide);
    connect(&(m_pTopWidget->m_pUserInfoDlg->m_btnOK),&QPushButton::clicked,this,&MainWindow::SltModifiUserInfo);
    connect(&(m_pTopWidget->m_pUserInfoDlg->m_btnCancel),&QPushButton::clicked,this,&MainWindow::SltCancelModifiUserInfo);
    connect(m_pExitDlg,&ExitDlg::sigMainWinHide,this,&MainWindow::SltMainWinExit);
    connect(&m_pRequestAdduserDlg->m_btnGroup,static_cast<void (QButtonGroup::*)(int)>(&QButtonGroup::buttonClicked),this,&MainWindow::SltReplyAddUser);
    connect(m_pBottomWidget, &BottomWidget::sigBottomMenu, this, &MainWindow::SltSysmenuCliecked);
}

//跟新TopWidget登录用户信息
void MainWindow::updateUserInfo()
{
    m_pTopWidget->m_pUserInfoDlg->m_editName.setText(m_userInfo.nickname);
    m_pTopWidget->m_pUserInfoDlg->m_labelAccount1.setText(m_userInfo.username);
    m_pTopWidget->m_pUserInfoDlg->setHeadPixmapIndex(m_userInfo.facetype);
    m_userInfo.gender == 0 ? (m_pTopWidget->m_pUserInfoDlg->m_btnMan.setChecked(true)) :
                             (m_pTopWidget->m_pUserInfoDlg->m_btnWoman.setChecked(true));
    m_pTopWidget->m_pUserInfoDlg->m_editSign.setPlainText(m_userInfo.signature);
    m_pTopWidget->m_pUserInfoDlg->m_editSite.setText(m_userInfo.address);
    m_pTopWidget->m_pUserInfoDlg->m_editTel.setText(m_userInfo.phonenumber);
    m_pTopWidget->m_pUserInfoDlg->m_editEmail.setText(m_userInfo.mail);
    m_pTopWidget->m_pUserInfoDlg->setDate(m_userInfo.birthday);

    m_pTopWidget->m_labelNickName.setText(m_userInfo.nickname);
    m_pTopWidget->m_lineEditSign.setText(m_userInfo.signature);
    //m_pTopWidget->m_btnSign->setText(m_userInfo.signature);
    m_pTopWidget->SltBtnSignText(m_userInfo.signature);
}

/**
 * @brief MainWindow::setUserInfo 连接socket信号，更新用户信息
 * @param info
 * @date 2018/9/28
 */
void MainWindow::setUserInfo(const QJsonObject& info)
{
    if(info.isEmpty())
    {
        CMessageBox::Infomation(tr("User information update failed"));
        return ;
    }
    m_userInfo.userid = info.value("userid").toInt();
    m_userInfo.username = info.value("username").toString();
    m_userInfo.nickname = info.value("nickname").toString();
    m_userInfo.facetype = info.value("facetype").toInt();
    m_userInfo.customface = info.value("customface").toString();
    m_userInfo.gender = info.value("gender").toInt();
    m_userInfo.birthday = QString("%1").arg(info.value("birthday").toInt());
    m_userInfo.signature = info.value("signature").toString();
    m_userInfo.address = info.value("address").toString();
    m_userInfo.phonenumber = info.value("phonenumber").toString();
    m_userInfo.mail = info.value("mail").toString();

    MainWindow::s_strUserNickName = m_userInfo.nickname;
    MainWindow::s_facetype = m_userInfo.facetype;
    //托盘气泡信息更新
    m_pSysTrayIcon->setToolTip(QString("Flamingo:%1(%2)").arg(m_userInfo.nickname).arg(m_userInfo.userid));
    updateUserInfo();
}

void MainWindow::SltTcpStatus(const quint8 &status)
{
    //qDebug() << "MainWindow::SltTcpStatus";
    if(status == DisConnectedHost)
    {
        s_tcpStatus = DisConnectedHost;
        m_pTopWidget->m_pTitleBar->setWinTitle(tr("The server is down"));
        QPixmap pixmap = m_pTopWidget->m_pHeadBtn->getBackGround();
        pixmap = myHelper::ChangeGrayPixmap(pixmap.toImage());
        m_pTopWidget->m_pHeadBtn->setBackGround(pixmap);
        m_pSysTrayIcon->setIcon(QIcon(":/Images/resource/app_2.ico"));
    }else if(ConnectedHost == status)
    {
        qDebug() << "MainWindow::SltTcpStatus : " << "ConnectedHost";
        m_pSysTrayIcon->setIcon(QIcon(":/Images/resource/app_1.ico"));
        s_tcpStatus = ConnectedHost;
        m_pTopWidget->m_pTitleBar->setWinTitle(tr(""));
        ///m_pTopWidget->m_pTitleBar->setWinTitle("");
        ///m_pTopWidget->setHeadPixmapIndex(m_userInfo.facetype);

        //重新连接以后需要登录
        //Send msg...
        // 构建 Json 对象
        QJsonObject json;
        json.insert("username", MyApp::m_strUserName);
        json.insert("password", MyApp::m_strUserPwd);
        json.insert("clienttype", CLIENT_TYPE_PC);
        json.insert("status",STATUS_ONLINE);

        Q_EMIT sigSend(msg_type_login,json);
        Q_EMIT sigSend(msg_type_getofriendlist,QJsonObject());
    }
}

void MainWindow::SltSysmenuCliecked(int types)
{
    switch ( types ) {
    case BottomWidget::SysSetting:
        break;
    case BottomWidget::MsgManager:
        break;
    case BottomWidget::FileAssistant:
        CMessageBox::Infomation(tr("View file assistant"));
        break;
    case BottomWidget::ModifyPwd:
        break;
    case BottomWidget::Help:
        break;
    case BottomWidget::Reconnect:
        Q_EMIT sigConnectToHost();
        break;
    case BottomWidget::Update:
        if (QDialog::Accepted == CMessageBox::Question(tr("A new version has been detected. Do you want to upgrade?")))
        {
            CMessageBox::Infomation(tr("System upgrade successful"));
        }
        break;
    default:
        break;

    }
}

//托盘菜单
void MainWindow::InitSysTrayIcon()
{
    m_pSysTrayIcon = new QSystemTrayIcon(this);
    m_ptimerSysTray = new QTimer(this);
    m_pSysTrayIcon->setIcon(QIcon(":/Images/resource/app_1.ico"));

    QMenu *m_trayMenu = new QMenu(this);
    m_trayMenu->addAction(tr("Online"));
    m_trayMenu->addAction(tr("Offline"));
    m_trayMenu->addSeparator();
    m_trayMenu->addAction(tr("Lock Dreamgo"));
    m_trayMenu->addSeparator();
    m_trayMenu->addAction(tr("Turn off all sounds"));
    m_trayMenu->addSeparator();
    m_trayMenu->addAction(tr("Display main panel"));
    m_trayMenu->addSeparator();
    m_trayMenu->addAction(tr("Exit"));

    m_pSysTrayIcon->setContextMenu(m_trayMenu);
    //m_pSysTrayIcon->show();

    connect(m_pSysTrayIcon,&QSystemTrayIcon::activated,this,&MainWindow::SltTrayIcoClicked);
    connect(m_trayMenu,&QMenu::triggered,this,&MainWindow::SltTrayIconMenuClicked);
    connect(m_ptimerSysTray,&QTimer::timeout,this,&MainWindow::SltSetTrayIcon);
}

//关闭主窗口前看是否有文件还在传输
void MainWindow::closeEvent(QCloseEvent *e)
{

    QList<ChatWin*>&& list = m_pMiddleWidget->getChatWinList();
    ChatWin* wgt;
    foreach(wgt,list)
    {
        if(wgt->getTransFileCnt()>0)
        {
            int retCode = CMessageBox::Question(tr("File is being transferred. Closing the window will terminate the transfer. Are you sure you want to close it?"));
            if(retCode == QDialog::Accepted)
            {
                e->accept();
            }else
            {
                e->ignore();            //不要关闭本窗口
                return;
            }
        }
    }
}
/*
void MainWindow::closeEvent(QCloseEvent *evt)
{
    qDebug() << "MainWindow::closeEvent(QCloseEvent *evt)";
}
*/
/**
 * @brief MainWindow::SltModifiUserInfo 通知服务器更新用户资料，界面暂时保持原有资料不变
 * @date 2018/9/28
 */
void MainWindow::SltModifiUserInfo()
{
    if(DisConnectedHost == s_tcpStatus)
    {
        CMessageBox::Infomation(tr("Network failure, update failed!"));
        updateUserInfo();
        return;
    }

    // 构建 Json 对象
    QJsonObject json;
    json.insert("nickname",m_pTopWidget->m_pUserInfoDlg->m_editName.text());
    json.insert("facetype",m_pTopWidget->m_pUserInfoDlg->getHeadPixmapIndex());
    json.insert("customface","");
    json.insert("gender",m_pTopWidget->m_pUserInfoDlg->m_btnGroup.checkedId());
    json.insert("birthday",m_pTopWidget->m_pUserInfoDlg->getDate().toInt());
    json.insert("signature",m_pTopWidget->m_pUserInfoDlg->m_editSign.toPlainText());
    json.insert("address",m_pTopWidget->m_pUserInfoDlg->m_editSite.text());
    json.insert("phonenumber",m_pTopWidget->m_pUserInfoDlg->m_editTel.text());
    json.insert("mail",m_pTopWidget->m_pUserInfoDlg->m_editEmail.text());

    //QJsonDocument jsonData(json);
    Q_EMIT sigSend(msg_type_updateuserinfo,json);

    //updateUserInfo();
    m_pTopWidget->m_pUserInfoDlg->close();
    m_pTopWidget->m_lineEditSign.setText(m_pTopWidget->m_pUserInfoDlg->m_editSign.toPlainText());

}

/**
 * @brief MainWindow::SltCancelModifiUserInfo   重置用户信息
 * @date 2018/9/28
 */
void MainWindow::SltCancelModifiUserInfo()
{
    updateUserInfo();
    m_pTopWidget->m_pUserInfoDlg->close();
}

void MainWindow::SltTrayIcoClicked(QSystemTrayIcon::ActivationReason reason)
{
    //qDebug() << "MainWindow::SltTrayIcoClicked" << reason;
    switch (reason)
    {
    case QSystemTrayIcon::Trigger:
    {
        if(msg_type_operatefriend == m_msgtype)
        {
            //清除消息状态,托盘图标停止闪烁
            m_msgtype = 0;
            m_ptimerSysTray->stop();
            SltSetTrayIcon();
            if(msg_type_addfriend == m_msgtype1)
            {
                //弹出好友请求对话框
                m_pRequestAdduserDlg->m_labelTips.setText(QString("(%1)请求添加您为好友，是否同意？").arg(m_requestUsername));
                m_pRequestAdduserDlg->setModal(true);
                m_pRequestAdduserDlg->showNormal();
                m_pRequestAdduserDlg->activateWindow();
            }else if(msg_type_replyAddFriend == m_msgtype1)
            {
                if(m_addFriendResult == 1)
                {
                    QString msg = QString("您和[%1]已经是好友，可以开始聊天了").arg(m_operateUsername);
                    CMessageBox::Infomation(msg,"添加好友");
                }
                else
                {
                    QString msg = QString("您或[%1]拒绝了添加好友请求").arg(m_operateUsername);
                    CMessageBox::Infomation(msg,"添加好友");
                }
            }
        }else
        {   //没有消息，点击托盘显示窗口
            showNormal();
            activateWindow();
        }
    }
        break;
    case QSystemTrayIcon::DoubleClick:
    {
    }
        break;
    default:
        break;
    }
}

void MainWindow::SltTrayIconMenuClicked(QAction* action)
{
    if ("离开" == action->text()) {
        //qDebug() << "MainWindow::SltTrayIconMenuClicked";
        this->close();
    }
    else if ("显示主面板" == action->text()) {
        this->show();
        this->activateWindow();
    }
    else if (!QString::compare("在线", action->text()))
    {
        if(DisConnectedHost == s_tcpStatus)
            Q_EMIT sigConnectToHost();
    }
    else if (!QString::compare("离线", action->text()))
    {
        Q_EMIT sigDisConnToHost();
    }
    else if (!QString::compare("锁定 Dreamgo", action->text()))
    {
        //m_tcpSocket->ColseConnected();
    }
    else if (!QString::compare("关闭所有提示音", action->text()))
    {
        MyApp::m_bPlaySound = false;
        //m_tcpSocket->ColseConnected();
    }
}

void MainWindow::SltQuitApp()
{
    qDebug() << "MainWindow::SltQuitApp";
    qApp->quit();
}

//单击关闭按钮
void MainWindow::SltClickedCloseBtn()
{
    if(MyApp::m_bShowExitDlg)
    {
        //打开退出提示框
        m_pExitDlg->show();
        //m_pExitDlg->setModal(true);
    }else{
        if(MyApp::m_bMainWindowHide)
        {
            this->hide();
        }else
        {
            this->close();
        }
    }
}

//点击关闭按钮弹出对话框，选择隐藏主界面还是关闭
void MainWindow::SltMainWinExit(bool hide)
{
    if(m_pExitDlg->m_box.isChecked())
    {
        MyApp::m_bShowExitDlg = false;
    }
    if(hide)
    {
        qDebug() << "隐藏";
        MyApp::m_bMainWindowHide = true;
        this->hide();
        //this->setVisible(false);
    }else
    {
        qDebug() << "关闭";
        MyApp::m_bMainWindowHide = false;
        m_pSysTrayIcon->hide();
        this->close();
    }
}

//对方请求添加我为好友
void MainWindow::SltRequestAddUser(const QJsonObject &info)
{
    //播放声音
    if(MyApp::m_bPlaySound)
        myHelper::PlaySound("Global");

    int userid = info.value("userid").toInt();
    QString username = info.value("username").toString();
    m_requestUserid = userid;
    m_requestUsername = username;
    //设置托盘图标闪烁
    m_msgtype = msg_type_operatefriend;
    m_msgtype1 = msg_type_addfriend;
    m_ptimerSysTray->start(500);
}

//回复对方是否同意添加为好友
void MainWindow::SltReplyAddUser(int select)
{
    //cmd = 1005, seq = 0, {"userid": 9, "type": 3, "username": "xxx", "accept": 1(1同意，0拒绝)}
    QJsonObject json;
    json.insert("userid",m_requestUserid);
    json.insert("type",msg_type_replyAddFriend);
    json.insert("username",m_requestUsername);
    json.insert("accept",select);

    Q_EMIT  sigSend(msg_type_operatefriend,json);
}

//设置托盘图标
void MainWindow::SltSetTrayIcon()
{
    m_SysTrayTwinkle = (m_SysTrayTwinkle+1)%2;
    if(msg_type_operatefriend == m_msgtype)
    {
        m_pSysTrayIcon->setIcon(QIcon(QString(":/Images/resource/speaker_%1.ico").arg(m_SysTrayTwinkle)));
    }else
    {
        if(DisConnectedHost == s_tcpStatus)
        {
            m_pSysTrayIcon->setIcon(QIcon(":/Images/resource/app_2.ico"));
        }else if(ConnectedHost == s_tcpStatus)
        {
            m_pSysTrayIcon->setIcon(QIcon(":/Images/resource/app_1.ico"));
        }
    }
}

//添加好友结果
void MainWindow::SltAddUserResult(QString username, int accept)
{
    //播放声音
    if(MyApp::m_bPlaySound)
        myHelper::PlaySound("Global");

    //设置托盘图标闪烁
    m_msgtype = msg_type_operatefriend;
    m_msgtype1 = msg_type_replyAddFriend;
    m_addFriendResult = accept;
    m_operateUsername = username;
    m_ptimerSysTray->start(500);
}

void MainWindow::SltReplyUserExisted(int userid, QString username)
{
    Q_UNUSED(userid);
    CMessageBox::Infomation(tr("[%1] is already your friend, please do not add again").arg(username),tr("Add Buddy"));
}

/**
 * @brief MainWindow::setSocket 设置主窗口socket
 * @param socket
 * @param info  更新用户信息
 */
void MainWindow::setSocket(ClientSocket *socket)
{
    m_pMiddleWidget->setSocket(socket);
    m_pBottomWidget->setSocket(socket,m_userInfo.userid);

    s_tcpStatus = ConnectedHost;
    m_pClientSocket = socket;
    connect(this,&MainWindow::sigSend,m_pClientSocket,&ClientSocket::SltSend);

    //信号槽-------------------------------------------------------------------------
    connect(this,&MainWindow::sigConnectToHost,m_pClientSocket,&ClientSocket::ConnectToHost);
    connect(this,&MainWindow::sigDisConnToHost,m_pClientSocket,&ClientSocket::CloseConnected);
    connect(m_pClientSocket,&ClientSocket::sigUserInfo,this,&MainWindow::setUserInfo);
    connect(m_pClientSocket,&ClientSocket::sigRequestAddUser,this,&MainWindow::SltRequestAddUser);
    connect(m_pClientSocket,&ClientSocket::sigAddUserResult,this,&MainWindow::SltAddUserResult);
    connect(m_pClientSocket,&ClientSocket::sigFriendList,this,&MainWindow::SltGotFriendList);
    connect(m_pClientSocket,&ClientSocket::sigDelFriend,this,&MainWindow::SltDelFriendUpdateWidget);
    connect(m_pClientSocket,&ClientSocket::sigUpdateUserStatus,this,&MainWindow::SltUpdateUserStatus);
    connect(m_pClientSocket,&ClientSocket::sigChatMsg,this,&MainWindow::SltRecvChatMsg);
    connect(m_pClientSocket,&ClientSocket::sigUpdateFriendInfo,this,&MainWindow::SltUpdateFriendInfo);
    connect(m_pClientSocket,&ClientSocket::sigReplyUserExisted,this,&MainWindow::SltReplyUserExisted);
}

//打开主窗口，必须在此处连接这个信号槽，避免和登录窗口的这个槽函数冲突
void MainWindow::showMainWin()
{
    this->setAttribute(Qt::WA_DeleteOnClose);
    this->showNormal();
    this->m_pSysTrayIcon->show();
    this->activateWindow();
    this->setAttribute(Qt::WA_QuitOnClose,true);//这个属性一定需要在show后面设置，不然登录窗口关闭以后主界面直接退出
    //此处必须再设置一次个人信息
    //原因：topwidget中的签名需要在resizeevent执行以后，签名区域的widget宽度改变以后再设置签名信息
    this->updateUserInfo();
    connect(m_pClientSocket,&ClientSocket::signalStatus,this,&MainWindow::SltTcpStatus);
}

void MainWindow::SltGotFriendList(const QJsonObject &jsonArray)
{
    QJsonArray friendList = jsonArray.value("userinfo").toArray();
    for(int i = 0;i<friendList.size();i++)
    {
        QJsonObject info = friendList[i].toObject();

        User userInfo;
        userInfo.userid = info.value("userid").toInt();
        userInfo.username = info.value("username").toString();
        userInfo.nickname = info.value("nickname").toString();
        userInfo.facetype = info.value("facetype").toInt();
        userInfo.customface = info.value("customface").toString();
        userInfo.gender = info.value("gender").toInt();
        userInfo.birthday = QString("%1").arg(info.value("birthday").toInt());
        userInfo.signature = info.value("signature").toString();
        userInfo.address = info.value("address").toString();
        userInfo.phonenumber = info.value("phonenumber").toString();
        userInfo.mail = info.value("mail").toString();
        int Status = info.value("status").toInt();
        QQCellChild *child = NULL;
        if(m_hashChilds.contains(userInfo.userid))
        {
            //如果好友存在，刷新好友信息即可
            child = m_hashChilds[userInfo.userid];
            child->m_childInfo = userInfo;
            child->m_id = userInfo.userid;
            child->m_status = Status;

            //如果存在聊天窗口更新其对应好友个人资料
            if(child->m_pChatWin != NULL)
            {
                ChatWin* chatwin = child->m_pChatWin;
                chatwin->UpdateWinInfo();
            }
        }
        else
        {
            child = new QQCellChild();
            child->m_childInfo = userInfo;
            child->m_id = userInfo.userid;
            child->m_status = Status;
            m_hashChilds.insert(child->m_id,child);
        }
        m_pMiddleWidget->addFriendToGroup(child);
        //m_pContactsWidget->addFriendToGroup(userInfo,info.value("status").toInt());
    }
    //qDebug() << Array;
    //加载离线消息
    for(int i = 0;i<m_listCachedChatMsg.size();i++)
    {
        const QPair<QJsonObject,int>& chatMsg = m_listCachedChatMsg.at(i);
        int senderid = chatMsg.second;
        if(m_hashChilds.contains(senderid))
        {
            const QJsonObject& json = chatMsg.first;
            const QString& groupName = m_hashChilds[senderid]->m_strGroupName;
            m_pMiddleWidget->recvChatMsg(json,senderid,m_hashChilds[senderid],groupName);
        }
    }
    m_listCachedChatMsg.clear();    //清空离线消息列表
}

void MainWindow::SltUpdateUserStatus(int userid, int status)
{
    //播放声音
    if(MyApp::m_bPlaySound)
        myHelper::PlaySound("system");
    if(m_hashChilds.contains(userid))
    {
        QQCellChild* child = m_hashChilds[userid];
        child->m_status = status;
        m_pMiddleWidget->updateUserStatus(userid,child->m_strGroupName);
    }
}

//更新好友个人资料
void MainWindow::SltUpdateFriendInfo(const QJsonObject &jsonObj)
{
    const QJsonObject& info = jsonObj.value("userinfo").toObject();
    User userInfo;
    userInfo.userid = info.value("userid").toInt();
    userInfo.username = info.value("username").toString();
    userInfo.nickname = info.value("nickname").toString();
    userInfo.facetype = info.value("facetype").toInt();
    userInfo.customface = info.value("customface").toString();
    userInfo.gender = info.value("gender").toInt();
    userInfo.birthday = QString("%1").arg(info.value("birthday").toInt());
    userInfo.signature = info.value("signature").toString();
    userInfo.address = info.value("address").toString();
    userInfo.phonenumber = info.value("phonenumber").toString();
    userInfo.mail = info.value("mail").toString();
    int Status = info.value("status").toInt();
    QQCellChild *child = nullptr;
    if(m_hashChilds.contains(userInfo.userid))
    {
        //如果好友存在，刷新好友信息即可
        child = m_hashChilds[userInfo.userid];
        child->m_childInfo = userInfo;
        child->m_id = userInfo.userid;
        child->m_status = Status;

        //如果存在聊天窗口更新其对应好友个人资料
        if(child->m_pChatWin != nullptr)
        {
            ChatWin* chatwin = child->m_pChatWin;
            chatwin->UpdateWinInfo();
        }
    }
    else
    {
        child = new QQCellChild();
        child->m_childInfo = userInfo;
        child->m_id = userInfo.userid;
        child->m_status = Status;
        m_hashChilds.insert(child->m_id,child);
    }
    m_pMiddleWidget->addFriendToGroup(child);
}

void MainWindow::SltDelFriendUpdateWidget(int userid)
{
    QQCellChild* child= m_hashChilds.take(userid);
    m_pMiddleWidget->delFriendUpdateWidget(userid);
    delete child;
    child = NULL;
}

void MainWindow::SltRecvChatMsg(const QJsonObject &json, int senderid)
{
    if(MyApp::m_bPlaySound)
        myHelper::PlaySound("msg");
    if(m_hashChilds.contains(senderid))
    {
        const QString& groupName = m_hashChilds[senderid]->m_strGroupName;
        m_pMiddleWidget->recvChatMsg(json,senderid,m_hashChilds[senderid],groupName);
    }else
    {
        //如果是刚登录，那么服务端会先于好友列表发送离线消息，现将离线消息缓存起来，等获取了好友列表以后再推送到好友列表
        m_listCachedChatMsg.append(qMakePair(json, senderid));
    }
}

void MainWindow::initUi()
{
    //this->setWindowFlags(Qt::FramelessWindowHint);
    //setAttribute(Qt::WA_TranslucentBackground,true);
    //setFixedSize(360,680);
    //setMinimumSize(0, 0);
    //设置背景图片
    this->setAutoFillBackground(true); // 这句要加上, 否则可能显示不出背景图.
    //this->setAttribute(Qt::WA_QuitOnClose);

    //this->setWindowFlags(windowFlags() | Qt::Tool | Qt::WindowStaysOnTopHint);
    this->setWindowFlags(windowFlags() | Qt::Tool);
    this->setWindowIcon(QIcon(":/Images/resource/flamingo.ico"));
    this->setMinimumSize(380,700);
    this->resize(360,700);
    this->setMaximumSize(720,920);
    m_pExitDlg = new ExitDlg();
    m_pRequestAdduserDlg = new RequestAdduserDlg();

    m_pTopWidget = new TopWidget(this);
    m_pMiddleWidget = new MiddleWidget(this);
    m_pBottomWidget = new BottomWidget(this);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->setSpacing(0);
    layout->setContentsMargins(0,0,0,0);

    layout->addWidget(m_pTopWidget);
    layout->addWidget(m_pMiddleWidget);
    layout->addWidget(m_pBottomWidget);
    //m_pTopWidget->hide();
    //m_pBottomWidget->hide();
    //m_pMiddleWidget->hide();
    m_MainWin.setLayout(layout);
}
