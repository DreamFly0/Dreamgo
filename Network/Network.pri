HEADERS += \
    $$PWD/ClientSocket.h \
    $$PWD/Msg.h \
    $$PWD/protocolstream.h \
    $$PWD/ClientFileSocket.h \
    $$PWD/FileMsg.h \
    $$PWD/CFileTask.h

SOURCES += \
    $$PWD/ClientSocket.cpp \
    $$PWD/protocolstream.cc \
    $$PWD/ClientFileSocket.cpp \
    $$PWD/CFileTask.cpp
