﻿#ifndef CLIENTFILESOCKET_H
#define CLIENTFILESOCKET_H

#include <QObject>
#include <QAtomicInt>

class QTcpSocket;

class ClientFileSocket : public QObject
{
    Q_OBJECT
public:
    explicit ClientFileSocket(QObject *parent = nullptr);
    ~ClientFileSocket();
    bool IsConneciton();
    void StopSend();
    void StopRecv();
private:
    void InitSocket();
    void SendOnFilePort();
private:
    QTcpSocket*         m_pTcpSocket;
    int32_t             m_seq;
    QAtomicInt          m_nSendStop;
    QAtomicInt          m_nRecvStop;
    static int          m_sWaitForTimeout;
signals:
    void sigTransFileStatus(qint8 status);              //向ChatWin返回文件传输的状态
    void sigUpdateTransFileProgress(int value);         //更新传输文件的进度条
public slots:
    void ConnectToHost();
    void CloseFileServerConnection();
    void SltUploadFile(QString fileName,QString fileMd5);
    void SltDownloadFile(QString filePath, QString fileMd5);
private:
    bool SendPackage(const char* pBuffer, qint64 length);
    bool ReadPackage(QByteArray& buffer, qint64 nSize);
    bool ReadPackage(char* pBuffer, qint64 nSize);
};

#endif // CLIENTFILESOCKET_H
