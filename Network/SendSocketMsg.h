﻿#ifndef SENDSOCKETMSG_H
#define SENDSOCKETMSG_H

#include <QObject>
#include <QTcpSocket>
class SendSocketMsg : public QObject
{
    Q_OBJECT
public:
    explicit SendSocketMsg(QTcpSocket* socket, QObject *parent = 0);
private:
    QTcpSocket      *m_pTcpSocket;
    int             m_seq{};
signals:
private:
    void SendPackage(const char* str, int32_t length);
public slots:
    //发送消息
    void SltSendMsg(int32_t cmd, const QJsonDocument& data);
};
#endif // SENDSOCKETMSG_H
