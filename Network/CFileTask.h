﻿#ifndef CFILETASK_H
#define CFILETASK_H

#include <QObject>
#include <QThread>
#include <QList>
#include <QMutex>
#include <QWaitCondition>

class TransFileWgt;
class ClientFileSocket;
class CFileTask : public QThread
{
    Q_OBJECT
public:
    explicit CFileTask(ClientFileSocket* fSocket,QObject *parent = nullptr);
    ~CFileTask();
    void Stop();
    bool AddSendItem(TransFileWgt* pItem);
    bool AddRecvItem(TransFileWgt* pItem);
    void RemoveSendItem(TransFileWgt* pItem);           //移除队列中上传的项
    void RemoveRecvItem(TransFileWgt* pItem);           //移除队列中下载的项
    void ClearAllItems();

    void HandleItem(TransFileWgt* pFileItem);
protected:
    virtual void run();
private:
    ClientFileSocket*       m_pFileSocket;
    TransFileWgt*           m_pCurrentSendTask;         //当前正在传输的Task
    QList<TransFileWgt*>    m_SendFileTasks;            //发送任务队列
    QList<TransFileWgt*>    m_RecvFileTasks;            //接受任务队列
    QMutex                  m_mutex;
    QWaitCondition          m_conditon;
    bool                    m_bStop;                    //停止任务线程
    bool                    m_bBusy;                    //当前传输任务是否空闲
signals:
    void sigUploadFile(QString filePath,QString fileMd5);
    void sigDownloadFile(QString filePath,QString fileMd5);
public slots:
    void SltTransFileStatus(qint8 status);
};

#endif // CFILETASK_H
