﻿/**
* 协议类型定义,Msg.h
* 2018.8.14
**/

#ifndef MSG_H
#define MSG_H

#include <stdint.h>

enum msg_type
{
	msg_type_unknown,
	msg_type_heartbeart = 1000,
	msg_type_register,
	msg_type_login,
	msg_type_getofriendlist,
	msg_type_finduser,
	msg_type_operatefriend,
	msg_type_userstatuschange,
	msg_type_updateuserinfo,
	msg_type_modifypassword,
	msg_type_creategroup,
	msg_type_getgroupmembers,
	msg_type_chat = 1100,         //单聊消息
	msg_type_multichat,             //群发消息
};

#define msg_type_addfriend 1
#define msg_type_replyAddFriend 3
#define msg_type_delFriend 4

#pragma pack(push,1)
struct msg
{
    int32_t packagesize;		//指定包体的大小
};
#pragma pack(pop)

/**
 *  错误码
 *  0   成功
 *  1   未知失败
 *  2   用户未登录
 *  100 注册失败
 *  101 已注册
 *  102 未注册
 *  103 密码错误
 *  104 更新用户信息失败
 *  105 修改密码失败
 *  106 创建群失败
 *  107 客户端版本太旧，需要升级成新版本
 */
//TODO: 其他的地方改成这个错误码
enum error_code
{
    error_code_ok                   = 0,
    error_code_unknown              = 1,
    error_code_notlogin             = 2,
    error_code_registerfail         = 100,
    error_code_registeralready      = 101,
    error_code_notregister          = 102,      //Login: not registered
    error_code_invalidpassword      = 103,      //Login: incorrect password
    error_code_updateuserinfofail   = 104,
    error_code_modifypasswordfail   = 105,
    error_code_creategroupfail      = 106,
    error_code_toooldversion        = 107
};

enum CHAT_MSG_TYPE
{
    FMG_MSG_TYPE_UNKNOWN,
    FMG_MSG_TYPE_BUDDY,
    FMG_MSG_TYPE_GROUP,
    FMG_MSG_TYPE_MULTI,		//群发消息
    FMG_MSG_TYPE_SESS
};

//typedef NS_ENUM(int, IUKMessageInsideType) {// 聊天消息类型
//    IUKMessageInsideTypeUndefined   = 0,// 未知消息类型
//    IUKMessageInsideTypeText        = 1,// 文本类型
//    IUKMessageInsideTypeImage       = 2,// 图片类型  pc专用 （pc在上传图片成功后 补发的消息 用于去下载  pc和app端都要处理）
//    IUKMessageInsideTypeFile        = 3,// 文件类型
//    IUKMessageInsideTypeShake       = 4,// 窗口抖动  pc专用
//    IUKMessageinsideTypeApp         = 5 // app端发出的图片  （app端发送的图片信息，已经上传好才会发送的这一条  pc接到这个类型可以直接显示）
//};

#define FMG_MSG_TYPE_WORD 1                 // 文本类型
#define FMG_MSG_TYPE_PICTURE 2              // 图片类型  pc专用

/**
 *  心跳包协议
 **/
/*
    send : cmd = 1000, seq = 0
    recv : cmd = 1000, seq = 0+1
 **/

/**
 *  注册协议
 **/
/*
    send : cmd = 1001, seq = 0,  {"username": "13917043329", "nickname": "balloon", "password": "123"}
    recv : cmd = 1001, seq = 0+1,  {"code": 0, "msg": "ok"}                     注册成功
                                   {"code": 100, "msg": "register failed"}      注册失败
                                   {"code": 101, "msg": "registered already"}   账号已存在
 **/

/**
 *  登录协议
 **/
/*
    //status: 在线状态 0离线 1在线 2忙碌 3离开 4隐身
    //clienttype: 客户端类型,pc=1, android=2, ios=3
    send : cmd = 1002, seq = 0, {"username": "13917043329", "password": "123", "clienttype": 1, "status": 1}
    recv : cmd = 1002, seq = 0+1, {"code": 0, "msg": "ok", "userid": 8, "username": "13917043320", "nickname": "zhangyl",
                          "facetype": 0, "customface":"文件md5", "gender":0, "birthday":19891208, "signature":"哈哈，终于成功了",
                          "address":"上海市东方路3261号", "phonenumber":"021-389456", "mail":"balloonwj@qq.com"}
 **/

/**
 * 获取用户列表
 **/
/*
    cmd = 1003, seq = 0
    cmd = 1003, seq = 0,
    {"code": 0, "msg": "ok", "userinfo":[{"userid": 2,"username":"qqq",
                                          "nickname":"qqq123", "facetype": 0,
                                          "customface":"466649b507cdf7443c4e88ba44611f0c",
                                          "gender":1, "birthday":19900101, "signature":"生活需要很多的力气呀。xx",
                                          "address": "", "phonenumber": "", "mail":"", "clienttype": 1,
                                          "status":1},
                                          {"userid": 3,"username":"hujing", "nickname":"hujingx",
                                          "facetype": 0, "customface":"", "gender":0, "birthday":19900101,
                                          "signature":"", "address": "", "phonenumber": "", "mail":"",
                                          "clienttype": 1, "status":0}]}
**/

/**
 * 查找用户
 **/
/*
    //type查找类型 0所有， 1查找用户 2查找群
    cmd = 1004, seq = 0, {"type": 1, "username": "zhangyl"}
    cmd = 1004, seq = 0, { "code": 0, "msg": "ok", "userinfo": [{"userid": 2, "username": "qqq", "nickname": "qqq123", "facetype":0}] }
**/

/**
 *  操作好友，包括加好友、删除好友
 **/
/*
    //type为1发出加好友申请 2 收到加好友请求(仅客户端使用) 3应答加好友 4删除好友请求 5应答删除好友 6服务器应答已经是好友关系
    //当type=3时，accept是必须字段，0对方拒绝，1对方接受
    cmd = 1005, seq = 0, {"userid": 9, "type": 1}
    cmd = 1005, seq = 0, {"userid": 9, "type": 6, "username": "xxx"}
    cmd = 1005, seq = 0, {"userid": 9, "type": 2, "username": "xxx"}
    cmd = 1005, seq = 0, {"userid": 9, "type": 3, "username": "xxx", "accept": 1(1同意，0拒绝)}

    //发送
    cmd = 1005, seq = 0, {"userid": 9, "type": 4}
    //应答
    cmd = 1005, seq = 0, {"userid": 9, "type": 5, "username": "xxx"}
 **/

/**
 *  用户状态改变
 **/
/*
    //type 1上线(将来会扩展成在线、隐身、忙碌、离开等状态) 2离线 3用户签名、头像、昵称发生变化
    cmd = 1006, seq = 0, {"type": 1, "onlinestatus": 1} //上线onlinestatus=1, 离线onlinestatus=0
    cmd = 1006, seq = 0, {"type": 3}
    **/

/**
 *  更新用户信息
 **/
/*
    cmd = 1007, seq = 0, 用户信息: {"nickname": "xx", "facetype": 0,"customface":"文件md5", "gender":0, "birthday":19891208, "signature":"哈哈，终于成功了",
                                                                "address":"上海市东方路3261号", "phonenumber":"021-389456", "mail":"balloonwj@qq.com"}
    cmd = 1007, seq = 0, {"code": 0, "msg": "ok", "userid": 9, "username": "xxxx", "nickname": "xx", "facetype": 0,
                                                                "customface":"文件md5", "gender":0, "birthday":19891208, "signature":"哈哈，终于成功了",
                                                                "address":"上海市东方路3261号", "phonenumber":"021-389456", "mail":"balloonwj@qq.com"}
**/

/*Flamingo聊天格式（json）
//排版格式
{
    "msgType": 1, //消息类型 0未知类型 1文本 2窗口抖动 3文件
    "time": 2434167,
    "clientType": 0,		//0未知 1pc端 2苹果端 3安卓端
    "font":["fontname", fontSize, fontColor, fontBold, fontItalic, fontUnderline],
    "content":
    [
        {"msgText": "text1"},
        {"msgText": "text2"},
        {"faceID": 101},
        {"faceID": 102},
        {"pic": ["name", "server_path", 400, w, h]},
        {"shake": 1},
        {"file":["name", "server_path", 400, onlineflag]}		//onlineflag为0是离线文件，不为0为在线文件
    ]
}
*/

/**
 *  聊天协议
 **/
/*
    cmd = 1100, seq = 0, data:聊天内容 + targetid(消息接受者)
    cmd = 1100, seq = 0, data:聊天内容 senderid(消息发送者), targetid(消息接受者)

    data(发送文件)：data= {"msgType":1,"time":1544694988,"clientType":1,"content":[{"file":["日志系统的设计.zip","a9c16205582437436c740832da9ce2b9",324217,0]}]}
 **/

/**
 *  修改密码
 **/
/*
    cmd = 1008, seq = 0, {"userid": 9,"username": "xxx","oldpassword": "xxx", "newpassword": "yyy"}
    cmd = 1008, seq = 0, {"code":0, "msg": "ok"}    成功
    cmd = 1008, seq = 0, {"code":103, "msg": "incorrect register phonenumber"}  注册手机号不一致
    cmd = 1008, seq = 0, {"code":104, "msg": "incorrect old password"}  旧密码不一致
    cmd = 1008, seq = 0, {"code":105, "msg": "modify password error"}   服务器原因更新失败，提示客户端网络原因更新失败
**/

#endif//MSG_H
