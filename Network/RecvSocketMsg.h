﻿#ifndef RECVSOCKETMSG_H
#define RECVSOCKETMSG_H

#include <QObject>
#include <QTcpSocket>

class RecvSocketMsg : public QObject
{
    Q_OBJECT
public:
    explicit RecvSocketMsg(QTcpSocket* socket, QObject *parent = 0);
private:
    QTcpSocket      *m_pTcpSocket;
    QByteArray      m_buffer;     //缓存上一次或多次的未处理的数据,重新粘包
    int             m_seq;        //当前数据包序列号
private:
    void initConection();
    bool Process(const char* inbuf, size_t length);
    void OnRegisterResult(const char* data,int length);
signals:
    void signalStatus(const quint8 &state);
public slots:
    // tcp消息处理
    void SltReadyRead();
};

#endif // RECVSOCKETMSG_H
