﻿#ifndef DATABASEMAGR_H
#define DATABASEMAGR_H
/*第一次插入，后面更新sql语句：
 *     query2.exec("create table student (id INTEGER PRIMARY KEY AUTOINCREMENT, "
                "uid INTEGER NOT NULL UNIQUE,"设置约束：该值唯一
                "name varchar(20))");
    query2.exec("REPLACE INTO student(uid,name)"
                "VALUES(11,'张三');");如果是第一次插入，则直接插入，否则更新
 *
 *
 *
 *
*/
#include <QObject>
#include <QMutex>
#include <QMutexLocker>
#include <QSqlDatabase>
#include <QSqlQuery>

class DataBaseMagr : public QObject
{
    Q_OBJECT
public:
    explicit DataBaseMagr(QObject *parent = 0);
    virtual ~DataBaseMagr();

    bool OpenBuddyInfoDb(const QString &dataName);
    bool OpenMessageDb(const QString &dataName);
    void AddHistoryMsg(const int &uid, int sendflag,const int& utctime,const QString& font,const QString& content);
    QSqlQuery LoadHistoryMsg(int uid,int offset = 0,int count = -1);
    // 单实例运行
    static DataBaseMagr *Instance()
    {
        /*
        static QMutex mutex;
        if (NULL == self) {
            QMutexLocker locker(&mutex);

            if (!self) {
                self = new DataBaseMagr();
            }
        }
        */
        //启动时初始化，暂不需要加锁
        if (NULL == self) {
                self = new DataBaseMagr();
        }
        return self;
    }
signals:

public slots:
private:
    static DataBaseMagr *self;
    // 数据库管理
    QSqlDatabase m_buddyInfodb;
    QSqlDatabase m_msgdb;
};

#endif // DATABASEMAGR_H
