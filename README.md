# 逐梦通讯客户端代码

#### 项目介绍
这是一款即时通讯软件，客户端Based on Qt 5.9.7 (MSVC 2015, 32 bit)编写，服务器代码见https://gitee.com/DreamFly0/DreamgoServer
截止2019/02/12号实现的功能如下：
1.登录模块：
用户登录、注册功能，本地记录用户登录状态
2.主界面模块：
好友信息获取、好友的添加、删除、查询、上下线提示
3.聊天窗口：
支持单人聊天发送消息，暂不支持群聊，可以发送表情、文字、文件
4.部分效果以及客户端业务逻辑见 即时通讯软件.md：https://gitee.com/DreamFly0/Dreamgo/blob/master/%E5%8D%B3%E6%97%B6%E9%80%9A%E8%AE%AF%E8%BD%AF%E4%BB%B6.md

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)