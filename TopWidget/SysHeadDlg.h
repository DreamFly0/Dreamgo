﻿#ifndef SYSHEADDLG_H
#define SYSHEADDLG_H

#include <QWidget>
#include "CustomWidget.h"
#include <QPushButton>

class SysHeadDlg : public CBaseDialog
{
    Q_OBJECT
public:
    explicit SysHeadDlg(QWidget *parent = 0);
    virtual ~SysHeadDlg();
private:
    void initUi();

    QPushButton* Btn[6][6];
signals:
    void sigHeadIndex(int);
public slots:
};

#endif // SYSHEADDLG_H
