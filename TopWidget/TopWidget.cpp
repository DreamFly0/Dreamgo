﻿#include "TopWidget.h"
#include "CustomWidget.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QMargins>
#include <QEvent>
#include <QDebug>
#include <QMouseEvent>
#include <QStackedLayout>
#include <QFont>
#include <QFontMetrics>
#include <QVector>
#include <QPainter>
#include <QPaintEvent>

TopWidget::TopWidget(QWidget *parent)
    : QWidget(parent),
      m_index(0),
      m_skinPic(":/Images/resource/skin/M4A1.jpg")
{
    this->setFixedHeight(154);
    //自动填充背景，这句必须加上，否则导致MiddleWidget和BottomWidget快速伸缩界面时导致背景不及时填充的问题
    this->setAutoFillBackground(true);
    //个人资料对话框
    m_pUserInfoDlg = new UserInfoDialog;
    m_pHeadDlg = new SysHeadDlg;

    m_pTitleBar = new titleBar(this);
    m_pTitleBar->setMinimumSize(360,24);
    m_pTitleBar->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);

    //用户信息
    QWidget *widgetInfo = new QWidget(this);

    QVector<QString> list;
    list.push_back(":/Images/resource/Padding4Normal.png");
    list.push_back(":/Images/resource/Padding4Hot.png");
    //m_pHeadBtn = new HeadButton(":/Images/resource/head_bkg.png", 2, this);
    m_pHeadBtn = new HeadButton(list,this);
    m_pHeadBtn->setMouseTracking(true);
    m_pHeadBtn->setFixedSize(64,64);
    m_pHeadBtn->setCursor(Qt::PointingHandCursor);
    m_pUserInfoDlg->m_labelHead.setPixmap(m_pHeadBtn->getBackGround());

    QPalette pa;
    pa.setColor(QPalette::WindowText,Qt::white);
    m_labelNickName.setPalette(pa);
    m_labelNickName.setText(tr("NickName"));
    m_labelNickName.setMinimumSize(80,25);
    m_labelNickName.setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);

    m_pStackSign = new QStackedWidget(this);
    m_pStackSign->setFixedHeight(25);
    //m_pStackSign->resize(250,25);
    m_pStackSign->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
    //m_lineEditSign.setMinimumSize(230,25);
    //m_lineEditSign.setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
    //m_lineEditSign.setStyleSheet("QLineEdit{border-radius:1px;background-color:transparent;color:white}");
    m_lineEditSign.setStyleSheet("QLineEdit{ background:rgba(0,0,0,0%); border:1px;}"
                        "QLineEdit:hover{ border-image:url(:/Images/resource/btn_background.png); }"
                        "QLineEdit:!hover{background:rgba(0,0,0,0%);}"
                        "QLineEdit:focus {background:white;border-image:none; border:1px groove lightgray; border-radius:2px}");
    m_lineEditSign.setPlaceholderText(tr("Signature"));
    m_lineEditSign.installEventFilter(this);
    m_lineEditSign.setAlignment(Qt::AlignLeft);
    m_btnSign = new QPushButton(this);
    m_btnSign->setObjectName("SignBtn");

    m_pStackSign->addWidget(m_btnSign);
    m_pStackSign->addWidget(&m_lineEditSign);
    //m_pStackSign->setMinimumWidth(250);
    QVBoxLayout *vLayout=new QVBoxLayout;
    vLayout->addWidget(&m_labelNickName);
    //vLayout->addSpacing(5);
    vLayout->addWidget(m_pStackSign);
    vLayout->setSpacing(0);
    //vLayout->addStretch(1);
    vLayout->setContentsMargins(5,0,10,0);

    QHBoxLayout *hLayout = new QHBoxLayout;
    hLayout->addSpacing(20);
    hLayout->addWidget(m_pHeadBtn);
    hLayout->addSpacing(10);
    hLayout->addLayout(vLayout);
    //hLayout->setContentsMargins(5,0,5,0);
    hLayout->setMargin(0);

    widgetInfo->setLayout(hLayout);
    widgetInfo->resize(360,90);
    widgetInfo->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
    //搜索框
    m_lineEditSearch.setMinimumSize(360,30);
    m_lineEditSearch.setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
    //m_lineEditSearch.setStyleSheet("QLineEdit{border-radius:13px;background:rgb(255,255,255,180);}");
    m_lineEditSearch.setStyleSheet("QLineEdit{border-radius:1px;background:rgb(255,255,255,50);color:white;}");
    m_lineEditSearch.setContextMenuPolicy(Qt::NoContextMenu);
    m_pSearchBtn = new myPushButton(&m_lineEditSearch);
    m_pSearchBtn->setFixedSize(16,16);
    m_pSearchBtn->setStyleSheet("QPushButton{background:transparent;border-image: url(:/Images/resource/images/btn_search (1).png);}"
                          "QPushButton::hover{border-image: url(:/Images/resource/images/btn_search (2).png);}"
                          "QPushButton::pressed{border-image: url(:/Images/resource/images/btn_search (3).png);}");
    m_lineEditSearch.setPlaceholderText(tr("Search"));
    QMargins margins = m_lineEditSearch.textMargins();
    m_lineEditSearch.setTextMargins(margins.left()+15, margins.top(), m_pSearchBtn->width()+15, margins.bottom());
    m_lineEditSearch.installEventFilter(this);

    QHBoxLayout *pSearchLayout = new QHBoxLayout;
    pSearchLayout->addStretch();
    pSearchLayout->addWidget(m_pSearchBtn);
    pSearchLayout->setSpacing(0);
    pSearchLayout->setContentsMargins(0,0,15,0);
    m_lineEditSearch.setLayout(pSearchLayout);

    this->setMouseTracking(true);
    widgetInfo->setMouseTracking(true);
    m_pTitleBar->setMouseTracking(true);

    //这三个要加上，否则mainwindow无边框拉动的时候，
    //不能追踪到鼠标事件，慢慢从边框移动到这些widget就能发现鼠标的形状还是拉伸的形状
    QVBoxLayout *vMainLayout=new QVBoxLayout;
    vMainLayout->addWidget(m_pTitleBar);
    vMainLayout->addWidget(widgetInfo);

    vMainLayout->addWidget(&m_lineEditSearch);
    vMainLayout->addStretch(1);
    vMainLayout->setContentsMargins(0,0,0,0);
    //vMainLayout->setMargin(0);
    setLayout(vMainLayout);

    connect(m_pHeadBtn,&HeadButton::clicked,this,&TopWidget::showUserDlg);
    connect(m_pUserInfoDlg,&UserInfoDialog::sigHeadIndex,this,&TopWidget::setHeadPixmapIndex);
    connect(&m_lineEditSign,&QLineEdit::returnPressed,this,&TopWidget::SltEditSign);
    connect(m_btnSign,&QPushButton::clicked,this,&TopWidget::SltClickedSignBtn);
    connect(&m_pUserInfoDlg->m_btnSysHead,&QPushButton::clicked,this,&TopWidget::SltHeadDlgShow);
    connect(m_pHeadDlg,&SysHeadDlg::sigHeadIndex,m_pUserInfoDlg,&UserInfoDialog::setHeadPixmapIndex);
}

TopWidget::~TopWidget()
{
    m_pUserInfoDlg->close();
    delete m_pUserInfoDlg;
    delete m_pHeadDlg;
}

void TopWidget::setHeadPixmapIndex(int index)
{
    m_index = index;
    m_pHeadBtn->setBackGround(QPixmap(QString(":/Images/resource/head/%1.png").arg(index)));
}

void TopWidget::SltClickedSignBtn()
{
    m_pStackSign->setCurrentIndex(1);
    m_lineEditSign.setFocus();
}

//必须通过这个函数来修改签名
void TopWidget::SltBtnSignText(const QString &text)
{
    QFont ft = m_btnSign->font();
    QFontMetrics fm(ft);
    QString text2 = fm.elidedText(text, Qt::ElideRight, m_btnSign->width());
    m_btnSign->setText(text2);
}

void TopWidget::SltHeadDlgShow()
{
    m_pHeadDlg->show();
}

void TopWidget::showUserDlg()
{
    m_pUserInfoDlg->showNormal();
    m_pUserInfoDlg->activateWindow();
}

//主界面签名修改也是先更新UserInfoDlg的签名，然后单击ok按钮，发送服务器更新资料
void TopWidget::SltEditSign()
{
    qDebug() << "TopWidget::SltEditSign m_btnSign:" << m_btnSign->width();
    qDebug() << "TopWidget::SltEditSign m_lineEditSign:" <<  m_lineEditSign.width();

    m_pStackSign->setCurrentIndex(0);
    SltBtnSignText(m_lineEditSign.text());

    m_pUserInfoDlg->m_editSign.setPlainText(m_lineEditSign.text());
    m_pUserInfoDlg->m_btnOK.click();
}

bool TopWidget::eventFilter(QObject *obj, QEvent *evt)
{

    if(obj==&m_lineEditSearch)//lineedit的
    {
        if(evt->type()==QEvent::FocusOut)
        {
            m_lineEditSearch.setStyleSheet("QLineEdit{border-radius:1px;background:rgb(255,255,255,70);color:white;}");
            //m_lineEditSearch.setFrame(false);
        }else if(evt->type()==QEvent::FocusIn)
        {
            m_lineEditSearch.setStyleSheet("QLineEdit{border: 1px solid rgb(214,214,214);border-radius:1px;background:rgb(255,255,255,255);color:black;}");
            //m_lineEditSearch.setFrame(true);
        }

    }else if(obj==&m_lineEditSign)//lineedit的
    {

        if(evt->type()==QEvent::FocusOut)
        {
            //background-color:
            //m_lineEditSign.setStyleSheet("QLineEdit{border-radius:1px;background:transparent;color:white}");
            Q_EMIT m_lineEditSign.returnPressed();
            //m_lineEditSign.setMaxLength(10);
        }else if(evt->type()==QEvent::FocusIn)
        {
            //m_lineEditSign.setMaxLength(100);
            //m_lineEditSign.setStyleSheet("QLineEdit{border-radius:1px;background:rgb(255,255,255,255);}");
            //m_lineEditSign.setCursorPosition(0);
        }

    }

    return QWidget::eventFilter(obj,evt);
}

void TopWidget::mousePressEvent(QMouseEvent *evt)
{
    m_lineEditSign.clearFocus();
    evt->ignore();
}

void TopWidget::resizeEvent(QResizeEvent *evt)
{
    QWidget::resizeEvent(evt);
    QFontMetrics fm(m_btnSign->font());
    QString text2 = fm.elidedText(m_lineEditSign.text(), Qt::ElideRight, m_btnSign->width());
    m_btnSign->setText(text2);
}

void TopWidget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    float d =(float)m_skinPic.height()/m_skinPic.width();
    int h=d*width();
    int w=height()/d;
    if(h<height())//如果图片高度小于窗口高度
    {
         painter.drawPixmap(0,0,w,height(),m_skinPic);
         return;
    }
    painter.drawPixmap(0,0,width(),h,m_skinPic);

}
