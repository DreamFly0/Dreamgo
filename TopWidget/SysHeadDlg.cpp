﻿#include "SysHeadDlg.h"
#include "LoginTitleBar.h"
#include <QPixmap>
#include <QString>
#include <QDebug>

SysHeadDlg::SysHeadDlg(QWidget *parent) : CBaseDialog(parent)
{
    initUi();
}

SysHeadDlg::~SysHeadDlg()
{
    qDebug() << "~SysHeadDlg()";
    for(int i = 0;i<6;i++)
    {
        for(int j = 0;j<6;j++)
        {
            delete Btn[i][j];
        }
    }
}

void SysHeadDlg::initUi()
{
    this->setFixedSize(500,520);
    this->setAttribute(Qt::WA_DeleteOnClose,false); //由于该对话框就是顶层对话框，不需要设置这个属性自动销毁
    //设置窗口背景
    this->setAutoFillBackground(true); // 这句要加上, 否则可能显示不出背景图.
    QPalette palette = this->palette();
    palette.setBrush(QPalette::Window,QBrush(QColor("#f2f1f1")));             // 使用平滑的缩放方式
    setPalette(palette);

    //设置标题背景
    m_pTitleBar->setAutoFillBackground(true); // 这句要加上, 否则可能显示不出背景图.
    QPalette palette1 = m_pTitleBar->palette();
    palette1.setBrush(QPalette::Window,
                      QBrush(QPixmap(":/Images/resource/titleBg.png").scaled(// 缩放背景图.
                      this->size(),
                      Qt::IgnoreAspectRatio,
                      Qt::SmoothTransformation)));             // 使用平滑的缩放方式

    m_pTitleBar->setPalette(palette1);
    m_pTitleBar->setWinTitle("选择头像");

    //初始化头像
    for(int i = 0;i<6;i++)
    {
        for(int j = 0;j<6;j++)
        {
            Btn[i][j] = new QPushButton(this);
            Btn[i][j]->setObjectName("SysHead");
            Btn[i][j]->setIcon(QPixmap(QString(":/Images/resource/head/%1.png").arg(6*i + j)));
            Btn[i][j]->move(i*80 + 20,j*80 + 40);
            Btn[i][j]->setFixedSize(64,64);
            Btn[i][j]->setIconSize(QSize(56,56));
            connect(Btn[i][j],&QPushButton::clicked,[=](){
                Q_EMIT sigHeadIndex(6*i + j);
                this->close();
            });
        }
    }
}

