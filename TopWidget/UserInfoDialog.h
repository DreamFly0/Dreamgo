﻿#ifndef USERINFODIALOG_H
#define USERINFODIALOG_H

#include <QWidget>
#include "CustomWidget.h"
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QRadioButton>
#include <QButtonGroup>
#include <QTextEdit>
#include <QToolButton>
#include <QCalendarWidget>
#include <QDate>

class UserInfoDialog : public CBaseDialog
{
    Q_OBJECT
public:
    explicit UserInfoDialog(QWidget *parent = 0);
    virtual ~UserInfoDialog();
    void setDate(QString str);
    QString getDate();

    int getHeadPixmapIndex(){ return m_index; }
    int             m_index;
    QLabel          m_labelName;
    QLabel          m_labelSex;
    QLabel          m_labelAccount0;
    QLabel          m_labelBirthday;
    QLabel          m_labelSign;
    QLabel          m_labelSite;
    QLabel          m_labelTel;
    QLabel          m_labelEmail;

    QPushButton     m_btnOK;
    QPushButton     m_btnCancel;

    QLineEdit       m_editName;
    QLabel          m_labelAccount1;
    QRadioButton    m_btnMan;
    QRadioButton    m_btnWoman;
    QButtonGroup    m_btnGroup;
    QTextEdit       m_editSign;

    QLineEdit       m_editSite;
    QLineEdit       m_editTel;
    QLineEdit       m_editEmail;

    QLabel          m_labelHead;
    QLineEdit       m_editBirthday;
    QToolButton     m_btnDate;
    QCalendarWidget m_calWidget;
    QPushButton     m_btnSysHead;
    QPushButton     m_btnCustomHead;
signals:
    void sigHeadIndex(int);
public slots:
    void SltShowCal();
    void SltCalClicked(const QDate &date);
    void setHeadPixmapIndex(int index);
};

#endif // USERINFODIALOG_H
