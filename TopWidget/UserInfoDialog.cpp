﻿#include "UserInfoDialog.h"
#include "LoginTitleBar.h"
#include <QHBoxLayout>
#include <QDebug>
#pragma execution_character_set("utf-8")
UserInfoDialog::UserInfoDialog(QWidget *parent)
    : CBaseDialog(parent),
      m_labelName("昵  称:",this),
      m_labelSex("性  别:",this),
      m_labelAccount0("账  号:",this),
      m_labelAccount1("1",this),
      m_labelBirthday("生  日:",this),
      m_labelSign("签  名:",this),
      m_labelSite("地  址:",this),
      m_labelTel("电  话:",this),
      m_labelEmail("邮  箱:",this),
      m_btnOK("确定",this),
      m_btnCancel("取消",this),
      m_editName(this),
      m_btnMan("男",this),
      m_btnWoman("女",this),
      m_btnGroup(this),
      m_editSign(this),
      m_editSite(this),
      m_editTel(this),
      m_editEmail(this),
      m_labelHead(this),
      m_editBirthday(this),
      m_btnDate(this),
      m_calWidget(this),
      m_btnSysHead(this),
      m_btnCustomHead(this)
{
    this->setFixedSize(660,480);
    this->setAttribute(Qt::WA_DeleteOnClose,false); //由于该对话框不需要parent，需要设置这个属性自动销毁

    m_pTitleBar->setWinTitle("我的资料");
    m_pTitleBar->getMinimizeBtn()->hide();
    m_pTitleBar->getTitlelabe()->hide();
    //
    m_labelName.move(30,40);
    m_labelAccount0.move(30,80);
    m_labelSex.move(30,120);
    m_labelBirthday.move(30,160);
    m_labelSign.move(30,200);
    m_labelSite.move(30,290);
    m_labelTel.move(30,330);
    m_labelEmail.move(30,370);

    m_btnOK.move(220,420);
    m_btnCancel.move(360,420);

    m_editName.move(100,40);
    m_editName.resize(200,30);
    m_labelAccount1.move(100,80);
    m_labelAccount1.setFixedSize(200,30);

    m_btnMan.move(100,120);
    m_btnWoman.move(180,120);
    m_btnGroup.addButton(&m_btnMan,0);
    m_btnGroup.addButton(&m_btnWoman,1);
    //m_btnMan.setChecked(true);

    m_editSign.resize(500,70);
    m_editSign.move(100,210);
    m_editSign.setStyleSheet("font: 18px;");

    m_editSite.resize(500,30);
    m_editTel.resize(500,30);
    m_editEmail.resize(500,30);

    m_editSite.move(100,290);
    m_editTel.move(100,330);
    m_editEmail.move(100,370);

    m_labelHead.resize(64,64);
    m_labelHead.move(405,70);
    //m_labelHead.setPixmap(QPixmap(":/Images/resource/head/0.png"));

    m_editBirthday.setReadOnly(true);
    m_editBirthday.move(100,160);
    m_editBirthday.resize(200,30);
    m_btnSysHead.move(320,160);
    m_btnCustomHead.move(450,160);
    m_btnSysHead.setText("系统头像");
    m_btnCustomHead.setText("自定义头像");
    m_btnSysHead.setStyleSheet("font: 18px;");
    m_btnCustomHead.setStyleSheet("font: 18px;");
    //生日选项框
    m_calWidget.resize(m_calWidget.sizeHint().width(),m_calWidget.sizeHint().height());
    m_calWidget.move(270,190);
    m_calWidget.hide();
    m_calWidget.setVerticalHeaderFormat(QCalendarWidget::NoVerticalHeader);
    m_calWidget.setGridVisible(true);
    m_btnDate.resize(30,30);
    m_btnDate.setCursor(Qt::ArrowCursor);
    m_btnDate.setIcon(QPixmap(":Images/resource/images/ic_calendar.png"));
    QHBoxLayout *hlayout = new QHBoxLayout();
    hlayout->setSpacing(0);
    hlayout->addStretch();
    hlayout->setContentsMargins(0,0,0,0);
    hlayout->addWidget(&m_btnDate);
    m_editBirthday.setLayout(hlayout);

    connect(&m_btnDate,&QToolButton::clicked,this,&UserInfoDialog::SltShowCal);
    connect(&m_calWidget,&QCalendarWidget::clicked,this,&UserInfoDialog::SltCalClicked);
}

UserInfoDialog::~UserInfoDialog()
{
    qDebug() << "~UserInfoDialog()";
}

void UserInfoDialog::setDate(QString str)
{
    str.insert(4,'/');
    str.insert(7,'/');
    m_editBirthday.setText(str);
}

QString UserInfoDialog::getDate()
{
    QString str = m_editBirthday.text();
    str.remove('/');
    return str;
}

void UserInfoDialog::setHeadPixmapIndex(int index)
{
    m_index = index;
    m_labelHead.setPixmap(QPixmap(QString(":/Images/resource/head/%1.png").arg(m_index)));
    Q_EMIT sigHeadIndex(m_index);
}

void UserInfoDialog::SltShowCal()
{
    QString str = m_editBirthday.text();
    if(!str.isEmpty())
    {
        QString date = m_editBirthday.text();
        int y = date.left(4).toInt();
        int m = date.mid(4,2).toInt();
        int d = date.right(2).toInt();
        m_calWidget.setSelectedDate(QDate(y,m,d));
    }
    m_calWidget.show();
}

void UserInfoDialog::SltCalClicked(const QDate& date)
{
    m_calWidget.hide();
    m_editBirthday.setText(date.toString("yyyy/MM/dd"));
    //QString str = ui->pushButton->text();
}

