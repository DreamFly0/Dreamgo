﻿#ifndef TOPWIDGET_H
#define TOPWIDGET_H

#include <QWidget>
#include <QLineEdit>
#include <QLabel>
#include <QPixmap>

//class titleBar;
#include "titleBar.h"
#include "BasedWidget.h"
#include "UserInfoDialog.h"
#include "HeadButton.h"
#include <QStackedWidget>
#include "SysHeadDlg.h"

class myPushButton;

class TopWidget : public QWidget
{
    Q_OBJECT
protected:
    virtual bool eventFilter(QObject *obj, QEvent *evt);
    void mousePressEvent(QMouseEvent *);
    void resizeEvent(QResizeEvent *evt);
    void paintEvent(QPaintEvent *);
public:
    explicit TopWidget(QWidget *parent = 0);
    virtual  ~TopWidget();
    int getHeadPixmapIndex(){return m_index;}
public:
    int             m_index;
    titleBar        *m_pTitleBar;
    HeadButton     *m_pHeadBtn;
    myPushButton    *m_pSearchBtn;
    QLineEdit       m_lineEditSign;
    QLineEdit       m_lineEditSearch;
    QLabel          m_labelNickName;
    UserInfoDialog  *m_pUserInfoDlg;
    SysHeadDlg      *m_pHeadDlg;
    QStackedWidget  *m_pStackSign;
    QPixmap         m_skinPic;
signals:
private:
    QPushButton     *m_btnSign;
public slots:
    void showUserDlg();
    void SltEditSign();
    void setHeadPixmapIndex(int index);
    void SltClickedSignBtn();
    void SltBtnSignText(const QString& text);
    void SltHeadDlgShow();
};

#endif // TOPWIDGET_H
