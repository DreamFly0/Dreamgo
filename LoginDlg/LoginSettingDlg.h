﻿#ifndef LOGINSETTINGDLG_H
#define LOGINSETTINGDLG_H

#include <QWidget>
#include <QLineEdit>
#include <QPushButton>
#include <QByteArray>
#include <QSettings>
#include <QApplication>

class LoginTitleBar;
#define MAX_SRV_ADDR	16
#define MAX_SRV_PORT	8
class LoginSettingDlg : public QWidget
{
    Q_OBJECT
protected:
    void mousePressEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
public:
    explicit LoginSettingDlg(QWidget *parent = 0);
    ~LoginSettingDlg();
private:
    void initUi(void);
    void initConnect(void);
private:
    LoginTitleBar   *m_pTitleBar;

    bool            isMousePress;
    QPoint          windowPos;
    QPoint          mousePos;
    QLineEdit		*m_pEditSrvAddr;
    QLineEdit		*m_pEditSrvPort;

    QPushButton		*m_pBtnOK;
    QPushButton		*m_pBtnCancel;

    QString		m_szSrvAddr;
    QString		m_szSrvPort;
    QSettings       m_SetInfo;
signals:
    void signalConfEdit();
public slots:
    void onBtnOK();
    void onBtnCancel();
};

#endif // LOGINSETTINGDLG_H
