﻿#include "LoginRegDlg.h"
#include "LoginTitleBar.h"
#include <QMouseEvent>
#include <QRegExp>
#include <QValidator>

LoginRegDlg::LoginRegDlg(QWidget *parent) : QWidget(parent)
{
    initUi();
    initConnect();
}

void LoginRegDlg::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton){
         windowPos = this->pos();
         mousePos = event->globalPos();
         isMousePress = true;
    }
}

void LoginRegDlg::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
        isMousePress = false;
}

void LoginRegDlg::mouseMoveEvent(QMouseEvent *event)
{
    if (isMousePress == true){
        move(windowPos + (event->globalPos() - mousePos));
    }
}

void LoginRegDlg::initUi(void)
{
    setWindowFlags(Qt::FramelessWindowHint);
    m_pTitleBar = new LoginTitleBar("注册账号",this);
    m_pTitleBar->move(0,0);
    m_pTitleBar->resize(550,24);
    setFixedSize(550,350);

    //设置背景图片
    this->setAutoFillBackground(true); // 这句要加上, 否则可能显示不出背景图.
    QPalette palette = this->palette();
    palette.setBrush(QPalette::Window,
                QBrush(QPixmap(":/Images/resource/LoginSettingDlgBg.png").scaled(// 缩放背景图.
                this->size(),
                Qt::IgnoreAspectRatio,
                Qt::SmoothTransformation)));             // 使用平滑的缩放方式
    setPalette(palette);
    //编辑框初始化
    QLabel *labelPhone = new QLabel(tr("注册新手机号:"),this);
    QLabel *labelNickName = new QLabel(tr("注册昵称:"),this);
    QLabel *labelPwd1 = new QLabel(tr("账号密码:"),this);
    QLabel *labelPwd2 = new QLabel(tr("确认密码:"),this);
    labelPhone->setFixedHeight(30);
    labelNickName->setFixedHeight(30);
    labelPwd1->setFixedHeight(30);
    labelPwd2->setFixedHeight(30);

    m_pEditPhone = new QLineEdit(this);
    m_pEditNickName = new QLineEdit(this);
    m_pEditRegPwd1 = new QLineEdit(this);
    m_pEditRegPwd2 = new QLineEdit(this);
    //m_pEditRegPwd1->setPlaceholderText(QStringLiteral("密码由字母、数字、下划线组成，长度8-16位")); //QStringLieral是Qt5中新引入的一个用来从“字符串常量”创建QString对象的宏
    //m_pEditRegPwd2->setPlaceholderText(QStringLiteral("密码由字母、数字、下划线组成，长度8-16位")); //QStringLieral是Qt5中新引入的一个用来从“字符串常量”创建QString对象的宏
    m_pEditRegPwd1->setEchoMode(QLineEdit::Password);
    m_pEditRegPwd2->setEchoMode(QLineEdit::Password);

    m_pOkBtn = new QPushButton(tr("注册"),this);
    m_pCancelBtn = new QPushButton(tr("取消"),this);

    m_pOkBtn->move(130,300);
    m_pCancelBtn->move(300,300);
    m_pEditPhone->setGeometry(185,70,300,30);
    labelPhone->move(50,70);
    m_pEditNickName->setGeometry(185,125,300,30);
    labelNickName->move(50,125);
    m_pEditRegPwd1->setGeometry(185,180,300,30);
    labelPwd1->move(50,180);
    m_pEditRegPwd2->setGeometry(185,235,300,30);
    labelPwd2->move(50,235);
    //编辑框数据过滤
    QRegExp regxPort("\\d+");
    QValidator *validatorPort = new QRegExpValidator(regxPort, m_pEditPhone);
    m_pEditPhone->setValidator( validatorPort );
}

void LoginRegDlg::initConnect(void)
{
    connect(m_pTitleBar->getCloseBtn(),&QPushButton::clicked,this,&LoginRegDlg::close);
    connect(m_pTitleBar->getMinimizeBtn(), &QPushButton::clicked, this, &LoginRegDlg::showMinimized);
}
