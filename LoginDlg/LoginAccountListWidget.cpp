﻿#include "LoginAccountListWidget.h"
#include "MyApp.h"
#include <QCursor>
#include <QMouseEvent>
#include <QListWidgetItem>

LoginAccountListWidget::LoginAccountListWidget(QWidget *parent)
    : QListWidget(parent)
{
    this->setObjectName("LoginListWgt");
    setMouseTracking(true);
    this->setMaximumHeight(90);
    /*
    setStyleSheet("QListWidget{background:white;border:1px solid rgb(214,214,214);outline:0px;}"
                  "QListWidget::item{color:rgb(102,102,102);height:30;}");
                  */
    setFrameShadow(QFrame::Plain);//设置平的样式
    setFrameShape(QFrame::NoFrame);//设置无边样式
    //this->setFocus();
    setFocusPolicy(Qt::NoFocus);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);//水平scrollbar禁用
    //setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);//垂直scrollbar禁用
    //setSelectionBehavior ( QAbstractItemView::SelectRows); //设置选择行为，以行为单位
    //setSelectionMode ( QAbstractItemView::NoSelection);//单选
}

LoginAccountListWidget::~LoginAccountListWidget()
{
    this->clear();
    int count=this->count();
    for(int i=0;i<count;i++)
    {
       QListWidgetItem *item = this->takeItem(0);
       delete item;
    }
}

void LoginAccountListWidget::mouseMoveEvent(QMouseEvent*e)
{
    QListWidget::mouseMoveEvent(e);
    QListWidgetItem *listitem=this->itemAt(this->mapFromGlobal(QCursor::pos()));

    if(listitem==Q_NULLPTR)
    {
        /*
        for(int row=0;row<this->count();row++)
        {
            this->item(row)->setBackgroundColor(Qt::transparent);
        }
        */
    }
    else
    {
        QListWidgetItem *plistitem=this->item(prerow);
        /*
        if(plistitem!=Q_NULLPTR)
        {
            plistitem->setBackgroundColor(Qt::transparent);
        }
        listitem->setBackgroundColor(QColor(244,244,244));
        */
        prerow=this->row(listitem);
    }

}

void LoginAccountListWidget::leaveEvent(QEvent *e)
{
    QListWidget::leaveEvent(e);

    QListWidgetItem *plistitem=this->item(prerow);
    /*
    if(plistitem!=Q_NULLPTR)
    {
        plistitem->setBackgroundColor(Qt::transparent);
        plistitem->setTextColor(QColor(102,102,102));
    }
    */
}

void LoginAccountListWidget::focusInEvent(QFocusEvent *e)
{
   QListWidget::focusInEvent(e);
}
