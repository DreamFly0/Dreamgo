﻿#ifndef TITLEBAR_H
#define TITLEBAR_H

#include <QWidget>
#include "Button.h"
#include <QHBoxLayout>
#include <QLabel>

class titleBar : public QWidget
{
    Q_OBJECT

public:
    explicit titleBar(QWidget *parent = 0);
    Button *getCloseBtn() const {return m_pCloseBtn;}
    Button *getMinimizeBtn() const {return m_pMinimizeBtn;}
    Button *getMenuBtn() const { return m_pMenuBtn; }
    void InsertUserLayout(QLayout *UserLayout, int pos, int stretch);
    void InsertUserWidget(QWidget *widget, int index, int stretch = 0, Qt::Alignment alignment = 0)
    {
        layout->insertWidget(index, widget, stretch, alignment);
    }
    void setWinTitle(const QString &text);
private:
    //Button *m_pSkinBtn;
    //Button *m_pVideoBtn;
    Button *m_pCloseBtn;
    Button *m_pMinimizeBtn;
    Button *m_pMenuBtn;
    QLabel *m_pIconLabel;
    QLabel *m_pTitleLabel;

    QHBoxLayout* layout;
    void WidgetLayoutInit(void);

};

#endif // TITLEBAR_H
