﻿#ifndef TOPLOGIN_H
#define TOPLOGIN_H

#include <QWidget>
#include <memory>
#include <QJsonObject>
#include <QThread>
#include <QJsonDocument>
#include <QListWidgetItem>
#include "LoginAccountList.h"
#include "BaseWindow.h"
#include <QToolButton>

class QCheckBox;
class titleBar;
class QLineEdit;
class QPushButton;
class QFile;
class LoginSettingDlg;
class LoginRegDlg;
class QLabel;
class ClientSocket;
class LoginAccountListWidget;

class TopLogin : public BaseWindow
{
    Q_OBJECT
protected:
    void mousePressEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
    bool eventFilter(QObject *obj, QEvent *evt);
public:
    TopLogin(QWidget *parent = 0);
    ~TopLogin();
private:
    void initUi(void);
    void initConnect(void);
    void initNetWork(void);
    void loadAccountFile(void);
    void loadUserCfg(void);
private:
    titleBar           *m_pTitle;             //标题栏
    bool               m_bMousePress;
    bool               m_bConnected;
    bool               m_bLogin;
    QPoint             m_windowPos;
    QPoint             m_mousePos;

    QLineEdit          *m_pNameEdit;          //账号
    QPushButton        *m_pAccountListBtn;    //下拉账号按钮
    LoginAccountListWidget *m_pAccountListWidget;
    QPushButton        *m_pRegIdBtn;          //注册按钮
    QFile              *qssFile;

    QLineEdit          *m_pPasswdEdit;        //密码输入框
    QPushButton        *m_pFindPasswdBtn;     //找回密码按钮
    QToolButton        *m_pLoginBtn;          //登录按钮
    QLabel             *m_pPicHead;           //头像

    QString             m_strRegPhoneNum;     //注册手机号
    QString             m_strRegNickName;     //昵称
    QString             m_strRegPassword;     //密码

    QCheckBox           *m_pboxRemberPwd;
    QCheckBox           *m_pboxAutoLogin;
    int                 m_prerow;
    LoginAccountList    m_accountList;        //登录账户信息
    QJsonObject         m_userInfo;           //用户信息
    QWidget             *m_pInsertWidget;
    QLabel              *m_plabelLogin;
    std::unique_ptr<LoginSettingDlg>    m_pLoginSettingDlg;
    std::unique_ptr<LoginRegDlg>        m_pLoginRegDlg;
    ClientSocket*       m_pClientSocket;
    QThread             m_thread;
signals:
    void sigSend(int cmd, const QJsonObject& data);
    void sigConnectToHost();
public slots:
    void SltTcpStatus(const quint8 &status);
    void SltLogin();
    void SltClickedLoginBtn();
    void SltRegister();
    void SltShowRegdlg();
    void SltUserInfo(const QJsonObject& userInfo);
    void SltShowAccountListWidget();
    void SltLoginAccountInfo(QListWidgetItem* item);
    void SltNameEditTextChanged(const QString & text);
};

#endif // TOPLOGIN_H
