﻿#ifndef LOGINACCOUNTLISTWIDGET_H
#define LOGINACCOUNTLISTWIDGET_H

#include <QWidget>
#include <QListWidget>
#include "LoginAccountList.h"

class LoginAccountListWidget : public QListWidget
{
    Q_OBJECT
public:
    explicit LoginAccountListWidget(QWidget *parent = 0);
    virtual ~LoginAccountListWidget();
protected:
    void mouseMoveEvent(QMouseEvent*);
    void leaveEvent(QEvent *);
    void focusInEvent(QFocusEvent *);

private:
    int prerow;
    LoginAccountList m_accountList;
signals:

public slots:
};

#endif // LOGINACCOUNTLISTWIDGET_H
