﻿#include "titlebar.h"
#include <QLabel>
#include <QPixmap>
#include <QDebug>

titleBar::titleBar(QWidget *parent) :
    QWidget(parent)
{
    m_pIconLabel = new QLabel(this);
    m_pIconLabel->setFixedSize(24,24);
    m_pIconLabel->setPixmap(QIcon(":/Images/resource/LoginWindow/Dreamgo.png").pixmap(m_pIconLabel->size()));
    m_pTitleLabel = new QLabel(tr(""),this);
    m_pTitleLabel->setStyleSheet("color:white; font: 18px");

    m_pCloseBtn = new Button(":/Images/resource/sys_button_close.png", 4, this);
    m_pMinimizeBtn =new Button(":/Images/resource/min.png", 4, this);
    m_pMenuBtn = new Button(":/Images/resource/Menu.png", 4, this);
    //m_pSkinBtn = new Button(":/Images/Skin.png", 4, this);
    //m_pVideoBtn = new Button(":/Images/playvideo.png", 4, this);

    WidgetLayoutInit();
}

void titleBar::InsertUserLayout(QLayout *UserLayout, int pos, int stretch)
{
    layout->insertLayout(pos, UserLayout, stretch);
}

void titleBar::WidgetLayoutInit()
{
    QHBoxLayout *hlayout = new QHBoxLayout;
    //hlayout->addSpacing(10);
    hlayout->setSpacing(0);
    hlayout->setMargin(0);
    hlayout->addWidget(m_pIconLabel);
    hlayout->addSpacing(5);
    hlayout->addWidget(m_pTitleLabel);
    hlayout->addSpacing(20);
    hlayout->addStretch(1);
    //hlayout->addWidget(m_pVideoBtn);
    //hlayout->addWidget(m_pSkinBtn);
    hlayout->addWidget(m_pMenuBtn);
    hlayout->addWidget(m_pMinimizeBtn);
    hlayout->addWidget(m_pCloseBtn);

    QVBoxLayout *vlayout = new QVBoxLayout;
    vlayout->addLayout(hlayout);
    vlayout->addStretch(1);
    vlayout->setMargin(0);
    setLayout(vlayout);
}

void titleBar::setWinTitle(const QString &text)
{
    m_pTitleLabel->setText(text);
}
