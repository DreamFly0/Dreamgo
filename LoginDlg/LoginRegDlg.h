﻿#ifndef LOGINREGDLG_H
#define LOGINREGDLG_H

#include <QWidget>
#include <QLineEdit>
#include <QPushButton>
class LoginTitleBar;

class LoginRegDlg : public QWidget
{
    Q_OBJECT
protected:
    void mousePressEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
public:
    explicit LoginRegDlg(QWidget *parent = 0);

private:
    void initUi(void);
    void initConnect(void);
public:
    LoginTitleBar* m_pTitleBar;

    bool isMousePress;
    QPoint windowPos;
    QPoint mousePos;

    QLineEdit       *m_pEditPhone;
    QLineEdit       *m_pEditNickName;
    QLineEdit       *m_pEditRegPwd1;
    QLineEdit       *m_pEditRegPwd2;
    QPushButton     *m_pOkBtn;
    QPushButton     *m_pCancelBtn;
signals:

public slots:
};

#endif // LOGINREGDLG_H
