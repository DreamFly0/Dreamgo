﻿#include "LoginTitleBar.h"
#include <QLabel>
#include <QPixmap>
#include <QString>
#include <QPainter>
#include <QPaintEvent>

LoginTitleBar::LoginTitleBar(const QString &name, QWidget *parent)
    : QWidget(parent),
      m_skinPic(":/Images/resource/skin/M4A1.jpg")
{
    m_pIconLabel = new QLabel(this);
    m_pIconLabel->setPixmap(QPixmap(":/Images/resource/flamingo.png"));
    m_pTitleLabel = new QLabel(name,this);
    //设置字体颜色
    m_pTitleLabel->setStyleSheet("color:white; font: 18px");

    m_pMinimizeBtn =new Button(":/Images/resource/min.png", 4, this);
    m_pCloseBtn = new Button(":/Images/resource/sys_button_close.png", 4, this);
    //m_pCloseBtn->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    m_pCloseBtn->setFixedSize(35,30);
    m_pMinimizeBtn->setFixedSize(35,30);
    m_pIconLabel->setFixedSize(30,30);

    this->setFixedHeight(30);

    m_pHlayout = new QHBoxLayout;
    m_pHlayout->setSpacing(0);
    //m_pHlayout->setMargin(0);
    m_pHlayout->setContentsMargins(0,0,0,0);
    m_pHlayout->addWidget(m_pIconLabel);
    m_pHlayout->addSpacing(5);
    m_pHlayout->addWidget(m_pTitleLabel);
    m_pHlayout->addSpacing(20);
    m_pHlayout->addStretch(1);
    m_pHlayout->addWidget(m_pMinimizeBtn);
    m_pHlayout->addWidget(m_pCloseBtn);
    m_pHlayout->addSpacing(0);

    setLayout(m_pHlayout);
}

void LoginTitleBar::setWinTitle(const QString& name)
{
    m_pTitleLabel->setText(name);
}

void LoginTitleBar::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    float d =(float)m_skinPic.height()/m_skinPic.width();
    int h=d*width();
    int w=height()/d;
    if(h<height())//如果图片高度小于窗口高度
    {
         painter.drawPixmap(0,0,w,height(),m_skinPic);
         return;
    }
    painter.drawPixmap(0,0,width(),h,m_skinPic);

}
