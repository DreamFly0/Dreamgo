﻿#ifndef CLOSTBTN_H
#define CLOSTBTN_H

/*按钮类的设计思路：按钮图片一般都以4种状态提供，分为x方向一组，y方向一组
 * 将4张图片拼成1张png，所以每次只需取出图片中的1/4就行，
 *按钮样式包括鼠标进入、退出、点击不同状态，每个事件通过paintevent函数重绘样式
 *sizeHint()返回按钮在布局当中的大小 */
#include <QWidget>
#include <QPushButton>
#include <QString>
#include <QWidget>


class Button : public QPushButton
{
    Q_OBJECT
    Q_PROPERTY(int curIndex READ getcurIndex WRITE setcurIndex)

public:
    //Button( QString FileName, int num  = 1, QWidget *parent = 0);
    Button( QString FileName, int xnum  = 1,  QWidget *parent = 0, int ynum = 1, QString bkGrnd = "");
    Button(QVector<QString> &list, QWidget *parent = 0, QString bkGrnd = "");
    QList<QPixmap> *getPixmapList(void){return &pixmatpList;}
    void setcurIndex(int index){curIndex = index; update();}
    int getcurIndex(void){return curIndex;}
    ~Button();
private:
    void setPixmapList(QVector<QString> &list);
    QPixmap *background;

public slots:


protected:
    QList<QPixmap> pixmatpList;
    int curIndex;
protected:
    virtual void paintEvent ( QPaintEvent * event);
    virtual void enterEvent(QEvent *event);
    virtual void leaveEvent ( QEvent * event );
    virtual void mousePressEvent ( QMouseEvent * event ) ;
    virtual void mouseReleaseEvent ( QMouseEvent * event );
    virtual  QSize sizeHint () const;

signals:

public slots:

};

#endif // CLOSTBTN_H
