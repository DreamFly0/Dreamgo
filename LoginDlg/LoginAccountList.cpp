﻿#include "LoginAccountList.h"
#include <QDebug>

LoginAccountList::LoginAccountList()
{
    m_nLastLoginUser = 0;
}

LoginAccountList::~LoginAccountList()
{
    Clear();
}

/**
 * @brief LoginAccountList::LoadFile    载入配置文件
 * @param filename
 * @return
 * @date:                       2018/9/30
 */
bool LoginAccountList::LoadFile(const QString &filename)
{
    if(filename.isEmpty()) return false;
    QFile file(filename);
    if(!file.open(QIODevice::ReadOnly))
        return false;

    //读取缓存用户数
    int nCount = 0;
    _fread(&nCount,sizeof(nCount),1,file);
    //获取最近登录用户
    _fread(&m_nLastLoginUser, sizeof(m_nLastLoginUser), 1, file);
    //加载所有历史用户
    for(int i=0;i<nCount;i++)
    {
        LOGIN_ACCOUNT_INFO* lpAccount = new LOGIN_ACCOUNT_INFO;
        if(lpAccount != NULL)
        {
            _fread(lpAccount,sizeof(LOGIN_ACCOUNT_INFO),1,file);
            m_arrLoginAccount.push_back(lpAccount);
            /*
            qDebug() << QString(m_arrLoginAccount[i]->szUser);
            qDebug() << QString(m_arrLoginAccount[i]->szPwd);
            qDebug() << m_arrLoginAccount[i]->nStatus;
            qDebug() << m_arrLoginAccount[i]->bRememberPwd;
            qDebug() << m_arrLoginAccount[i]->bAutoLogin;
            */
        }
    }

    file.close();
    return true;
}

/**
 * @brief LoginAccountList::SaveFile    保存登录信息到文件
 * @param filename
 * @return
 * @date:                       2018/9/30
 */
bool LoginAccountList::SaveFile(const QString &filename)
{
    if(filename.isEmpty()) return false;

    int nCount = m_arrLoginAccount.size();
    if (nCount <= 0)
        return false;

    QFile file(filename);
    if(!file.open(QIODevice::WriteOnly))
        return false;
    //写入用户数
    _fwrite(&nCount, sizeof(nCount), 1, file);
    //写入最近登录用户信息
    _fwrite(&m_nLastLoginUser, sizeof(m_nLastLoginUser), 1, file);
    //写入所有登录用户信息
    int nCount2 = 0;
    for(int i = 0;i<nCount;i++)
    {
        LOGIN_ACCOUNT_INFO* lpAccount = m_arrLoginAccount[i];
        if(lpAccount != NULL)
        {
            _fwrite(lpAccount,sizeof(LOGIN_ACCOUNT_INFO),1,file);
            nCount2++;
        }
    }

    if(nCount2 != nCount)
    {
        file.seek(0);
        _fwrite(&nCount2,sizeof(nCount2),1,file);
    }

    file.close();
    return true;
}

/**
 * @brief LoginAccountList::Add 添加新账号
 * @param szUser                账号，
 * @param szPwd                 密码，
 * @param nStatus               登录状态
 * @param bRememberPwd          记住密码
 * @param bAutoLogin            自动登录
 * @return                      成功返回true
 * @date:                       2018/9/30
 */
bool LoginAccountList::Add(QChar *szUser, QChar *szPwd, int nStatus, bool bRememberPwd, bool bAutoLogin)
{
    if(NULL == szUser) return false;

    LOGIN_ACCOUNT_INFO* lpAccount = Find(szUser);
    if(lpAccount != NULL)
        return false;

    lpAccount = new LOGIN_ACCOUNT_INFO;
    if(NULL == lpAccount)
        return false;

    int nCount = sizeof(lpAccount->szUser)/sizeof(QChar);
    memcpy(lpAccount->szUser,szUser,nCount);
    lpAccount->szUser[nCount-1] = QChar('\0');

    if(bRememberPwd && szPwd != NULL)
    {
        nCount = sizeof(lpAccount->szPwd)/sizeof(QChar);
        memcpy(lpAccount->szPwd,szPwd,nCount);
        lpAccount->szPwd[nCount-1] = QChar('\0');
    }

    lpAccount->nStatus = nStatus;
    lpAccount->bRememberPwd = bRememberPwd;
    lpAccount->bAutoLogin = bAutoLogin;
    m_arrLoginAccount.push_back(lpAccount);
    return true;
}

/**
 * @brief LoginAccountList::Del
 * @param index
 * @return
 * @date:                       2018/9/30
 */
bool LoginAccountList::Del(int index)
{
    if(index < 0 || index >= m_arrLoginAccount.size()) return false;

    LOGIN_ACCOUNT_INFO *lpAccount = m_arrLoginAccount[index];
    if(lpAccount != NULL)
        delete lpAccount;
    m_arrLoginAccount.erase(m_arrLoginAccount.begin() + index);
    return true;
}

/**
 * @brief LoginAccountList::Modify  保存登录账户信息
 * @param nIndex
 * @param szUser
 * @param szPwd
 * @param nStatus
 * @param bRememberPwd
 * @param bAutoLogin
 * @return
 * @date:                       2018/9/30
 */
bool LoginAccountList::Modify(QChar *szUser, QChar *szPwd, int nStatus, bool bRememberPwd, bool bAutoLogin)
{
    LOGIN_ACCOUNT_INFO* lpAccount = Find(szUser);
    if(NULL == lpAccount)
        Add(szUser,szPwd,nStatus,bRememberPwd,bAutoLogin);
    else
    {
        int nCount = sizeof(lpAccount->szUser)/sizeof(QChar);
        memcpy(lpAccount->szUser,szUser,nCount);
        lpAccount->szUser[nCount-1] = QChar('\0');

        if(bRememberPwd && szPwd != NULL)
        {
            nCount = sizeof(lpAccount->szPwd)/sizeof(QChar);
            memcpy(lpAccount->szPwd,szPwd,nCount);
            lpAccount->szPwd[nCount-1] = QChar('\0');
        }

        lpAccount->nStatus = nStatus;
        lpAccount->bRememberPwd = bRememberPwd;
        lpAccount->bAutoLogin = bAutoLogin;
    }

    return true;
}

/**
 * @brief LoginAccountList::Clear
 * @date:                       2018/9/30
 */
void LoginAccountList::Clear()
{
    for(int i = 0; i < m_arrLoginAccount.size();i++)
    {
        LOGIN_ACCOUNT_INFO* lpAccount = m_arrLoginAccount[i];
        if(lpAccount != NULL)
            delete lpAccount;
    }
    m_arrLoginAccount.clear();
}

/**
 * @brief LoginAccountList::GetCount
 * @return
 * @date:                       2018/9/30
 */
int LoginAccountList::GetCount()
{
    return m_arrLoginAccount.size();
}

LOGIN_ACCOUNT_INFO *LoginAccountList::GetAccountInfo(int nIndex)
{
    if(nIndex < 0 || nIndex >= m_arrLoginAccount.size()) return NULL;

    return m_arrLoginAccount[nIndex];
}

/**
 * @brief LoginAccountList::Find    根据用户名查找历史用户
 * @param szUser
 * @return
 * @date:                       2018/9/30
 */
LOGIN_ACCOUNT_INFO *LoginAccountList::Find(const QChar *szUser)
{
    if(NULL == szUser) return NULL;

    for(int i = 0; i<m_arrLoginAccount.size();i++)
    {
        LOGIN_ACCOUNT_INFO* lpAccount = m_arrLoginAccount[i];

        if(NULL != lpAccount && QString(szUser).compare(QString(lpAccount->szUser)) == 0)
            return lpAccount;
    }
    return NULL;
}

int LoginAccountList::GetLastLoginUser()
{
    return m_nLastLoginUser;
}

void LoginAccountList::SetLastLoginUser(QChar *lpszUser)
{
    if(NULL == lpszUser)
        return;

    for(int i = 0; i < m_arrLoginAccount.size(); i++)
    {
        LOGIN_ACCOUNT_INFO* lpAccount = m_arrLoginAccount[i];
        if(lpAccount != NULL && 0 == QString(lpszUser).compare(QString(lpAccount->szUser)) )
        {
            m_nLastLoginUser = i;
            return;
        }
    }
}

LOGIN_ACCOUNT_INFO *LoginAccountList::GetLastLoginAccountInfo()
{
    return GetAccountInfo(m_nLastLoginUser);
}

bool LoginAccountList::GetLastLoginAccountInfo(LOGIN_ACCOUNT_INFO *lpAccount)
{
    if(NULL == lpAccount)
        return false;

    LOGIN_ACCOUNT_INFO* lpAccount2 = GetAccountInfo(m_nLastLoginUser);
    if(NULL == lpAccount2)
        return false;

    memcpy(lpAccount, lpAccount2,sizeof(LOGIN_ACCOUNT_INFO));
    return true;
}

bool LoginAccountList::IsAutoLogin()
{
    LOGIN_ACCOUNT_INFO* lpAccount = GetAccountInfo(m_nLastLoginUser);
    return (lpAccount != NULL) ? lpAccount->bAutoLogin : false;
}

//加密
void LoginAccountList::Encrypt(char *lpBuf, int nLen)
{
    for (int i = 0; i < nLen; i++)
    {
        lpBuf[i] = lpBuf[i] ^ 0x88;
    }
}

//解密
void LoginAccountList::Decrypt(char *lpBuf, int nLen)
{
    for (int i = 0; i < nLen; i++)
    {
        lpBuf[i] = lpBuf[i] ^ 0x88;
    }
}

/**
 * @brief LoginAccountList::_fread  读取nSize*nCount个字节数据到lpBuf
 * @param lpBuf
 * @param nSize
 * @param nCount
 * @param file
 * @return
 * @date:                       2018/9/30
 */
int LoginAccountList::_fread(void *lpBuf, int nSize, int nCount, QFile &file)
{
    int nRet = file.read((char*)lpBuf, nSize*nCount);
    Decrypt((char*)lpBuf, nSize*nCount);
    return nRet;
}

int LoginAccountList::_fwrite(const void *lpBuf, int nSize, int nCount, QFile &file)
{
    int nLen = nSize*nCount;
    if(nLen > sizeof(LOGIN_ACCOUNT_INFO)) return 0;
    char cBuf[sizeof(LOGIN_ACCOUNT_INFO)];
    memcpy(cBuf, lpBuf, nLen);
    Encrypt(cBuf,nLen);
    return file.write(cBuf,nLen);
}

