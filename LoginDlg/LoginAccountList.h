﻿#ifndef LOGINACCOUNTLIST_H
#define LOGINACCOUNTLIST_H

#include <QString>
#include <QChar>
#include <QFile>
#include <QVector>
//#include "LoginAccountListWidget.h"

struct LOGIN_ACCOUNT_INFO	// 登录账号信息
{
    QChar szUser[32];		// 用户名
    QChar szPwd[32];		// 密码
    int nStatus;			// 登录状态
    bool bRememberPwd;		// 记住密码
    bool bAutoLogin;		// 自动登录
    //BOOL bAlreadyLogin;		// 是否已经登录
};

class LoginAccountList
{
public:
    friend class TopLogin;
    friend class LoginWindow;
    LoginAccountList();
    ~LoginAccountList();

    bool LoadFile(const QString& filename);                 // 加载登录帐号列表文件
    bool SaveFile(const QString& filename);                 // 保存登录帐号列表文件

    bool Add(QChar* szUser,QChar* szPwd,                    // 添加帐号
             int nStatus,bool bRememberPwd,bool bAutoLogin);
    bool Del(int index);                                    // 删除帐号
    bool Modify(QChar* szUser, QChar* szPwd,    // 修改帐号
        int nStatus, bool bRememberPwd, bool bAutoLogin);
    void Clear();											// 清除所有帐号
    int GetCount();											// 获取帐号总数
    LOGIN_ACCOUNT_INFO* GetAccountInfo(int nIndex);         // 获取帐号信息
    LOGIN_ACCOUNT_INFO* Find(const QChar* szUser);             // 查找帐号

    int GetLastLoginUser();                                 //获取最后登录账号索引
    void SetLastLoginUser(QChar* lpszUser);

    LOGIN_ACCOUNT_INFO* GetLastLoginAccountInfo();
    bool GetLastLoginAccountInfo(LOGIN_ACCOUNT_INFO* lpAccount);

    bool IsAutoLogin();

private:
    void Encrypt(char* lpBuf, int nLen);					// 加密
    void Decrypt(char* lpBuf, int nLen);					// 解密
    int _fread(void* lpBuf, int nSize, int nCount, QFile& file);
    int _fwrite(const void* lpBuf, int nSize, int nCount,QFile& file);

private:
    int m_nLastLoginUser;									// 最后登录用户索引
    QVector<LOGIN_ACCOUNT_INFO*> m_arrLoginAccount;         // 登录账号列表
};

#endif // LOGINACCOUNTLIST_H
