﻿#include "LoginSettingDlg.h"
#include "LoginTitleBar.h"
#include "CustomWidget.h"
#include "MyApp.h"

#include <QMouseEvent>
#include <QValidator>
#include <QDebug>
#include <QDialog>
#include <QMessageBox>
LoginSettingDlg::LoginSettingDlg(QWidget *parent) :
    QWidget(parent),
    m_SetInfo(QSettings::IniFormat, QSettings::UserScope, QCoreApplication::organizationName(), QCoreApplication::applicationName())
{
    initUi();
    initConnect();
}

LoginSettingDlg::~LoginSettingDlg()
{
    //qDebug() << "LoginSettingDlg::~LoginSettingDlg";
}

void LoginSettingDlg::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton){
         windowPos = this->pos();
         mousePos = event->globalPos();
         isMousePress = true;
    }
}

void LoginSettingDlg::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
        isMousePress = false;
}

void LoginSettingDlg::mouseMoveEvent(QMouseEvent *event)
{
    if (isMousePress == true){
        move(windowPos + (event->globalPos() - mousePos));
    }
}

void LoginSettingDlg::initUi(void)
{
    setWindowFlags(Qt::FramelessWindowHint);
    m_pTitleBar = new LoginTitleBar("网络设置",this);
    m_pTitleBar->move(0,0);
    m_pTitleBar->resize(550,24);
    setFixedSize(550,350);

    //设置背景图片
    this->setAutoFillBackground(true); // 这句要加上, 否则可能显示不出背景图.
    QPalette palette = this->palette();
    palette.setBrush(QPalette::Window,
                QBrush(QPixmap(":/Images/resource/LoginSettingDlgBg.png").scaled(// 缩放背景图.
                this->size(),
                Qt::IgnoreAspectRatio,
                Qt::SmoothTransformation)));             // 使用平滑的缩放方式
    setPalette(palette);
    //
    m_pEditSrvAddr = new QLineEdit(this);
    m_pEditSrvPort = new QLineEdit(this);
    m_pBtnOK = new QPushButton("确定",this);
    m_pBtnCancel = new QPushButton("取消",this);
    QLabel *LabelSrvAddr = new QLabel("聊天服务器地址:",this);
    QLabel *LabelSrvPort = new QLabel("聊天服务器端口:",this);
    m_pEditSrvAddr->setGeometry(30,80,180,30);
    m_pEditSrvPort->setGeometry(240,80,180,30);
    LabelSrvAddr->move(30,40);
    LabelSrvPort->move(240,40);
    m_pBtnOK->move(130,300);
    m_pBtnCancel->move(300,300);
    //设置输入数据格式
    QRegExp regxSrv("\\d{1,3}\\W\\d{1,3}\\W\\d{1,3}\\W\\d{1,3}");
    QValidator *validatorSrv = new QRegExpValidator(regxSrv, m_pEditSrvAddr);
    m_pEditSrvAddr->setValidator( validatorSrv );
    QRegExp regxPort("\\d+");
    QValidator *validatorPort = new QRegExpValidator(regxPort, m_pEditSrvPort);
    m_pEditSrvPort->setValidator( validatorPort );
    m_pEditSrvAddr->setPlaceholderText("2.2.2.2");
    //载入网络配置
    m_szSrvAddr = MyApp::m_strHostAddr;
    m_szSrvPort = QString("%1").arg(MyApp::m_nMsgPort);
    m_pEditSrvAddr->setText(m_szSrvAddr);
    m_pEditSrvPort->setText(m_szSrvPort);
}

void LoginSettingDlg::initConnect(void)
{
    connect(m_pTitleBar->getCloseBtn(),&QPushButton::clicked,this,&LoginSettingDlg::close);
    connect(m_pTitleBar->getMinimizeBtn(), &QPushButton::clicked, this, &LoginSettingDlg::showMinimized);
    connect(m_pBtnOK,&QPushButton::clicked,this,&LoginSettingDlg::onBtnOK);
    connect(m_pBtnCancel,&QPushButton::clicked,this,&LoginSettingDlg::onBtnCancel);
}

void LoginSettingDlg::onBtnOK()
{
    m_szSrvAddr = m_pEditSrvAddr->text();
    m_szSrvPort = m_pEditSrvPort->text();
    if(m_szSrvAddr.isEmpty())
    {
        CMessageBox::Infomation(tr("地址不能为空"));
        return;
    }
    if(m_szSrvPort.isEmpty())
    {
        CMessageBox::Infomation(tr("端口号不能为空"));
        return;
    }
    MyApp::m_strHostAddr = m_szSrvAddr;
    MyApp::m_nMsgPort = m_szSrvPort.toInt();
    //将配置写入文件
    MyApp::SaveConfig();
    //配置更改，发送信号给登录界面，请求重连
    Q_EMIT signalConfEdit();

    close();
}

void LoginSettingDlg::onBtnCancel()
{
    m_pEditSrvAddr->setText(m_szSrvAddr);
    m_pEditSrvPort->setText(m_szSrvPort);
    close();
}
