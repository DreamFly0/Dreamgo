﻿#ifndef LOGINTITLEBAR_H
#define LOGINTITLEBAR_H

#include <QWidget>
#include <QHBoxLayout>
#include <QLabel>

#include "Button.h"

class LoginTitleBar : public QWidget
{
    Q_OBJECT

public:
    LoginTitleBar(const QString& name = "", QWidget *parent = 0);
    Button *getCloseBtn() const {return m_pCloseBtn;}
    Button *getMinimizeBtn()const { return m_pMinimizeBtn; }
    QLabel *getTitlelabe()const{ return m_pIconLabel; }
    void setWinTitle(const QString& name);
protected:
    void paintEvent(QPaintEvent *);
private:
    Button *m_pCloseBtn;
    Button *m_pMinimizeBtn;

    QLabel *m_pIconLabel;
    QLabel *m_pTitleLabel;

    QHBoxLayout* m_pHlayout;
    QPixmap         m_skinPic;
signals:

public slots:
};

#endif // LOGINTITLEBAR_H
