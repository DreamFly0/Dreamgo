HEADERS += \
    $$PWD/Button.h \
    $$PWD/LoginRegDlg.h \
    $$PWD/LoginSettingDlg.h \
    $$PWD/LoginTitleBar.h \
    $$PWD/titleBar.h \
    $$PWD/TopLogin.h \
    $$PWD/LoginAccountList.h \
    $$PWD/LoginAccountListWidget.h

SOURCES += \
    $$PWD/Button.cpp \
    $$PWD/LoginRegDlg.cpp \
    $$PWD/LoginSettingDlg.cpp \
    $$PWD/LoginTitleBar.cpp \
    $$PWD/titleBar.cpp \
    $$PWD/TopLogin.cpp \
    $$PWD/LoginAccountList.cpp \
    $$PWD/LoginAccountListWidget.cpp
