HEADERS += \
    $$PWD/capture/tools/applauncher.h \
    $$PWD/capture/tools/arrowtool.h \
    $$PWD/capture/tools/blurtool.h \
    $$PWD/capture/tools/capturetool.h \
    $$PWD/capture/tools/circletool.h \
    $$PWD/capture/tools/copytool.h \
    $$PWD/capture/tools/exittool.h \
    $$PWD/capture/tools/markertool.h \
    $$PWD/capture/tools/movetool.h \
    $$PWD/capture/tools/penciltool.h \
    $$PWD/capture/tools/rectangletool.h \
    $$PWD/capture/tools/savetool.h \
    $$PWD/capture/tools/selectiontool.h \
    $$PWD/capture/tools/sizeindicatortool.h \
    $$PWD/capture/tools/toolfactory.h \
    $$PWD/capture/tools/undotool.h \
    $$PWD/capture/widget/amplifierwidget.h \
    $$PWD/capture/widget/buttonhandler.h \
    $$PWD/capture/widget/capturebutton.h \
    $$PWD/capture/widget/capturewidget.h \
    $$PWD/capture/widget/qcolorpicker.h \
    $$PWD/capture/workers/imgur/imagelabel.h \
    $$PWD/capture/workers/imgur/loadspinner.h \
    $$PWD/capture/workers/imgur/notificationwidget.h \
    $$PWD/capture/capturemodification.h \
    $$PWD/capture/screenshot.h \
    $$PWD/utils/confighandler.h \
    $$PWD/utils/desktopfileparse.h \
    $$PWD/utils/desktopinfo.h \
    $$PWD/utils/screengrabber.h \
    $$PWD/ui_amplifierwidget.h \
    $$PWD/ui_qcolorpicker.h

SOURCES += \
    $$PWD/capture/tools/applauncher.cpp \
    $$PWD/capture/tools/arrowtool.cpp \
    $$PWD/capture/tools/blurtool.cpp \
    $$PWD/capture/tools/capturetool.cpp \
    $$PWD/capture/tools/circletool.cpp \
    $$PWD/capture/tools/copytool.cpp \
    $$PWD/capture/tools/exittool.cpp \
    $$PWD/capture/tools/markertool.cpp \
    $$PWD/capture/tools/movetool.cpp \
    $$PWD/capture/tools/penciltool.cpp \
    $$PWD/capture/tools/rectangletool.cpp \
    $$PWD/capture/tools/savetool.cpp \
    $$PWD/capture/tools/selectiontool.cpp \
    $$PWD/capture/tools/sizeindicatortool.cpp \
    $$PWD/capture/tools/toolfactory.cpp \
    $$PWD/capture/tools/undotool.cpp \
    $$PWD/capture/widget/amplifierwidget.cpp \
    $$PWD/capture/widget/buttonhandler.cpp \
    $$PWD/capture/widget/capturebutton.cpp \
    $$PWD/capture/widget/capturewidget.cpp \
    $$PWD/capture/widget/qcolorpicker.cpp \
    $$PWD/capture/workers/imgur/imagelabel.cpp \
    $$PWD/capture/workers/imgur/loadspinner.cpp \
    $$PWD/capture/workers/imgur/notificationwidget.cpp \
    $$PWD/capture/capturemodification.cpp \
    $$PWD/capture/screenshot.cpp \
    $$PWD/utils/confighandler.cpp \
    $$PWD/utils/desktopfileparse.cpp \
    $$PWD/utils/desktopinfo.cpp \
    $$PWD/utils/screengrabber.cpp

RESOURCES += \
    $$PWD/img.qrc
