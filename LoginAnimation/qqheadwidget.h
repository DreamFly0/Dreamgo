﻿#ifndef QQHEADWIDGET_H
#define QQHEADWIDGET_H
#include <QWidget>
#include <QPainter>
#include <QDebug>

#define HEAD_WIDGET_WIDTH 64				// 头像宽度;

// 自定义头像控件;
class QQHeadWidget : public QWidget
{
public:
    QQHeadWidget(QWidget* parent = NULL)
        : QWidget(parent),
          m_pix(":/Images/resource/head/0.png")
    {
        this->setFixedSize(QSize(HEAD_WIDGET_WIDTH, HEAD_WIDGET_WIDTH));
        this->setAttribute(Qt::WA_TranslucentBackground);
    }
public:
    void setPixmap(const QPixmap& pic)
    {
        //qDebug() << "setPixmap";
        m_pix = pic;
        update();
    }
private:
    void paintEvent(QPaintEvent *event)
    {
        QPainter painter(this);
        QRect headRect = QRect(0, 0, HEAD_WIDGET_WIDTH, HEAD_WIDGET_WIDTH);
        painter.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
        // 绘制圆形头像;
        QPainterPath path;
        path.addEllipse(headRect);
        painter.setClipPath(path);
        painter.drawPixmap(headRect, m_pix);
        // 绘制圆形边框;
        painter.setPen(QPen(QColor(255, 255, 255, 200), 4));
        painter.drawRoundedRect(headRect, headRect.width() / 2, headRect.height() / 2);

        return QWidget::paintEvent(event);
    }
private:
    QPixmap m_pix;
};

#endif // QQHEADWIDGET_H
