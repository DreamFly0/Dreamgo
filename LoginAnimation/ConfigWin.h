﻿#ifndef CONFIGDLG_H
#define CONFIGDLG_H

#include "BackWgt.h"
#include <QSettings>

namespace Ui {
class ConfigWin;
}

class ConfigWin : public QWidget
{
    Q_OBJECT
    friend class LoginAnimationWin;
public:
    explicit ConfigWin(QWidget *parent = nullptr);
    ~ConfigWin();
private:
    Ui::ConfigWin *ui;
    QString         m_strAddr;
    QString         m_strPort;
    QString         m_strFileServerAddr;
    QString         m_strFileServerPort;
    QSettings       m_SetInfo;
signals:
    void signalConfEdit();
    void sigConfigCancel();
    void sigMinBtnClicked();
    void sigClosedBtnClicked();
public slots:
    void onBtnOK();
    void onBtnCancel();
};

#endif // CONFIGDLG_H
