
FORMS += \
    $$PWD/RegisterWin.ui \
    $$PWD/ConfigWin.ui \
    $$PWD/LoginWindow.ui \
    $$PWD/LoginAnimationWin.ui \
    $$PWD/ModifyPwdWin.ui

HEADERS += \
    $$PWD/rotatingstackedwidget.h \
    $$PWD/BackWgt.h \
    $$PWD/RegisterWin.h \
    $$PWD/qqheadwidget.h \
    $$PWD/BackWidget.h \
    $$PWD/ConfigWin.h \
    $$PWD/LoginWindow.h \
    $$PWD/LoginAnimationWin.h \
    $$PWD/ModifyPwdWin.h

SOURCES += \
    $$PWD/rotatingstackedwidget.cpp \
    $$PWD/BackWgt.cpp \
    $$PWD/RegisterWin.cpp \
    $$PWD/BackWidget.cpp \
    $$PWD/ConfigWin.cpp \
    $$PWD/LoginWindow.cpp \
    $$PWD/LoginAnimationWin.cpp \
    $$PWD/ModifyPwdWin.cpp
