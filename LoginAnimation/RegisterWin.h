﻿#ifndef REGISTERWIN_H
#define REGISTERWIN_H

#include "BackWgt.h"

namespace Ui {
class RegisterWin;
}

class RegisterWin : public BackWgt
{
    Q_OBJECT
    friend class LoginAnimationWin;
public:
    explicit RegisterWin(QWidget *parent = nullptr);
    ~RegisterWin();
signals:
    void sigMinBtnClicked();
    void sigClosedBtnClicked();
    void sigRegisterCancel();
    void sigRegisterOk(const QString& phoneNbr,const QString& nickName,const QString& pwd);
private:
    void SltOkBtnClicked();
private:
    Ui::RegisterWin *ui;
};

#endif // REGISTERWIN_H
