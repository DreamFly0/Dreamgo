﻿#include "ModifyPwdWin.h"
#include "ui_ModifyPwdWin.h"
#include "CustomWidget.h"
#include <QValidator>

ModifyPwdWin::ModifyPwdWin(QWidget *parent) :
    BackWgt(parent),
    ui(new Ui::ModifyPwdWin)
{
    ui->setupUi(this);
    //编辑框数据过滤
    QRegExp regxPort("\\d+");
    QValidator *validatorPort = new QRegExpValidator(regxPort, ui->pEditPhone);
    ui->pEditPhone->setValidator( validatorPort );
    connect(ui->pCancelBtn,&QPushButton::clicked,this,&ModifyPwdWin::sigModifyCancel);
    connect(ui->pModifyBtn,&QPushButton::clicked,this,&ModifyPwdWin::SltModifyBtnClicked);
    connect(ui->pButtonMin,&QPushButton::clicked,this,&ModifyPwdWin::sigMinBtnClicked);
    connect(ui->pButtonClose,&QPushButton::clicked,this,&ModifyPwdWin::sigClosedBtnClicked);

}

ModifyPwdWin::~ModifyPwdWin()
{
    delete ui;
}

void ModifyPwdWin::SltModifyBtnClicked()
{
    if(ui->pEditPhone->text().isEmpty())
    {
        CMessageBox::Warning(tr("The phone number cannot be empty"));
        return;
    }
    if(ui->pEditIdNumber->text().isEmpty())
    {
        CMessageBox::Warning(tr("The ID cannot be empty"));
        return;
    }
    if(ui->pEditOldPwd->text().isEmpty())
    {
        CMessageBox::Warning(tr("The old password cannot be empty"));
        return;
    }
    if(ui->pEditNewPwd->text().isEmpty())
    {
        CMessageBox::Warning(tr("The new password cannot be empty"));
        return;
    }

    Q_EMIT sigModifyOk(ui->pEditPhone->text(),
                       ui->pEditIdNumber->text().toInt(),
                       ui->pEditOldPwd->text(),
                       ui->pEditNewPwd->text());
}
