﻿#include "RegisterWin.h"
#include "ui_RegisterWin.h"
#include "CustomWidget.h"

RegisterWin::RegisterWin(QWidget *parent) :
    BackWgt(parent),
    ui(new Ui::RegisterWin)
{
    ui->setupUi(this);
    //编辑框数据过滤
    QRegExp regxPort("\\d+");
    QValidator *validatorPort = new QRegExpValidator(regxPort, ui->pEditPhone);
    ui->pEditPhone->setValidator( validatorPort );
    connect(ui->pCancelBtn,&QPushButton::clicked,this,&RegisterWin::sigRegisterCancel);
    connect(ui->pOkBtn,&QPushButton::clicked,this,&RegisterWin::SltOkBtnClicked);
    connect(ui->pButtonMin,&QPushButton::clicked,this,&RegisterWin::sigMinBtnClicked);
    connect(ui->pButtonClose,&QPushButton::clicked,this,&RegisterWin::sigClosedBtnClicked);

}

RegisterWin::~RegisterWin()
{
    delete ui;
}

void RegisterWin::SltOkBtnClicked()
{
    if(ui->pEditPhone->text().isEmpty())
    {
        CMessageBox::Warning(tr("The phone number cannot be empty"));
        return;
    }
    if(ui->pEditNickname->text().isEmpty())
    {
        CMessageBox::Warning(tr("The account cannot be empty"));
        return;
    }
    if(ui->pEditPasswd->text().isEmpty() || ui->pEditPasswd2->text().isEmpty())
    {
        CMessageBox::Warning(tr("The password cannot be empty"));
        return;
    }
    if(ui->pEditPasswd->text() != ui->pEditPasswd2->text())
    {
        CMessageBox::Warning(tr("The password input is inconsistent"));
        return;
    }

    Q_EMIT sigRegisterOk(ui->pEditPhone->text(),ui->pEditNickname->text(), ui->pEditPasswd->text());
}
