﻿#include "ConfigWin.h"
#include "ui_ConfigWin.h"
#include "MyApp.h"
#include <QLinearGradient>
#include <QPainter>
#include <QValidator>
#include "CustomWidget.h"

ConfigWin::ConfigWin(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ConfigWin),
    m_SetInfo(QSettings::IniFormat, QSettings::UserScope, QCoreApplication::organizationName(), QCoreApplication::applicationName())
{
    ui->setupUi(this);
    //设置输入数据格式
    QRegExp regxSrv("\\d{1,3}\\W\\d{1,3}\\W\\d{1,3}\\W\\d{1,3}");
    QValidator *validatorSrv = new QRegExpValidator(regxSrv, ui->pEditSrvAddr);
    ui->pEditSrvAddr->setValidator( validatorSrv );
    QRegExp regxPort("\\d+");
    QValidator *validatorPort = new QRegExpValidator(regxPort, ui->pEditSrvPort);
    ui->pEditSrvPort->setValidator( validatorPort );
    ui->pEditSrvAddr->setPlaceholderText("2.2.2.2");
    //载入网络配置
    m_strAddr = MyApp::m_strHostAddr;
    m_strPort = QString("%1").arg(MyApp::m_nMsgPort);
    m_strFileServerAddr = MyApp::m_strFileServerAddr;
    m_strFileServerPort = QString("%1").arg(MyApp::m_nFileSeverPort);
    ui->pEditSrvAddr->setText(m_strAddr);
    ui->pEditSrvPort->setText(m_strPort);
    ui->pEditFileSrvAddr->setText(m_strFileServerAddr);
    ui->pEditFileSrvPort->setText(m_strFileServerPort);

    connect(ui->pCancelBtn,&QPushButton::clicked,this,&ConfigWin::onBtnCancel);
    connect(ui->pOkBtn,&QPushButton::clicked,this,&ConfigWin::onBtnOK);
    connect(ui->pButtonMin,&QPushButton::clicked,this,&ConfigWin::sigMinBtnClicked);
    connect(ui->pButtonClose,&QPushButton::clicked,this,&ConfigWin::sigClosedBtnClicked);
}

ConfigWin::~ConfigWin()
{
    delete ui;
}

void ConfigWin::onBtnOK()
{
    m_strAddr = ui->pEditSrvAddr->text();
    m_strPort = ui->pEditSrvPort->text();
    m_strFileServerAddr = ui->pEditFileSrvAddr->text();
    m_strFileServerPort = ui->pEditFileSrvPort->text();

    if(m_strAddr.isEmpty())
    {
        CMessageBox::Infomation(tr("The address cannot be empty"));
        return;
    }
    if(m_strPort.isEmpty())
    {
        CMessageBox::Infomation(tr("The port number cannot be empty"));
        return;
    }
    if(m_strFileServerAddr.isEmpty())
    {
        CMessageBox::Infomation(tr("The FileServer address cannot be empty"));
        return;
    }
    if(m_strFileServerPort.isEmpty())
    {
        CMessageBox::Infomation(tr("The FileServer port number cannot be empty"));
        return;
    }

    MyApp::m_strHostAddr = m_strAddr;
    MyApp::m_nMsgPort = m_strPort.toInt();
    MyApp::m_strFileServerAddr = m_strFileServerAddr;
    MyApp::m_nFileSeverPort = m_strFileServerPort.toInt();
    //将配置写入文件
    MyApp::SaveConfig();
    //配置更改，发送信号给登录界面，请求重连
    Q_EMIT signalConfEdit();
}

void ConfigWin::onBtnCancel()
{
    ui->pEditSrvAddr->setText(MyApp::m_strHostAddr);
    ui->pEditSrvPort->setText(QString("%1").arg(MyApp::m_nMsgPort));
    Q_EMIT sigConfigCancel();
}
