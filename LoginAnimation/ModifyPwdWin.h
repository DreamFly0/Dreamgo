﻿#ifndef MODIFYPWDWIN_H
#define MODIFYPWDWIN_H

#include "BackWgt.h"

namespace Ui {
class ModifyPwdWin;
}

class ModifyPwdWin : public BackWgt
{
    Q_OBJECT

public:
    explicit ModifyPwdWin(QWidget *parent = nullptr);
    ~ModifyPwdWin();
signals:
    void sigMinBtnClicked();
    void sigClosedBtnClicked();
    void sigModifyCancel();
    void sigModifyOk(const QString& phoneNbr,const int& userid,const QString& oldpwd,const QString& newpwd);
private:
    void SltModifyBtnClicked();
private:
    Ui::ModifyPwdWin *ui;
};

#endif // MODIFYPWDWIN_H
