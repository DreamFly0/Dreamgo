﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTimer>

class BackWidget: public QWidget
{
    Q_OBJECT

public:
    explicit BackWidget(QWidget *parent = 0);
    ~BackWidget();
    virtual void paintEvent(QPaintEvent *e);
    void drawTriangle(QPainter *p, QPointF a, QPointF b, QPointF c, QColor color);
    QColor getRandomRGB();
    double  makeEaseInOut2(double percentComplete);

    void setColor();
    void setPoint();
    QPointF getMovePoint(QPointF point, int t, qint64 g, QPointF target);
    QColor getMoveColor(QColor from, QColor to, int t, qint64 gone, double l) ;
    QColor getRandomRGBLight();
    void setBright(double pointX, double pointY,double decaySpeed,int bright);
private:
    QColor  shapeColorUPFrom[3][11];
    QColor  shapeColorDOWNFrom[3][11];
    QColor  shapeColorUPTarget[3][11];
    QColor  shapeColorDOWNTarget[3][11];
    QColor  shapeColorUP[3][11];
    QColor  shapeColorDOWN[3][11];
    int     shapeColorUPTime[3][11];
    int     shapeColorDOWNTime[3][11];
    QPointF points[4][12];
    QPointF pointsTarget[4][12];
    QPointF pointsNow[4][12];
    int     pointsMoveTime[4][12];
    double  shapeBright[3][11];          //三角形亮度数组
    QTimer  *timer;
    double  nowHiLightSpeed[2];
    double  nowHiLight[2];
};

#endif // WIDGET_H
