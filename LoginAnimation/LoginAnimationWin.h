﻿#pragma once
#include <QtWidgets/QWidget>
#include "ui_LoginAnimationWin.h"
#include <QPropertyAnimation>
#include <QParallelAnimationGroup>
#include <QPushButton>
#include <QPainter>
#include <QGraphicsDropShadowEffect>
#include <QJsonObject>
#include "LoginWindow.h"

class ConfigWin;
class RegisterWin;
class ModifyPwdWin;
class MainWindow;

class LoginAnimationWin : public QWidget
{
	Q_OBJECT

public:
    LoginAnimationWin(QWidget *parent = Q_NULLPTR);
    virtual ~LoginAnimationWin();
protected:
    void paintEvent(QPaintEvent *);
private:
    void initAnimation();
	void initConnections();
	void initWidget();
    void initNetWork(void);
    void Login();
    void loadUserCfg();
    void openMainWin();

	void mousePressEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
signals:
    void sigConnectToHost();
    void sigSend(int cmd, const QJsonObject& data);
private slots:
	// 登录;
	void onLoginButtonClicked();
    void onTcpStatus(const quint8 &status);
	// 最小化;
	void onMinisizeButtonClicked();
	// 网络设置;
	void onNetWordSetButtonClicked();
    void onNetConfigOk();
    void onNetConfigCancel();
    //注册账号
    void onRegisterButtonClicked();
    void onRegisterOk(const QString& phoneNbr,const QString& nickName,const QString& pwd);
    void onRegisterCancel();
    //修改密码
    void onModifyPwdButtonClicked();
    void onModifyOk(const QString& phoneNbr,const int& userid,const QString& oldpwd,const QString& newpwd);
    void onModifyCancel();
public slots:
    void onMainWinDestoryed();
private:
    Ui::LoginAnimationWin ui;

    MainWindow              *m_pMainWin;
    LoginWindow             *m_pLoginWindow;
    ConfigWin               *m_pConfigDlg;
    RegisterWin             *m_pRegisterWin;
    ModifyPwdWin            *m_pModifyPwdWin;
    QPropertyAnimation      *m_vLineAnimation;
    QPropertyAnimation      *m_loginWIndowMoveAnimation;
    QGraphicsOpacityEffect  *m_pGraphicsOpacityEffect;
    QPropertyAnimation      *m_pOpacityAnimation;
    QParallelAnimationGroup *m_pGroupAnimation;
    bool                    m_isWindowHide;        // 当前窗口是否隐藏;
    bool                    m_isPressed;           // 窗口拖动;
    QPoint                  m_startMovePos;

    bool                    m_bConnected;           //是否连接服务器

    QString                 m_strRegPhoneNum;       //注册手机号
    QString                 m_strRegNickName;       //昵称
    QString                 m_strRegPassword;       //密码

    bool                    m_bLogin;               //
    ClientSocket*           m_pClientSocket;        //网络
    QThread                 m_thread;
};
