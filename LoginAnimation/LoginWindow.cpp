﻿#include "LoginWindow.h"
#include "MyApp.h"
#include "LoginAccountListWidget.h"

#include <QLabel>
#include <QIcon>
#include <QDebug>
#include <QMouseEvent>
#include <QSettings>

#define VLine_HEIGHT 330		// 动画效果线条高度;
#define WINDOW_WIDTH 500		// 整个窗口宽度;

LoginWindow::LoginWindow(QWidget *parent)
    : QWidget(parent),
      m_pButtonPullDown(nullptr),
      m_pButtonKeyborad(nullptr)
{
    initUi();
    initConnect();
    loadAccountFile();
}

LoginWindow::~LoginWindow()
{
    qDebug() << "LoginWindow::~LoginWindow";
    m_pAccountListWidget->clear();
    int count=m_pAccountListWidget->count();
    for(int i=0;i<count;i++)
    {
       QListWidgetItem *item = m_pAccountListWidget->takeItem(0);
       delete item;
    }
}

bool LoginWindow::eventFilter(QObject *obj, QEvent *evt)
{
    if(obj == m_loginWindow.lineEditAccount)//lineedit的
    {
        if(evt->type() == QEvent::FocusIn)
        {
            m_pAccountListWidget->hide();
        }
        return false;
    }
    else if(obj == m_pAccountListWidget)
    {
        if(evt->type()==QEvent::FocusOut)
        {
            m_pAccountListWidget->hide();
        }
        return false;
    }
    else
    {
        return QWidget::eventFilter(obj,evt);
    }
}

void LoginWindow::initUi()
{
    this->setMouseTracking(true);
    m_loginWindow.setupUi(this);
    // 设置顶部控件的图标;
    m_loginWindow.LogoIcon->setPixmap(QIcon(":/Images/resource/LoginWindow/logo.png").pixmap(m_loginWindow.LogoIcon->size()));

    m_loginWindow.pButtonNetSet->setIcon(QIcon(":/Images/resource/LoginWindow/NetSetButton.png"));
    m_loginWindow.pButtonNetSet->setIconSize(QSize(15, 15));

    m_loginWindow.pButtonMin->setIcon(QIcon(":/Images/resource/LoginWindow/minimizeButton.png"));
    m_loginWindow.pButtonMin->setIconSize(QSize(15, 15));

    m_loginWindow.pButtonClose->setIcon(QIcon(":/Images/resource/LoginWindow/closeButton.png"));
    m_loginWindow.pButtonClose->setIconSize(QSize(13, 13));

    // 设置两个输入框前后的图标及按钮;
    m_labelQQ = new QLabel;
    m_labelQQ->setFixedSize(QSize(20, 20));
    m_labelQQ->setPixmap(QIcon(":/Images/resource/LoginWindow/Dreamgo.png").pixmap(m_labelQQ->size()));

    m_labelLocker = new QLabel;
    m_labelLocker->setFixedSize(QSize(15, 20));
    m_labelLocker->setPixmap(QIcon(":/Images/resource/LoginWindow/Locker_Normal.png").pixmap(m_labelLocker->size()));

    m_pButtonPullDown = new QPushButton;
    m_pButtonPullDown->setCursor(Qt::PointingHandCursor);
    m_pButtonPullDown->setFixedSize(QSize(16, 16));
    m_pButtonPullDown->setStyleSheet("QPushButton{border-image:url(:/Images/resource/LoginWindow/PullDownButton_Normal.png);}\
                                        QPushButton:hover{border-image:url(:/Images/resource/LoginWindow/PullDownButton_Hover.png);}");

    m_pButtonKeyborad = new QPushButton;
    m_pButtonKeyborad->setCursor(Qt::PointingHandCursor);
    m_pButtonKeyborad->setFixedSize(QSize(16, 16));
    m_pButtonKeyborad->setStyleSheet("QPushButton{border-image:url(:/Images/resource/LoginWindow/keyboard.png) 0 80 0 0;}\
                                        QPushButton:hover{border-image:url(:/Images/resource/LoginWindow/keyboard.png) 0 40 0 40;}\
                                        QPushButton:pressed{border-image:url(:/Images/resource/LoginWindow/keyboard.png) 0 0 0 80;}");

    QHBoxLayout* hAccountLayout = new QHBoxLayout(m_loginWindow.lineEditAccount);
    hAccountLayout->addWidget(m_labelQQ);
    hAccountLayout->addStretch();
    hAccountLayout->addWidget(m_pButtonPullDown);
    hAccountLayout->setMargin(0);

    m_loginWindow.lineEditAccount->setTextMargins(20, 0, 20, 0);

    QHBoxLayout* hPswLayout = new QHBoxLayout(m_loginWindow.lineEditPassed);
    hPswLayout->addWidget(m_labelLocker);
    hPswLayout->addStretch();
    hPswLayout->addWidget(m_pButtonKeyborad);
    hPswLayout->setMargin(0);

    m_loginWindow.lineEditPassed->setTextMargins(20, 0, 20, 0);

    QRegExp regxSrv("\\d+");
    QValidator *validatorSrv = new QRegExpValidator(regxSrv, m_loginWindow.lineEditAccount);
    m_loginWindow.lineEditAccount->setValidator( validatorSrv );
    //账号下拉框
    m_pAccountListWidget = new LoginAccountListWidget(m_loginWindow.OperationWidget);
    m_pAccountListWidget->setFixedSize(230,115);
    m_pAccountListWidget->move(95,40);
    m_pAccountListWidget->raise();
    m_pAccountListWidget->hide();
    m_pAccountListWidget->installEventFilter(this);
    m_loginWindow.lineEditAccount->installEventFilter(this);
    m_pAccountListWidget->setStyleSheet("border: 1px solid rgb(214,214,214);border-radius: 5px;");
    //m_pAccountListWidget->setStyleSheet("border: 1px solid gray;border-radius: 3px;");

}

void LoginWindow::initConnect()
{
    connect(m_pButtonPullDown,&QPushButton::clicked,this,&LoginWindow::SltShowAccountListWidget);
    connect(m_pAccountListWidget,&LoginAccountListWidget::itemClicked,this,&LoginWindow::SltLoginAccountInfo);
    connect(m_loginWindow.lineEditAccount,&QLineEdit::textChanged,this,&LoginWindow::SltNameEditTextChanged);
    //
}

void LoginWindow::loadAccountFile()
{
    //载入登录信息文件
    m_accountList.LoadFile(MyApp::m_strUsersFile);
    for(int i=0; i<m_accountList.GetCount(); i++)
    {
        LOGIN_ACCOUNT_INFO* accountInfo = m_accountList.m_arrLoginAccount[i];
        m_pAccountListWidget->addItem(new QListWidgetItem(QString(accountInfo->szUser)));
    }
    m_prerow = m_accountList.GetLastLoginUser();
    LOGIN_ACCOUNT_INFO *lastUser = m_accountList.GetAccountInfo(m_prerow);
    if(lastUser != nullptr)
    {
        m_loginWindow.lineEditAccount->setText(QString(lastUser->szUser));
        SltNameEditTextChanged(QString(lastUser->szUser));
        if(lastUser->bRememberPwd)
        {
            m_loginWindow.checkBoxRememberPsw->setChecked(true);
            m_loginWindow.lineEditPassed->setText(QString(lastUser->szPwd));
        }
        if(lastUser->bAutoLogin)
        {
            m_loginWindow.checkBoxAutoLogin->setChecked(true);
            m_loginWindow.loginButton->click();
            QTimer::singleShot(1000,m_loginWindow.loginButton,&QPushButton::click);
        }
    }
}

void LoginWindow::SltLoginAccountInfo(QListWidgetItem *item)
{
    m_loginWindow.lineEditAccount->setText(item->text());
    m_pAccountListWidget->hide();
}

void LoginWindow::SltNameEditTextChanged(const QString &text)
{
    QString UserCfgFile = MyApp::m_strUsersPath + text + "/" + text + ".cfg";
    QFile file(UserCfgFile);
    if(file.exists())
    {
        QSettings settings(UserCfgFile, QSettings::IniFormat);
        settings.beginGroup("UserInfo");
        int FaceID = settings.value("FaceID",0).toInt();
        QPixmap picHead(QString(":/Images/resource/head/%1.png").arg(FaceID));
        m_loginWindow.pHead->setPixmap(picHead);
    }
    else
    {
        QPixmap picHead(QString(":/Images/resource/head/0.png"));
        m_loginWindow.pHead->setPixmap(picHead);
    }

    LOGIN_ACCOUNT_INFO *User = m_accountList.Find(text.data());
    if(User != nullptr)
    {
        if(User->bRememberPwd)
        {
            m_loginWindow.checkBoxRememberPsw->setChecked(true);
            m_loginWindow.lineEditPassed->setText(QString(User->szPwd));
            //记住密码的情况下，才勾选自动登录框
            if(User->bAutoLogin)
            {
                m_loginWindow.checkBoxAutoLogin->setChecked(true);
            }else
            {
                m_loginWindow.checkBoxAutoLogin->setChecked(false);
            }
        }
    }else
    {
        m_loginWindow.checkBoxRememberPsw->setChecked(false);
        m_loginWindow.lineEditPassed->setText("");
        m_loginWindow.checkBoxAutoLogin->setChecked(false);
    }
}

void LoginWindow::SltShowAccountListWidget()
{
    m_pAccountListWidget->raise();
    m_pAccountListWidget->show();
    m_pAccountListWidget->setFocus();
}
