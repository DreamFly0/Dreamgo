﻿#ifndef QQLOGINWINDOW_H
#define QQLOGINWINDOW_H
#include <QPainter>
#include <QWidget>
#include <QThread>

#include "ui_LoginWindow.h"
#include "LoginAccountList.h"

class LoginAccountListWidget;
class QListWidgetItem;
class ClientSocket;
class LoginWindow : public QWidget
{
    Q_OBJECT
public:
    friend class LoginAnimationWin;
    explicit LoginWindow(QWidget *parent = nullptr);
    ~LoginWindow();
public:
    Ui::LoginWindow m_loginWindow;
protected:
    bool eventFilter(QObject *obj, QEvent *evt);
private:
    void initUi();
    void initConnect();
    void loadAccountFile(void);
private:
    QLabel*                 m_labelQQ;             // 两个输入框前后的图标及按钮控件;
    QLabel*                 m_labelLocker;
    QPushButton*            m_pButtonPullDown;
    QPushButton*            m_pButtonKeyborad;

    LoginAccountList        m_accountList;          //登录账户信息
    LoginAccountListWidget* m_pAccountListWidget;
    int                     m_prerow;

signals:
    void sigAutoLogin();
public slots:
    void SltLoginAccountInfo(QListWidgetItem *item);
    void SltNameEditTextChanged(const QString &text);
    void SltShowAccountListWidget();
};

#endif // QQLOGINWINDOW_H
