﻿#ifndef BOTTOMWIDGET_H
#define BOTTOMWIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QToolButton>
#include <QMenu>

class RequestAdduserDlg;
class FindFriendDlg;
class ClientSocket;

class BottomWidget : public QWidget
{
    Q_OBJECT
public:
    explicit BottomWidget(QWidget *parent = 0);
    virtual ~BottomWidget();
    void setSocket(ClientSocket *pSocket,int userid);
    void paintEvent(QPaintEvent *);
public:
    QMenu               *m_pSysMenu;

    enum BottomMenu{
        SysSetting,
        MsgManager,
        FileAssistant,
        ModifyPwd,
        Help,
        Reconnect,
        Update,
    };
private:
    ClientSocket        *m_pClientSocket;
    int                 m_userid;
    QPushButton         m_btnSysMenu;
    QToolButton         m_btnAddUser;
    QToolButton         m_btnWeather;
    QToolButton         m_btnCalendar;

    FindFriendDlg       *m_pFindFriendDlg;
    QAction*            m_pReconnect;
signals:
    void sigSend(int cmd, const QJsonObject& data);
    void sigBottomMenu(int i);
public slots:
    void SltTcpStatus(const quint8 &status);
    void SltShowFindFriendDlg();
    void SltFindUser();
    void SltFindUserResult(const QJsonObject& jsonArray);
    void SltAddUser(int userid);
    void SltSysSettings();
    void SltMsgManager();
    void SltFileAssistant();
    void SltModifyPwd();
    void SltHelp();
    void SltReconnect();
    void SltUpdate();
};

#endif // BOTTOMWIDGET_H
