#ifndef FINDFRIENDDLG_H
#define FINDFRIENDDLG_H

#include <QWidget>
#include "CustomWidget.h"
#include <QLineEdit>
#include <QPushButton>
#include <QRadioButton>
#include <QButtonGroup>
#include <QLabel>

class FindUserResultDlg;

class FindFriendDlg : public CBaseDialog
{
    Q_OBJECT
public:
    friend class BottomWidget;
    explicit FindFriendDlg(QWidget *parent = 0);
    virtual ~FindFriendDlg();
    void setUserInfo(int findUserid, QString username,QString nickname,int facetype,bool showtips);
private:
    QLineEdit           m_edit;
    QPushButton         m_btnFind;
    QRadioButton        m_btnContact;
    QRadioButton        m_btnCluster;
    QButtonGroup        m_btnGroup;
    QLabel              m_labelTips;

    FindUserResultDlg   *m_pDlg;
    int                 m_findUserid;
signals:
    void sigAddUser(int userid);
public slots:
    void SltAddUser();
};

#endif // FINDFRIENDDLG_H
