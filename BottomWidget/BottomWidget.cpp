﻿#include "BottomWidget.h"
#include "FindFriendDlg.h"
#include "ClientSocket.h"
#include "Msg.h"
#include "unit.h"

#include <QJsonObject>
#include <QHBoxLayout>
#include <QMenu>
#include <QJsonArray>
#include <QPainter>

BottomWidget::BottomWidget(QWidget *parent)
    : QWidget(parent),
      m_btnSysMenu(this),
      m_btnAddUser(this),
      m_btnWeather(this),
      m_btnCalendar(this)
{
    this->setFixedHeight(50);
    //设置背景色
    this->setAutoFillBackground(true); // 这句要加上, 否则可能显示不出背景图.
    QPalette palette = this->palette();
    palette.setColor(QPalette::Background,Qt::white);
    this->setPalette(palette);
    this->setMouseTracking(true);
    // 设置子菜单
    m_pSysMenu = new QMenu(this);
    QAction *Setting = new QAction(QIcon(""),tr("System Settings"));
    QAction *Message = new QAction(QIcon(""),tr("Message manager"));
    QAction *File = new QAction(QIcon(""),tr("File assistant"));
    QAction *Password = new QAction(QIcon(""),tr("Change the password"));
    QAction *Help = new QAction(QIcon(""),tr("help"));
    m_pReconnect = new QAction(QIcon(""),tr("Reconnect server"));
    QAction *Update = new QAction(QIcon(""),tr("Update"));
    m_pReconnect->setEnabled(false);

    m_pSysMenu->addAction(Setting);
    m_pSysMenu->addAction(Message);
    m_pSysMenu->addAction(File);
    m_pSysMenu->addSeparator();
    m_pSysMenu->addAction(Password);
    m_pSysMenu->addAction(Help);
    m_pSysMenu->addAction(m_pReconnect);
    m_pSysMenu->addSeparator();
    m_pSysMenu->addAction(Update);

    connect(Setting,&QAction::triggered,this,&BottomWidget::SltSysSettings);
    connect(Message,&QAction::triggered,this,&BottomWidget::SltMsgManager);
    connect(File,&QAction::triggered,this,&BottomWidget::SltFileAssistant);
    connect(Password,&QAction::triggered,this,&BottomWidget::SltModifyPwd);
    connect(Help,&QAction::triggered,this,&BottomWidget::SltHelp);
    connect(m_pReconnect,&QAction::triggered,this,&BottomWidget::SltReconnect);
    connect(Update,&QAction::triggered,this,&BottomWidget::SltUpdate);
    // 添加菜单
    m_btnSysMenu.setMenu(m_pSysMenu);
    //connect(sysmenu, SIGNAL(triggered(QAction*)), this, SLOT(SltSysmenuCliecked(QAction*)));

    m_btnSysMenu.setToolTip(tr("The system menu"));
    m_btnAddUser.setToolTip(tr("Add buddy"));
    m_btnWeather.setToolTip(tr("Weather"));
    m_btnCalendar.setToolTip(tr("Calendar"));

    m_btnSysMenu.setFixedSize(40,50);
    m_btnAddUser.setFixedSize(40,50);
    m_btnWeather.setFixedSize(40,50);
    m_btnCalendar.setFixedSize(40,50);

    m_btnSysMenu.setObjectName("BottomButton");
    m_btnAddUser.setObjectName("BottomButton");
    m_btnWeather.setObjectName("BottomButton");
    m_btnCalendar.setObjectName("BottomButton");

    m_btnSysMenu.setIcon(QPixmap(":Images/resource/images/ic_sysmen.png"));
    m_btnAddUser.setIcon(QPixmap(":Images/resource/images/ic_user_add.png"));
    m_btnWeather.setIcon(QPixmap(":Images/resource/images/ic_weather.png"));
    m_btnCalendar.setIcon(QPixmap(":Images/resource/images/ic_calendar.png"));

    QHBoxLayout *layout = new QHBoxLayout;
    layout->setSpacing(5);
    layout->setContentsMargins(5,0,5,0);
    layout->addWidget(&m_btnSysMenu);
    layout->addWidget(&m_btnAddUser);
    layout->addStretch();
    layout->addWidget(&m_btnWeather);
    layout->addWidget(&m_btnCalendar);
    this->setLayout(layout);

    m_pFindFriendDlg = new FindFriendDlg;
    connect(&m_btnAddUser,&QToolButton::clicked,this,&BottomWidget::SltShowFindFriendDlg);
    connect(&m_pFindFriendDlg->m_btnFind,&QPushButton::clicked,this,&BottomWidget::SltFindUser);
}

BottomWidget::~BottomWidget()
{
    delete m_pFindFriendDlg;

}

void BottomWidget::setSocket(ClientSocket *pSocket, int userid)
{
    m_pClientSocket = pSocket;
    m_userid = userid;

    connect(this,&BottomWidget::sigSend,m_pClientSocket,&ClientSocket::SltSend);
    connect(m_pClientSocket,&ClientSocket::sigFindUser,this,&BottomWidget::SltFindUserResult);
    connect(m_pClientSocket,&ClientSocket::signalStatus,this,&BottomWidget::SltTcpStatus);
    connect(m_pFindFriendDlg,&FindFriendDlg::sigAddUser,this,&BottomWidget::SltAddUser);
}

void BottomWidget::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    p.setPen(QColor(220,220,220));
    p.drawLine(0,0,width(),0);
}

void BottomWidget::SltTcpStatus(const quint8 &status)
{
    if(status == DisConnectedHost)
    {
        m_pReconnect->setEnabled(true);
    }else if(ConnectedHost == status)
    {
        m_pReconnect->setEnabled(false);
    }
}

void BottomWidget::SltShowFindFriendDlg()
{
    m_pFindFriendDlg->show();
    m_pFindFriendDlg->activateWindow();
}

//根据username（注册手机号，不是id号）查找好友
void BottomWidget::SltFindUser()
{
    if(m_pFindFriendDlg->m_edit.text().isEmpty())
    {
        return;
    }
    m_pFindFriendDlg->m_labelTips.show();
    m_pFindFriendDlg->m_labelTips.setText(tr("Looking for it, please wait..."));

    int type = m_pFindFriendDlg->m_btnGroup.checkedId();
    QString username = m_pFindFriendDlg->m_edit.text();

    QJsonObject json;
    json.insert("type",type);
    json.insert("username",username);

    Q_EMIT  sigSend(msg_type_finduser,json);
}

//查找用户结果，返回的用户信息
void BottomWidget::SltFindUserResult(const QJsonObject &jsonArray)
{
    m_pFindFriendDlg->m_labelTips.hide();

    QJsonArray friendList = jsonArray.value("userinfo").toArray();
    for(int i = 0;i<friendList.size();i++)
    {
        QJsonObject info = friendList[i].toObject();
        int userid = info.value("userid").toInt();
        QString username = info.value("username").toString();
        QString nickname = info.value("nickname").toString();
        int facetype = info.value("facetype").toInt();
        bool showTips = userid == m_userid;
        m_pFindFriendDlg->setUserInfo(userid,username,nickname,facetype,showTips);
    }
}

//添加对方为好友
void BottomWidget::SltAddUser(int userid)
{
    //int type = m_pFindFriendDlg->m_btnGroup.checkedId();
    m_pFindFriendDlg->m_labelTips.show();
    m_pFindFriendDlg->m_labelTips.setText(tr("The request of adding friend has been sent, please wait for confirmation"));

    QJsonObject json;
    json.insert("userid",userid);
    json.insert("type",msg_type_addfriend);

    Q_EMIT  sigSend(msg_type_operatefriend,json);
}

void BottomWidget::SltSysSettings()
{
    Q_EMIT sigBottomMenu(SysSetting);
}

void BottomWidget::SltMsgManager()
{
    Q_EMIT sigBottomMenu(MsgManager);
}

void BottomWidget::SltFileAssistant()
{
    Q_EMIT sigBottomMenu(FileAssistant);
}

void BottomWidget::SltModifyPwd()
{
    Q_EMIT sigBottomMenu(ModifyPwd);
}

void BottomWidget::SltHelp()
{
    Q_EMIT sigBottomMenu(Help);
}

void BottomWidget::SltReconnect()
{
    Q_EMIT sigBottomMenu(Reconnect);
}

void BottomWidget::SltUpdate()
{
    Q_EMIT sigBottomMenu(Update);
}
