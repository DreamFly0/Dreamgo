HEADERS += \
    $$PWD/BottomWidget.h \
    $$PWD/FindFriendDlg.h \
    $$PWD/FindUserResultDlg.h \
    $$PWD/RequestAdduserDlg.h

SOURCES += \
    $$PWD/BottomWidget.cpp \
    $$PWD/FindFriendDlg.cpp \
    $$PWD/FindUserResultDlg.cpp \
    $$PWD/RequestAdduserDlg.cpp
