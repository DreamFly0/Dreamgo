﻿#ifndef REQUESTADDUSERDLG_H
#define REQUESTADDUSERDLG_H

#include <QWidget>
#include "CustomWidget.h"
#include <QLabel>
#include <QPushButton>
#include <QButtonGroup>

class MainWindow;
class RequestAdduserDlg : public CBaseDialog
{
    Q_OBJECT
public:
    friend class MainWindow;
    explicit RequestAdduserDlg(QWidget *parent = 0);
private:
    QLabel          m_labelTips;
    QPushButton     m_btnAccept;
    QPushButton     m_btnReject;
    QButtonGroup    m_btnGroup;
signals:

public slots:
};

#endif // REQUESTADDUSERDLG_H
