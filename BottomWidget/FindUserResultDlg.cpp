﻿#include "FindUserResultDlg.h"
#include "LoginTitleBar.h"

FindUserResultDlg::FindUserResultDlg(QWidget *parent)
    : CBaseDialog(parent),
      m_labeHead(this),
      m_labelAccount(this),
      m_labelNickname(this),
      m_labelTips(this),
      m_btnAddUser(tr("Add buddy"),this),
      m_btnCalcel(tr("Close"),this)
{
    this->setFixedSize(380,300);
    this->setAttribute(Qt::WA_DeleteOnClose,false); //由于该对话框不需要parent，需要设置这个属性自动销毁
    //设置窗口背景
    this->setAutoFillBackground(true); // 这句要加上, 否则可能显示不出背景图.
    QPalette palette = this->palette();
    palette.setBrush(QPalette::Window,QBrush(QColor("#f2f1f1")));             // 使用平滑的缩放方式
    setPalette(palette);

    //设置标题背景
    m_pTitleBar->setAutoFillBackground(true); // 这句要加上, 否则可能显示不出背景图.
    QPalette palette1 = m_pTitleBar->palette();
    palette1.setBrush(QPalette::Window,
                      QBrush(QPixmap(":/Images/resource/titleBg.png").scaled(// 缩放背景图.
                      this->size(),
                      Qt::IgnoreAspectRatio,
                      Qt::SmoothTransformation)));             // 使用平滑的缩放方式

    m_pTitleBar->setPalette(palette1);
    m_pTitleBar->setWinTitle(tr("User information"));

    //布局
    m_labeHead.setFixedSize(64,64);
    m_labeHead.move(60,60);

    m_labelAccount.setFixedSize(200,30);
    m_labelAccount.move(150,60);

    m_labelNickname.setFixedSize(200,30);
    m_labelNickname.move(150,90);

    m_labelTips.setFixedSize(200,60);
    m_labelTips.move(60,150);
    m_labelTips.setObjectName("lableTips");
    m_labelTips.setWordWrap(true);              //自动换行

    m_btnAddUser.move(100,200);
    m_btnAddUser.setFixedSize(80,30);
    m_btnAddUser.setStyleSheet("font:18px;");
    m_btnCalcel.move(200,200);
    m_btnCalcel.setFixedSize(80,30);
    m_btnCalcel.setStyleSheet("font:18px;");

    connect(&m_btnCalcel,&QPushButton::clicked,this,&FindUserResultDlg::close);
    connect(&m_btnAddUser,&QPushButton::clicked,this,&FindUserResultDlg::close);
}

void FindUserResultDlg::setUserinfo(QString username, QString nickname, int facetype, bool showtips)
{
    m_labeHead.setPixmap(QPixmap(QString(":/Images/resource/head/%1.png").arg(facetype)));
    m_labelAccount.setText(tr("Account: ")+username);
    m_labelNickname.setText(tr("Nickname: ")+nickname);
    m_labelTips.setText(tr("You cannot add yourself as a friend"));
    showtips == true ? m_labelTips.show():m_labelTips.hide();
    showtips == true ? m_btnAddUser.setEnabled(false):m_btnAddUser.setEnabled(true);
}

FindUserResultDlg::~FindUserResultDlg()
{

}

