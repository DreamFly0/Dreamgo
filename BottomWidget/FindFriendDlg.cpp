﻿#include "FindFriendDlg.h"
#include "LoginTitleBar.h"
#include "FindUserResultDlg.h"
#include <QDebug>
#include <QValidator>
#pragma execution_character_set("utf-8")
FindFriendDlg::FindFriendDlg(QWidget *parent)
    : CBaseDialog(parent),
      m_edit(this),
      m_btnFind(this),
      m_btnContact(tr("Contact"),this),
      m_btnCluster(tr("Group"),this),
      m_btnGroup(this),
      m_labelTips(this)
{
    this->setFixedSize(380,200);
    this->setAttribute(Qt::WA_DeleteOnClose,false); //由于该对话框不需要parent，需要设置这个属性自动销毁
    //设置窗口背景
    this->setAutoFillBackground(true); // 这句要加上, 否则可能显示不出背景图.
    QPalette palette = this->palette();
    palette.setBrush(QPalette::Window,QBrush(QColor("#f2f1f1")));             // 使用平滑的缩放方式
    setPalette(palette);

    //设置标题背景
    m_pTitleBar->setAutoFillBackground(true); // 这句要加上, 否则可能显示不出背景图.
    QPalette palette1 = m_pTitleBar->palette();
    palette1.setBrush(QPalette::Window,
                      QBrush(QPixmap(":/Images/resource/titleBg.png").scaled(// 缩放背景图.
                      this->size(),
                      Qt::IgnoreAspectRatio,
                      Qt::SmoothTransformation)));             // 使用平滑的缩放方式

    m_pTitleBar->setPalette(palette1);
    m_pTitleBar->setWinTitle(tr("Find a contact or group"));

    //界面控件
    m_edit.setFixedSize(200,40);
    m_edit.move(30,60);
    m_edit.setPlaceholderText(tr("Mobilephone/Group number"));
    QRegExp regxSrv("\\d+");
    QValidator *validatorSrv = new QRegExpValidator(regxSrv, &m_edit);
    m_edit.setValidator( validatorSrv );

    m_btnFind.setFixedSize(100,40);
    m_btnFind.move(250,60);
    m_btnFind.setText(tr("Find"));

    m_btnContact.move(50,115);
    m_btnContact.setChecked(true);
    m_btnCluster.move(150,115);
    m_btnGroup.addButton(&m_btnContact,1);
    m_btnGroup.addButton(&m_btnCluster,2);

    m_labelTips.setFixedSize(350,60);
    m_labelTips.move(30,130);
    m_labelTips.hide();
    m_labelTips.setObjectName("lableTips");
    m_labelTips.setWordWrap(true);              //自动换行

    m_pDlg = new FindUserResultDlg;
    connect(&m_pDlg->m_btnAddUser,&QPushButton::clicked,this,&FindFriendDlg::SltAddUser);
}

FindFriendDlg::~FindFriendDlg()
{
    qDebug() << "~FindFriendDlg()";
    delete m_pDlg;
}

void FindFriendDlg::setUserInfo(int findUserid, QString username, QString nickname, int facetype, bool showtips)
{
    qDebug() << "FindFriendDlg::setUserInfo";
    m_pDlg->setUserinfo(username,nickname,facetype,showtips);
    m_pDlg->setModal(true);
    m_pDlg->show();
    m_findUserid = findUserid;
}

void FindFriendDlg::SltAddUser()
{
    Q_EMIT sigAddUser(m_findUserid);
}

