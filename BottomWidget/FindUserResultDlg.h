﻿#ifndef FINDUSERRESULTDLG_H
#define FINDUSERRESULTDLG_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>

#include "CustomWidget.h"
class FindFriendDlg;

class FindUserResultDlg : public CBaseDialog
{
    Q_OBJECT
public:
    friend class FindFriendDlg;
    explicit FindUserResultDlg(QWidget *parent = 0);
    void setUserinfo(QString username,QString nickname,int facetype,bool showtips);
    virtual ~FindUserResultDlg();
signals:
private:
    QLabel          m_labeHead;
    QLabel          m_labelAccount;
    QLabel          m_labelNickname;
    QLabel          m_labelTips;
    QPushButton     m_btnAddUser;
    QPushButton     m_btnCalcel;
public slots:
};

#endif // FINDUSERRESULTDLG_H
