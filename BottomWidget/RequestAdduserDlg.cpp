﻿#include "RequestAdduserDlg.h"
#include "LoginTitleBar.h"

RequestAdduserDlg::RequestAdduserDlg(QWidget *parent)
    : CBaseDialog(parent),
      m_labelTips(this),
      m_btnAccept(tr("Accept"),this),
      m_btnReject(tr("Reject"),this),
      m_btnGroup(this)
{
    this->setFixedSize(380,200);
    this->setAttribute(Qt::WA_DeleteOnClose,false); //由于该对话框不需要parent，需要设置这个属性自动销毁
    //设置窗口背景
    this->setAutoFillBackground(true); // 这句要加上, 否则可能显示不出背景图.
    QPalette palette = this->palette();
    palette.setBrush(QPalette::Window,QBrush(QColor("#f2f1f1")));             // 使用平滑的缩放方式
    setPalette(palette);

    //设置标题背景
    m_pTitleBar->setAutoFillBackground(true); // 这句要加上, 否则可能显示不出背景图.
    QPalette palette1 = m_pTitleBar->palette();
    palette1.setBrush(QPalette::Window,
                      QBrush(QPixmap(":/Images/resource/titleBg.png").scaled(// 缩放背景图.
                      this->size(),
                      Qt::IgnoreAspectRatio,
                      Qt::SmoothTransformation)));             // 使用平滑的缩放方式

    m_pTitleBar->setPalette(palette1);
    m_pTitleBar->setWinTitle(tr("Add buddy request"));

    m_labelTips.setFixedSize(300,60);
    m_labelTips.move(30,30);
    m_labelTips.setWordWrap(true);              //自动换行

    m_btnAccept.setFixedSize(80,30);
    m_btnReject.setFixedSize(80,30);
    m_btnAccept.move(60,120);
    m_btnReject.move(240,120);

    m_btnGroup.addButton(&m_btnAccept,1);           //1同意
    m_btnGroup.addButton(&m_btnReject,0);           //0拒绝
    connect(&m_btnAccept,&QPushButton::clicked,this,&RequestAdduserDlg::close);
    connect(&m_btnReject,&QPushButton::clicked,this,&RequestAdduserDlg::close);
}

