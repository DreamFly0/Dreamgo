﻿#include "EmotionWindow.h"
#include "MyEmotionWidget.h"
#include <QMouseEvent>
#include <QVBoxLayout>
#include <QFile>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QDebug>

EmotionWindow::EmotionWindow(QWidget *parent) : BaseWindow(parent)
{
    this->resize(0,0);  //插入表情以后自动充满widget
    initEmotion();
}

EmotionWindow::~EmotionWindow()
{

}

// 初始化表情窗口
void EmotionWindow::initEmotion()
{
    //表情配置文件路径
    QFile file(":/face/resource/Face/FaceConfig.xml");
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug()<<"Error: cannot open file";
        return ;
    }
    // 初始化正常表情框;后续可以添加一个hover在表情按钮的最近使用表情
    m_normalEmotionWidget = new MyEmotionWidget();
    connect(m_normalEmotionWidget,&MyEmotionWidget::sigclickedFaceid,this,&EmotionWindow::sigclickedFaceid);

    QXmlStreamReader reader;

    // 设置文件，这时会将流设置为初始状态
    reader.setDevice(&file);

    // 如果没有读到文档结尾，而且没有出现错误
    while (!reader.atEnd()) {
        // 读取下一个记号，它返回记号的类型
        QXmlStreamReader::TokenType type = reader.readNext();

        // 下面便根据记号的类型来进行不同的输出
        if (type == QXmlStreamReader::StartElement) {
            if(reader.name() == "face")
            {
                int faceid = reader.attributes().value("id").toInt();
                QString&& tip = reader.attributes().value("tip").toString();
                m_normalEmotionWidget->addEmotionItem(faceid, tip);
            }else if(reader.name() == "faceconfig")
            {
                int row = reader.attributes().value("row").toInt();
                int col = reader.attributes().value("col").toInt();
                int max_row = reader.attributes().value("max_row").toInt();
                int movie_width = reader.attributes().value("movie_width").toInt();
                int movie_height = reader.attributes().value("movie_height").toInt();
                int emotion_width = reader.attributes().value("emotion_width").toInt();
                int emotion_height = reader.attributes().value("emotion_height").toInt();

                m_normalEmotionWidget->setRowAndColumn(row, col);
                m_normalEmotionWidget->setMaxRow(max_row);
                m_normalEmotionWidget->setEmotionMovieSize(QSize(movie_width, movie_height));
                m_normalEmotionWidget->setEmotionSize(QSize(emotion_width, emotion_height));
                m_normalEmotionWidget->initTableWidget();
            }
        }
    }

    // 如果读取过程中出现错误，那么输出错误信息
    if (reader.hasError()) {
        qDebug() << "error: " << reader.errorString();
    }

    file.close();
    // 初始化样式
    QVBoxLayout *layout = new QVBoxLayout;
    layout->setSpacing(0);
    layout->setContentsMargins(0,0,0,0);
    layout->addWidget(m_normalEmotionWidget);
    m_MainWin.setLayout(layout);

}

void EmotionWindow::mouseMoveEvent(QMouseEvent *evt)
{

}

void EmotionWindow::SltClickedFaceid(int faceid)
{
    qDebug() << faceid;
}
/*
// 显示大表情窗口
void EmotionWindow::showNormalEmotion(QPoint point)
{
    m_smallEmotionWidget->setVisible(false);
    m_lableTitle->setText("This is Normal Emotion Window");
    this->setFixedSize(QSize(m_normalEmotionWidget->width() + 20, m_normalEmotionWidget->height() + 50));
    move(point);
    show();
}
*/
