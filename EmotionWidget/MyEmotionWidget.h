﻿#ifndef MYEMOTIONWIDGET_H
#define MYEMOTIONWIDGET_H

#include <QTableWidget>

class MyEmotionWidget : public QTableWidget
{
    Q_OBJECT
public:
    explicit MyEmotionWidget(QWidget *parent = 0);
    ~MyEmotionWidget();
    void setRowAndColumn(int row, int column);
    void setEmotionSize(QSize emotionSize);
    void setEmotionMovieSize(QSize emotionMovieSize);
    void setMaxRow(int maxRow);
    void initTableWidget();
    void addEmotionItem(int faceid , QString toolTip);
signals:
    void sigclickedFaceid(int faceid);
public slots:
private:
    int m_tableRow;
    int m_tableColumn;
    QSize m_emotionSize;
    QSize m_emotionMovieSize;
    int m_maxRow;
    int m_emotionCnt;
};

#endif // MYEMOTIONWIDGET_H
