﻿#ifndef MYEMOTIONITEMWIDGET_H
#define MYEMOTIONITEMWIDGET_H

#include <QLabel>
class MyEmotionItemWidget : public QLabel
{
    Q_OBJECT
public:
    explicit MyEmotionItemWidget(int faceid , QSize emotionMoiveSize);
    ~MyEmotionItemWidget();
protected:
    void enterEvent(QEvent* event);
    void leaveEvent(QEvent* event);
    void mousePressEvent(QMouseEvent *evt);
signals:
    void sigclickedFaceid(int id);
public slots:
private:
    int m_faceid;
};

#endif // MYEMOTIONITEMWIDGET_H
