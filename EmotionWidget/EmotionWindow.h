﻿#ifndef EMOTIONWINDOW_H
#define EMOTIONWINDOW_H

#include "BaseWindow.h"

class MyEmotionWidget;
class EmotionWindow : public BaseWindow
{
    Q_OBJECT
public:
    explicit EmotionWindow(QWidget *parent = 0);
    ~EmotionWindow();
    void initEmotion();
protected:
    void mouseMoveEvent(QMouseEvent *evt);
signals:
    void sigclickedFaceid(int faceid);
public slots:
    void SltClickedFaceid(int faceid);
private:
    MyEmotionWidget *m_normalEmotionWidget;
};

#endif // EMOTIONWINDOW_H
