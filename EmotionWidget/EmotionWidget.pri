HEADERS += \
    $$PWD/EmotionWindow.h \
    $$PWD/MyEmotionItemWidget.h \
    $$PWD/MyEmotionWidget.h

SOURCES += \
    $$PWD/EmotionWindow.cpp \
    $$PWD/MyEmotionItemWidget.cpp \
    $$PWD/MyEmotionWidget.cpp
