﻿#include "MyEmotionWidget.h"
#include "MyEmotionItemWidget.h"
#include <QHeaderView>

MyEmotionWidget::MyEmotionWidget(QWidget *parent)
    : QTableWidget(parent),
      m_emotionCnt(0)
{

}

MyEmotionWidget::~MyEmotionWidget()
{

}

// 设置表情窗口的行列数目
void MyEmotionWidget::setRowAndColumn(int row, int column)
{
    m_tableRow = row;
    m_tableColumn = column;
}
// 设置item表情框大小
void MyEmotionWidget::setEmotionSize(QSize emotionSize)
{
    m_emotionSize = emotionSize;
}
// 设置表情movie大小
void MyEmotionWidget::setEmotionMovieSize(QSize emotionMovieSize)
{
    m_emotionMovieSize = emotionMovieSize;
}
// 设置最大行数
void MyEmotionWidget::setMaxRow(int maxRow)
{
    m_maxRow = maxRow;
}
// 设置完参数后，进行初始化
void MyEmotionWidget::initTableWidget()
{
    // 设置无焦点
    this->setFocusPolicy(Qt::NoFocus);
    // 设置不可编辑
    this->setEditTriggers(QAbstractItemView::NoEditTriggers);
    // 设置行数
    this->setRowCount(m_tableRow);
    // 设置列数
    this->setColumnCount(m_tableColumn);
    // 设置表头不可见并设置表情item框大小
    this->horizontalHeader()->setVisible(false);
    this->horizontalHeader()->setDefaultSectionSize(m_emotionSize.width());

    this->verticalHeader()->setVisible(false);
    this->verticalHeader()->setDefaultSectionSize(m_emotionSize.height());

    // 设置表情窗口的大小，这里行数超过m_maxRow时作了处理，当行数超过给的最大值，则通过滚动 滚动条显示剩余的表情
    if (m_tableRow > m_maxRow)
    {
        this->setFixedHeight(m_emotionSize.height()*m_maxRow+ 2);
        // 这里宽度加30，是因为在这种情况下会tablewidget会显示出滚动条，所以为了显示正常，增加一点宽度
        this->setFixedWidth(m_emotionSize.width()*m_tableColumn + 10);
    }
    else
    {
        this->setFixedHeight(m_emotionSize.height()*m_tableRow + 2);
        this->setFixedWidth(m_emotionSize.width()*m_tableColumn + 2);
    }
}

void MyEmotionWidget::addEmotionItem(int faceid , QString toolTip)
{
    int row = m_emotionCnt / this->columnCount();
    int column = m_emotionCnt % this->columnCount();

    QTableWidgetItem* tableWidgetItem = new QTableWidgetItem;
    tableWidgetItem->setToolTip(toolTip);
    this->setItem(row, column, tableWidgetItem);

    MyEmotionItemWidget* emotionIcon = new MyEmotionItemWidget(faceid , m_emotionMovieSize);
    connect(emotionIcon,&MyEmotionItemWidget::sigclickedFaceid,this,&MyEmotionWidget::sigclickedFaceid);
    this->setCellWidget(row, column, emotionIcon);
    //m_emotionList.push_back(fileName);
    m_emotionCnt++;
}
