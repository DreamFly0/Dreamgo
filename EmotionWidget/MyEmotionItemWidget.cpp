#include "MyEmotionItemWidget.h"
#include <QDebug>
#include <QMovie>
#include <QEvent>
#include <QMouseEvent>
MyEmotionItemWidget::MyEmotionItemWidget(int faceid, QSize emotionMoiveSize)
    : QLabel(NULL),
      m_faceid(faceid)
{
    //首先构造函数中进行初始化;
    QMovie* iconMovie = new QMovie;
    iconMovie->setFileName(QString(":/face/resource/Face/%1.gif").arg(faceid));
    this->setMovie(iconMovie);
    setContentsMargins(3, 3, 3, 3);
    iconMovie->setScaledSize(QSize(emotionMoiveSize.width(), emotionMoiveSize.height()));
    //为了动态图片正常显示，（不加上start图片不显示，不加上stop图片一种处于动态效果）
    iconMovie->start();
    iconMovie->stop();
    setStyleSheet("QLabel:hover{border: 1px solid rgb(111, 156, 207);\
                                background: rgba(255, 255, 255, 200);}");
}

MyEmotionItemWidget::~MyEmotionItemWidget()
{
    QMovie* movie = this->movie();
    delete movie;
    //qDebug() << "MyEmotionItemWidget::~MyEmotionItemWidget";
}

// 鼠标进入Label事件
void MyEmotionItemWidget::enterEvent(QEvent* event)
{
    QMovie* movie = this->movie();
    movie->start();
    // 当鼠标悬浮在item上时修改margin值达到表情切换效果，见下图（在鼠标从一个表情滑到另一个表情时)
    setContentsMargins(4, 2, 2, 4);
    return QLabel::enterEvent(event);
}
// 鼠标离开Label事件
void MyEmotionItemWidget::leaveEvent(QEvent* event)
{
    QMovie* movie = this->movie();
    movie->jumpToFrame(0);
    movie->stop();
    //恢复原来的margin
    setContentsMargins(3, 3, 3, 3);
    return QLabel::leaveEvent(event);
}

void MyEmotionItemWidget::mousePressEvent(QMouseEvent *evt)
{
    if(evt->button() == Qt::LeftButton)
    {
        Q_EMIT sigclickedFaceid(m_faceid);
    }
}
