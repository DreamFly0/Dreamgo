#-------------------------------------------------
#
# Project created by QtCreator 2018-08-22T09:26:15
#
#-------------------------------------------------

QT       += webenginewidgets core gui network multimedia sql
CONFIG   += c++11
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ChatClient
TEMPLATE = app
DESTDIR     = $$PWD/../chat

INCLUDEPATH +=$$PWD LoginAnimation
include(LoginAnimation/LoginAnimation.pri)

INCLUDEPATH +=$$PWD BaseWidget
include(BaseWidget/BaseWidget.pri)

INCLUDEPATH +=$$PWD TopWidget
include(TopWidget/TopWidget.pri)

INCLUDEPATH +=$$PWD MiddleWidget
include(MiddleWidget/MiddleWidget.pri)

INCLUDEPATH +=$$PWD BottomWidget
include(BottomWidget/BottomWidget.pri)

INCLUDEPATH +=$$PWD LoginDlg
include(LoginDlg/LoginDlg.pri)

INCLUDEPATH +=$$PWD Network
include(Network/Network.pri)

INCLUDEPATH +=$$PWD ComApi
include(ComApi/ComApi.pri)

INCLUDEPATH +=$$PWD ScreenShot
include(ScreenShot/ScreenShot.pri)

INCLUDEPATH +=$$PWD EmotionWidget
include(EmotionWidget/EmotionWidget.pri)

SOURCES += main.cpp \
    MainWindow.cpp \
    DataBaseMagr.cpp

RESOURCES += \
    res.qrc

DISTFILES +=

HEADERS += \
    MainWindow.h \
    DataBaseMagr.h

FORMS += \
    MainWindow.ui

TRANSLATIONS = myI18N_zh_CN.ts
LIBS += -lDbgHelp
win32: LIBS += -lUser32
QMAKE_LFLAGS_RELEASE = /INCREMENTAL:NO /DEBUG
